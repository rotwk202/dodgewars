﻿import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/index';
import { LoginComponent } from './login/index';
import { RegisterComponent } from './register/index';
import { CreatenicknameComponent } from './createnickname/index';
import { LadderOneComponent } from './ladderone/index';
import { MatchesHistoryComponent } from './matcheshistory/index';
import { AuthGuard } from './_guards/index';
import { RecentgamesComponent } from './recentgames/recentgames.component';
import { FaqComponent } from './faq/faq.component';
import { TeamladderComponent } from './teamladder/teamladder.component';
import { NewsComponent } from './news/news.component';
import { RulesComponent } from './rules/rules.component';
import { GetstartedComponent } from './getstarted/getstarted.component';
import { ContactComponent } from './contact/contact.component';
import { SupportedmapsComponent } from './supportedmaps/supportedmaps.component';
import { MonthlyladderoneComponent } from './monthlyladderone/monthlyladderone.component';
import { MonthlyteamladderComponent } from './monthlyteamladder/monthlyteamladder.component';


const appRoutes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'log-in', component: LoginComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'individual-ladder', component: LadderOneComponent },
    { path: 'team-ladder', component: TeamladderComponent },
    { path: 'recent-games', component: RecentgamesComponent },
    { path: 'create-nickname', component: CreatenicknameComponent, canActivate: [AuthGuard] },
    { path: 'matcheshistory/:nick', component: MatchesHistoryComponent },
    { path: 'faq', component: FaqComponent },
    { path: 'news', component: NewsComponent },
    { path: 'rules', component: RulesComponent },
    { path: 'get-started', component: GetstartedComponent },
    { path: 'contact', component: ContactComponent },
    { path: 'supported-maps', component: SupportedmapsComponent },
    { path: 'monthly-individual-ladder', component: MonthlyladderoneComponent },
    { path: 'monthly-team-ladder', component: MonthlyteamladderComponent },

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const routing = RouterModule.forRoot(appRoutes);
