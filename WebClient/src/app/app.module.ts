import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { NguiAutoCompleteModule } from '@ngui/auto-complete';

import { AppComponent } from './app.component';
import { routing } from './app.routing';

import { customHttpProvider } from './_helpers/index';
import { AlertComponent } from './_directives/index';
import { CircleComponent, BulletsComponent, CircleSidenavComponent } from './_spinners/index';
import { AuthGuard } from './_guards/index';
import { AlertService, AuthenticationService, UserService, RateDataService, ImageService } from './_services/index';
import { HomeComponent } from './home/index';
import { LoginComponent } from './login/index';
import { RegisterComponent } from './register/index';
import { CreatenicknameComponent } from './createnickname/index';
import { NavbarComponent } from './navbar/index';
import { LadderOneComponent } from './ladderone/index';
import { MatchesHistoryComponent } from './matcheshistory/index';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Ng2CompleterModule } from "ng2-completer";
import { RecentgamesComponent } from './recentgames/recentgames.component';
import { MapImageDirective } from './_directives/index';
import { FaqComponent } from './faq/faq.component';
import { TeamladderComponent } from './teamladder/teamladder.component';
import { NewsComponent } from './news/news.component';
import { FooterComponent } from './footer/footer.component';
import { RulesComponent } from './rules/rules.component';
import { GetstartedComponent } from './getstarted/getstarted.component';
import { ContactComponent } from './contact/contact.component';
import { SupportedmapsComponent } from './supportedmaps/supportedmaps.component';
import { MonthlyladderoneComponent } from './monthlyladderone/monthlyladderone.component';
import { MonthlyteamladderComponent } from './monthlyteamladder/monthlyteamladder.component';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        routing,
        BrowserAnimationsModule,
        Ng2CompleterModule,
        AngularMultiSelectModule,
        NguiAutoCompleteModule,
    ],
    declarations: [
        AppComponent,
        AlertComponent,
        NavbarComponent,
        HomeComponent,
        LoginComponent, RegisterComponent,
        CreatenicknameComponent,
        LadderOneComponent,
        CircleComponent, BulletsComponent, CircleSidenavComponent, MatchesHistoryComponent,
        RecentgamesComponent,
        MapImageDirective,
        FaqComponent,
        TeamladderComponent,
        NewsComponent,
        FooterComponent,
        RulesComponent,
        GetstartedComponent,
        ContactComponent,
        SupportedmapsComponent,
        MonthlyladderoneComponent,
        MonthlyteamladderComponent
    ],
    exports: [
        MapImageDirective
    ],
    providers: [
        customHttpProvider,
        AuthGuard,
        AlertService,
        AuthenticationService,
        UserService,
        RateDataService,
        ImageService
    ],
    bootstrap: [AppComponent]
})

export class AppModule { }
