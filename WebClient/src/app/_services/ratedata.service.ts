﻿import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response, ResponseContentType } from '@angular/http';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/RX';
import { IWarrior, IWarriorMatchHistory, IApiMapModel, IWarriorMonthly, IGamesCountData } from '../_models/index';
import { IWarriorMatchDetail } from '../_models/IWarriorMatchDetail';
import { ITeamWarrior } from '../_models/ITeamWarrior';

@Injectable()
export class RateDataService {

    constructor(private http: Http) {
    }

    getLadderOneData(): Observable<IWarrior[]> {

        const url = '/api/Rate/GetLadderOneData/';

        return this.http.get(url)
            .map((response: Response) => response.json().Data)
            .catch(error => {
                return error;
            });
    }

    getTeamLadderData(): Observable<ITeamWarrior[]> {
        
        const url = '/api/Rate/GetTeamGamesLadderData/';

        return this.http.get(url)
            .map((response: Response) => response.json().Data)
            .catch(error => {
                return error;
            });
    }

    getMonthlyLadderOneData(dateString: string): Observable<IWarriorMonthly[]> {

        const url = '/api/Rate/GetMonthlyLadderOneData/' + dateString;

        return this.http.get(url)
            .map((response: Response) => response.json().Data)
            .catch(error => {
                return error;
            });
    }

    getMonthlyTeamLadderData(dateString: string): Observable<IWarriorMonthly[]> {

        const url = '/api/Rate/GetMonthlyTeamLadderData/' + dateString;

        return this.http.get(url)
            .map((response: Response) => response.json().Data)
            .catch(error => {
                return error;
            });
    }

    getWarriorHistory(nickname: string, pageNumber: number, take: number): Observable<IWarriorMatchDetail[]> {
      const url = '/api/Rate/GetMatchesHistory/';
      const body = {
        "Nickname" : nickname,
        "PageNumber" : pageNumber,
        "Take" : take
      };

      return this.http.post(url, body)
        .map((response: Response) => response.json().Data)
        .catch(error => {
            return error;
        });
    }

    getLastGames(gamesCount: number): Observable<IWarriorMatchDetail[]> {
        const url = '/api/Rate/GetLastGames/';
        const body = {
          "GamesCount" : gamesCount
        };
  
        return this.http.post(url, body)
          .map((response: Response) => response.json().Data)
          .catch(error => {
              return error;
          });
    }

    downloadFile(id: number): Observable<Blob> { 
        const options = new RequestOptions({responseType: ResponseContentType.Blob }); 
        const url = '/api/Rate/ExampleFile/' + id;
        
        return this.http.get(url, options).map(res => res.blob()) 
        
      } 

      downloadFileNew(id: number): Observable<Blob> { 
        const options = new RequestOptions({responseType: ResponseContentType.Blob }); 
        const url = '/api/Rate/DownloadReplay/' + id;
        
        return this.http.get(url, options).map(res => res.blob()) 
        
      } 

    getAllMapNames(): Observable<IApiMapModel[]> {
        var url = '/api/GameReport/GetAllMaps/';

        return this.http.get(url)
            .map((response: Response) => {
                var x = response.json().Data; 
                return response.json().Data;
            })
            .catch(error => {
                return error;
            });
    }

    fillAR(replayFile: File, jsonViewModel: string) {
        const url = '/api/GameReport/UnreportedMatch/';
        let headers = new Headers({ "ApiUnreportedMatchDetails": jsonViewModel } );
        headers.append('ClientName', 'web');
        headers.append('Token', localStorage.getItem('currentToken'));
        const options = new RequestOptions({ headers: headers });
    
        let formData = new FormData();
        formData.append("ReplayFile", replayFile, replayFile.name);
    
        return this.http.post(url, formData, options)
          .map((response: Response) => {
                return response.json().Message;
            })
            .catch(error => {
                throw error;
            });
    }

    getGamesCountData(): Observable<IGamesCountData> {
        const url = '/api/Rate/GetGamesCountData';

        return this.http.get(url)
            .map((response: Response) => response.json())
            .catch(error => {
                console.log(error);
                return null;
            });
    }

}
