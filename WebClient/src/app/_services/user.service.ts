﻿import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response } from '@angular/http';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/RX';
import { User, UserModel, IApiNickLoggedOn, IApiNickModel } from '../_models/index';
import { IWarrior } from '../_models/IWarrior';

@Injectable()
export class UserService {

    constructor(private http: Http) {
    }

    register(user: UserModel) {

        const headers = new Headers({ 'ClientName': 'web' });
        const options = new RequestOptions({ headers: headers });
        const url = '/api/Account/RegisterUser/';

        return this.http.post(url, user, options)
            .map((response: Response) => {
                return response.json();
            });
    }

    getToken(user: UserModel) {
        const headers = new Headers({ 'ClientName': 'web' });
        const options = new RequestOptions({ headers: headers });
        const url = '/api/Account/GetToken/';

        return this.http.post(url, user, options)
            .map((response: Response) => {
                return response.json();
            });

    }

    createNickname(nickname: string) {
        var url = '/api/Account/CreateNickname/';
        var body = { nicknameRequest: nickname };
        var token = localStorage.getItem('currentToken');

        const headers = new Headers();
        headers.append('Token', token);
        headers.append('ClientName', 'web');
        const options = new RequestOptions({ headers: headers });

        return this.http.post(url, body, options)
            .map((response: Response) => {
                return response.json();
            });
    }

    whoIsOnline(): Observable<IApiNickLoggedOn[]> {
        var url = '/api/Account/WhoIsOnline/';

        return this.http.get(url)
            .map((response: Response) => response.json().Data)
            .catch(error => {
                return error;
            });
    }

    getAllNicknames(): Observable<IApiNickModel[]> {
        var url = '/api/Account/GetAllNicknames/';

        return this.http.get(url)
            //.map((response: Response) => response.json().Data)
            .map((response: Response) => {
                var x = response.json().Data; 
                return response.json().Data;
            })
            .catch(error => {
                return error;
            });
    }

}
