import { Injectable } from '@angular/core';

@Injectable()
export class ImageService {
   
   localImageDictionary: { [CodeName: string] : string; } = {};
   

   constructor() {
      this.localImageDictionary["FordsOfIsen"] = "/assets/map-icons/fords-of-isen-ii.bmp";
   }

   getImageAssetPath(mapCodeName: string){
      return this.localImageDictionary[mapCodeName];
   }
    
}