﻿export interface IWarriorMatchHistory {
    GameId: number;
    MapName: string;
    IsGameWinner: boolean;
    PointsGained: number;
    ReplayFile: string;
}
