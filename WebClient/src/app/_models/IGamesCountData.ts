﻿export interface IGamesCountData {
    GamesCurrentMonthCount: number;
    GamesAllTimeCount: number;
}