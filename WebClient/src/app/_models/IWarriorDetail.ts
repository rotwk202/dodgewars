export interface IWarriorDetail {
    Nickname: string;
    IsLooser: boolean;
    PointsGained: number;
    IsGameHost: boolean;
}