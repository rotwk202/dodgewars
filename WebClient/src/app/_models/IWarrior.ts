﻿export interface IWarrior {
    Nickname: string;
    Rating: number;
    CareerWinsCount: number;
    CareerLosesCount: number;
    PointsOnLadderOne: number;
    IsLooser: boolean;
}