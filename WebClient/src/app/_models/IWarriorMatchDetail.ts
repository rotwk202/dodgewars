import { IWarriorDetail } from "./index";

export interface IWarriorMatchDetail {
    PlayedOn: string;
    GameId: number;
    MapCodeName: string;
    MapDisplayName: string;
    IsNeutralHost: boolean;
    Players: IWarriorDetail[];
}
