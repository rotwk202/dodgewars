﻿export interface IWarriorMonthly {
    Nickname: string;
    MonthlyRating: number;
    Wins: number;
    Loses: number;
}