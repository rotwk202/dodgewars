import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupportedmapsComponent } from './supportedmaps.component';

describe('SupportedmapsComponent', () => {
  let component: SupportedmapsComponent;
  let fixture: ComponentFixture<SupportedmapsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupportedmapsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupportedmapsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
