﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../_services/index';
import { fadeInAnimation } from '../_animations/fade-in.animation';

@Component({
    moduleId: module.id,
    templateUrl: 'createnickname.component.html',
    animations: [fadeInAnimation],
    host: { '[@fadeInAnimation]': '' }
})

export class CreatenicknameComponent {

    rForm: FormGroup;
    nickname: string = '';
    public loading = false;
    private alertMessage: string = '';
    private isFailedAlert: boolean = false;
    private isAlertVisible: boolean = false;

    constructor(private http: Http,
        private userService: UserService,
        private router: Router,
        fb: FormBuilder) {

        this.rForm = fb.group({
            'nickname': [null, Validators.compose([Validators.required, Validators.maxLength(10)])],
        });
    }

    showInfoAlert(eventType: string, message: string): void {
        if (eventType == 'success') {
            this.isAlertVisible = true;
            this.alertMessage = message;
            this.isFailedAlert = false;
        }
        else if (eventType == 'error') {
            this.isAlertVisible = true;
            this.alertMessage = message;
            this.isFailedAlert = true;
        }
    }

    hideAlert(){
        this.isAlertVisible = false;
    }

    createNickname() {
        this.loading = true;
        return this.userService.createNickname(this.nickname).subscribe(
            data => {
                this.loading = false;
                let res = JSON.parse(data);
                this.showInfoAlert("success", res.Message);
            },
            error => {
                this.loading = false;
                let res = JSON.parse(error);
                this.showInfoAlert("error", res.Message);
            });
    }
}