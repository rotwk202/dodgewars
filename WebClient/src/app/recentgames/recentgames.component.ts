import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Headers, Http, RequestOptions, Response } from '@angular/http';
import { RateDataService, ImageService } from '../_services/index';
import { IWarriorMatchHistory } from '../_models/index';
import { slide } from '../_animations/index';
import {saveAs as importedSaveAs} from "file-saver";
import { CircleComponent } from '../_spinners/index';
import { IWarriorMatchDetail } from '../_models/IWarriorMatchDetail';
import { fadeInAnimation } from '../_animations/fade-in.animation';

@Component({
  templateUrl: './recentgames.component.html',
  styleUrls: ['./recentgames.component.css'],
  animations: [fadeInAnimation],
  host: { '[@fadeInAnimation]': '' },
})
export class RecentgamesComponent implements OnInit {

  lastGamesCount: number;
  lastGames: IWarriorMatchDetail[];
  loading: boolean;
  gamesCurrentMonthCount: number;
  gamesAllTimeCount: number;

  constructor(private http: Http,
    private rateDataService: RateDataService,
    private route: ActivatedRoute,
    private imageService: ImageService) {
  }

  ngOnInit() {
    this.lastGamesCount = 10;
    this.getLastGames(this.lastGamesCount);
    this.getGamesCountData();
  }

  keyPress(event: any) {
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
       event.preventDefault();
    }
 }

  getLastGames(gamesCount: number){
    this.loading = true;
    this.lastGames = [];
    this.rateDataService.getLastGames(gamesCount)
        .subscribe(
        lastApiGames => {
            this.loading = false;
            this.lastGames = lastApiGames;
            //console.log(lastApiGames);
        },
        error => {
            this.loading = false;
        });
  }

  downloadReplay(gameIdentifier: number){
    var fileName = 'Game_' + gameIdentifier +'_Replay.BfME2Replay'
    this.rateDataService.downloadFileNew(gameIdentifier)
           .subscribe(blob => {
               importedSaveAs(blob, fileName);
       }
   )
  }

  getImageAssetPath(mapCodeName: string) {
    return this.imageService.getImageAssetPath(mapCodeName);
  }

  getGamesCountData(){
    this.loading = true;
    var x = this.rateDataService.getGamesCountData()
      .subscribe(
        apiGamesCountData => {
          this.gamesCurrentMonthCount = apiGamesCountData.GamesCurrentMonthCount;
          this.gamesAllTimeCount = apiGamesCountData.GamesAllTimeCount;
          this.loading = false;
      },
      error => {
          this.loading = false;
      });;
  }

}
