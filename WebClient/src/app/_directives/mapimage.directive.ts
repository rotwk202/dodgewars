import {Directive, OnInit, Input} from '@angular/core';
import { Http, Response } from '@angular/http';
import { ɵBROWSER_SANITIZATION_PROVIDERS, DomSanitizer } from '@angular/platform-browser';

@Directive({
  selector: '[map-image]',
  providers: [ɵBROWSER_SANITIZATION_PROVIDERS],
  host: {
    '[src]': 'sanitizedImageData'
  }
})
export class MapImageDirective implements OnInit {
  imageData: any;
  sanitizedImageData: any;
  @Input('map-image') mapCodeName: string;

  constructor(private http: Http,
              private sanitizer: DomSanitizer) { }

  ngOnInit() {        
    this.http.get("/api/Image/GetMapImage/" + this.mapCodeName)
      .map((response: Response) => response.json())
      .subscribe(
        data => {
          this.imageData = 'data:image/bmp;base64,' + data;
          this.sanitizedImageData = this.sanitizer.bypassSecurityTrustUrl(this.imageData);
        }
      );
  }
}