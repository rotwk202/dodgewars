import { Component, OnInit } from '@angular/core';
import { slide } from '../_animations/index';
import { fadeInAnimation } from '../_animations/fade-in.animation';

declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.css'],
  animations: [fadeInAnimation],
  host: { '[@fadeInAnimation]': '' }
})
export class FaqComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    $(".question").on("click", function () {

        //var isSubmenuVisible = $('#my-panel-options').is(':visible');
        var neighborAnswer = $(this).siblings('.answer');
        var isAnswerVisible = neighborAnswer.is(':visible');       

        if (!isAnswerVisible) {

            neighborAnswer.removeClass("hide-faq");
            neighborAnswer.addClass("show-faq");
        }
        else {
            neighborAnswer.removeClass("show-faq");
            neighborAnswer.addClass("hide-faq");
        }
    });

    // //hide menu if option was chosen
    // $("#my-panel-options li").on("click", function () {
    //     $("#my-panel-options").removeClass("show");
    //     $("#my-panel-options").addClass("hide");
    // });

    // //hide menu if mouse left options list
    // $("#my-panel-options").on("mouseleave", function () {
    //     $("#my-panel-options").removeClass("show");
    //     $("#my-panel-options").addClass("hide");
    // });

    // //hide menu if clicked outside the menu
    // $(document).click(function (event: any) {
    //     if (!$(event.target).is("#my-panel")) {
    //         $("#my-panel-options").removeClass("show");
    //         $("#my-panel-options").addClass("hide");
    //     }
    // });
}

}
