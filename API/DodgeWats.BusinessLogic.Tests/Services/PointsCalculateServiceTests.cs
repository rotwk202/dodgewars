﻿using DodgeWars.BusinessLogic.Models;
using DodgeWars.BusinessLogic.Services;
using DodgeWars.DAL.Model;
using NUnit.Framework;
using System.Collections.Generic;

namespace DodgeWats.BusinessLogic.Tests.Services
{
    [TestFixture]
    public class PointsCalculateServiceTests
    {
        [TestCase(1500, 1020, 1502, 1020, 2, 0)] //+2, -0
        [TestCase(1035, 1000, 1052, 1000, 17, 0)] //+17, -0
        [TestCase(1010, 1800, 1048, 1769, 38, -31)] //+38, -31
        [TestCase(1800, 1201, 1801, 1200, 1, -1)] //+1, -1
        [TestCase(1200, 1203, 1220, 1200, 20, -3)] //+20, -3 -> can't go lower than 1200
        public void UpdateNicknamesIndividualRatingsTests(int winnerRating,
            int looserRating,
            int winnerUpdatedRating,
            int loserUpdatedRating,
            int winnerExpectedDelta,
            int loserExpectedDelta)
        {
            //Arrange
            var dbNicknames = new List<Nickname>
            {
                new Nickname() { Nickname1 = "Loser", Id = 1, Rating = looserRating, CurrentMonthlyRating = looserRating },
                new Nickname() { Nickname1 = "Winner", Id = 2, Rating = winnerRating, CurrentMonthlyRating = winnerRating }
            };

            var apiPlayers = new List<ApiPlayer>
            {
                new ApiPlayer() { Nickname = "Loser", IsLooser = true },
                new ApiPlayer() { Nickname = "Winner", IsLooser = false }
            };

            //Act
            var calculationResults = PointsCalculateService.GetIndividualRatingsDeltas(dbNicknames, apiPlayers);
            RateService.UpdateNicknamesIndividualRatings(dbNicknames, calculationResults);

            //Assert
            Assert.AreEqual(winnerUpdatedRating, dbNicknames.Find(x => x.Nickname1 == "Winner").Rating);
            Assert.AreEqual(loserUpdatedRating, dbNicknames.Find(x => x.Nickname1 == "Loser").Rating);
            Assert.AreEqual(winnerUpdatedRating, dbNicknames.Find(x => x.Nickname1 == "Winner").CurrentMonthlyRating);
            Assert.AreEqual(loserUpdatedRating, dbNicknames.Find(x => x.Nickname1 == "Loser").CurrentMonthlyRating);
            Assert.AreEqual(winnerExpectedDelta, calculationResults.Find(x => x.NicknameId == 2).GlobalPointsDelta);
            Assert.AreEqual(loserExpectedDelta, calculationResults.Find(x => x.NicknameId == 1).GlobalPointsDelta);
            Assert.AreEqual(winnerExpectedDelta, calculationResults.Find(x => x.NicknameId == 2).MonthlyPointsDelta);
            Assert.AreEqual(loserExpectedDelta, calculationResults.Find(x => x.NicknameId == 1).MonthlyPointsDelta);
        }

        [TestCase(1400, 1450, 1100, 1120, 1405, 1455, 1100, 1120, 5, 5, 0, 0)] //inside lenience range, +5, -0
        [TestCase(1100, 1150, 1005, 1000, 1114, 1164, 1005, 1000, 14, 14, 0, 0)] //inside lenience range, +14, -0
        [TestCase(1500, 1500, 1500, 1500, 1516, 1516, 1484, 1484, 16, 16, -16, -16)] //outside out bonus range +16, -16
        [TestCase(1450, 1450, 1450, 1450, 1470, 1470, 1434, 1434, 20, 20, -16, -16)] //inside out bonus range +20, -16
        public void GetTeamRatingDeltasTests(int winner1Rating, int winner2Rating,
            int loser1Rating, int loser2Rating,
            int winner1UpdatedRating, int winner2UpdatedRating,
            int loser1UpdatedRating, int loser2UpdatedRating,
            int winner1ExpectedDelta, int winner2ExpectedDelta,
            int loser1ExpectedDelta, int loser2ExpectedDelta)
        {
            //Arrange
            var dbNicknames = new List<Nickname>
            {
                new Nickname() { Nickname1 = "Winner1", Id = 1, TeamRating = winner1Rating, CurrentMonthlyTeamRating = winner1Rating },
                new Nickname() { Nickname1 = "Winner2", Id = 2, TeamRating = winner2Rating, CurrentMonthlyTeamRating = winner2Rating },
                new Nickname() { Nickname1 = "Loser1", Id = 3, TeamRating = loser1Rating, CurrentMonthlyTeamRating = loser1Rating },
                new Nickname() { Nickname1 = "Loser2", Id = 4, TeamRating = loser2Rating, CurrentMonthlyTeamRating = loser2Rating }
            };

            var apiPlayers = new List<ApiPlayer>
            {
                new ApiPlayer() { Nickname = "Winner1", IsLooser = false },
                new ApiPlayer() { Nickname = "Winner2", IsLooser = false },
                new ApiPlayer() { Nickname = "Loser1", IsLooser = true },
                new ApiPlayer() { Nickname = "Loser2", IsLooser = true }
            };

            //Act
            var calculationResults = PointsCalculateService.GetTeamRatingDeltas(dbNicknames, apiPlayers);
            RateService.UpdateNicknamesTeamRatings(dbNicknames, calculationResults);

            //Assert
            Assert.AreEqual(winner1ExpectedDelta, calculationResults.Find(x => x.NicknameId == 1).GlobalPointsDelta);
            Assert.AreEqual(winner2ExpectedDelta, calculationResults.Find(x => x.NicknameId == 2).GlobalPointsDelta);
            Assert.AreEqual(loser1ExpectedDelta, calculationResults.Find(x => x.NicknameId == 3).GlobalPointsDelta);
            Assert.AreEqual(loser2ExpectedDelta, calculationResults.Find(x => x.NicknameId == 4).GlobalPointsDelta);

            Assert.AreEqual(winner1ExpectedDelta, calculationResults.Find(x => x.NicknameId == 1).MonthlyPointsDelta);
            Assert.AreEqual(winner2ExpectedDelta, calculationResults.Find(x => x.NicknameId == 2).MonthlyPointsDelta);
            Assert.AreEqual(loser1ExpectedDelta, calculationResults.Find(x => x.NicknameId == 3).MonthlyPointsDelta);
            Assert.AreEqual(loser2ExpectedDelta, calculationResults.Find(x => x.NicknameId == 4).MonthlyPointsDelta);

            Assert.AreEqual(winner1UpdatedRating, dbNicknames.Find(x => x.Nickname1 == "Winner1").TeamRating);
            Assert.AreEqual(winner2UpdatedRating, dbNicknames.Find(x => x.Nickname1 == "Winner2").TeamRating);
            Assert.AreEqual(loser1UpdatedRating, dbNicknames.Find(x => x.Nickname1 == "Loser1").TeamRating);
            Assert.AreEqual(loser2UpdatedRating, dbNicknames.Find(x => x.Nickname1 == "Loser2").TeamRating);

            Assert.AreEqual(winner1UpdatedRating, dbNicknames.Find(x => x.Nickname1 == "Winner1").CurrentMonthlyTeamRating);
            Assert.AreEqual(winner2UpdatedRating, dbNicknames.Find(x => x.Nickname1 == "Winner2").CurrentMonthlyTeamRating);
            Assert.AreEqual(loser1UpdatedRating, dbNicknames.Find(x => x.Nickname1 == "Loser1").CurrentMonthlyTeamRating);
            Assert.AreEqual(loser2UpdatedRating, dbNicknames.Find(x => x.Nickname1 == "Loser2").CurrentMonthlyTeamRating);
        }
    }
}
