﻿using DodgeWars.DAL.Contracts;
using DodgeWars.DAL.Model;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DodgeWats.BusinessLogic.Tests
{
    internal class ServiceTestsBase
    {
        protected IRepository<Nickname> BaseNicknameRepository { get; }
        protected IRepository<OneVsOneLadder> BaseOneVsOneLadderRepository { get; }
        protected IRepository<MatchesHistory> BaseMatchesHistoryRepository { get; }
        protected IRepository<Game> BaseGameRepository { get; }
        protected IRepository<AllMatchesHistory> BaseAllMatchesHistoryRepository { get; }
        protected IRepository<Map> BaseMapRepository { get; }
        protected IRepository<TeamGamesLadder> BaseTeamGamesLadderRepository { get; }
        protected IRepository<Replay> BaseReplayRepository { get; }

        public ServiceTestsBase()
        {
            var nickRepoMock = new Mock<IRepository<Nickname>>();
            var oneVsOneLadderRepoMock = new Mock<IRepository<OneVsOneLadder>>();
            var matchesHistoryRepoMock = new Mock<IRepository<MatchesHistory>>();
            var gameRepoMock = new Mock<IRepository<Game>>();
            var allMatchesHistoryRepoMock = new Mock<IRepository<AllMatchesHistory>>();
            var mapRepoMock = new Mock<IRepository<Map>>();
            var teamGamesLadderRepoMock = new Mock<IRepository<TeamGamesLadder>>();
            var replayRepoMock = new Mock<IRepository<Replay>>();

            BaseNicknameRepository = nickRepoMock.Object;
            BaseOneVsOneLadderRepository = oneVsOneLadderRepoMock.Object;
            BaseMatchesHistoryRepository = matchesHistoryRepoMock.Object;
            BaseGameRepository = gameRepoMock.Object;
            BaseAllMatchesHistoryRepository = allMatchesHistoryRepoMock.Object;
            BaseMapRepository = mapRepoMock.Object;
            BaseTeamGamesLadderRepository = teamGamesLadderRepoMock.Object;
            BaseReplayRepository = replayRepoMock.Object;
        }
    }
}
