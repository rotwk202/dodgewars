﻿using System;

namespace DodgeWars.Tools.Constants
{
    [Flags]
    public enum LadderType
    {
        SingleLadder,
        TeamLadder
    }
}
