﻿using log4net;

namespace DodgeWars.Tools
{
    public static class GameLogUtility
    {
        public static void LogGameInfo(ILog logger, string apiGameJson)
        {
            var indexOfReplayString = apiGameJson.IndexOf("\"ReplayFile\"");
            var indexOfPlayers = apiGameJson.IndexOf("\"Players");
            var truncated = apiGameJson.Remove(indexOfReplayString, indexOfPlayers - indexOfReplayString);
            var logContent = $"GameLogUtility.LogGameInfo(): This is game json that is going to be reported: {truncated}";
            logger.Info(logContent);
        }
    }
}
