﻿using DodgeWars.BusinessLogic.Contracts;
using DodgeWars.BusinessLogic.Models;
using DodgeWars.DAL.Contracts;
using DodgeWars.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DodgeWars.BusinessLogic.Builders
{
    public class RatingBuilder : IRatingBuilder
    {
        private readonly IRepository<RatingType> _ratingTypeRepository;
        private readonly IRepository<EventType> _eventTypeRepository;

        public RatingBuilder(IRepository<RatingType> ratingTypeRepository,
            IRepository<EventType> eventTypeRepository)
        {
            _ratingTypeRepository = ratingTypeRepository;
            _eventTypeRepository = eventTypeRepository;
        }

        public IList<Rating> GetRatingRecords(IList<CalculationResult> calculationResults, Game newGame)
        {
            var result = new List<Rating>();
            var ratingTypeId = GetRatingTypeId(calculationResults);
            var gameEventTypeId = GetGameEventTypeId();

            foreach (var cr in calculationResults)
            {
                result.Add(new Rating()
                {
                    RatingTypeId = ratingTypeId,
                    EventTypeId = gameEventTypeId,
                    OccuredOn = newGame.PlayedOn,
                    NicknameId = cr.NicknameId,
                    PointsDelta = cr.GlobalPointsDelta,
                    MonthlyPointsDelta = cr.MonthlyPointsDelta
                });
            }

            return result;
        }

        public IList<Rating> GetTeamRatingRecords(IList<CalculationResult> calculationResults, Game newGame)
        {
            throw new NotImplementedException();
        }

        private int GetRatingTypeId(IList<CalculationResult> calculationResults)
        {
            if (calculationResults.Count == 2)
            {
                return _ratingTypeRepository.GetAll().FirstOrDefault(x => x.Name == "Individual").Id;
            }
            if (calculationResults.Count > 2)
            {
                return _ratingTypeRepository.GetAll().FirstOrDefault(x => x.Name == "Team").Id;
            }

            return -1;
        }

        private int GetGameEventTypeId()
        {
            return _eventTypeRepository.GetAll().FirstOrDefault(x => x.Name == "Game").Id;
        }
    }
}
