﻿namespace DodgeWars.BusinessLogic
{
    public static class Constants
    {
        public static double InitialRatingValue = 1000;
        public static double LenienceRatingCap = 1200;
        public static double BonusMultiplierCap = 1500;
        public static double BonusDivisor = 5;  // 1/5 = 0.2
    }
}
