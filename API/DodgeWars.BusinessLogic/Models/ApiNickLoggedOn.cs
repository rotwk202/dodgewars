﻿using System;

namespace DodgeWars.BusinessLogic.Models
{
    public class ApiNickLoggedOn
    {
        public string Nickname { get; set; }

        public DateTime LoggedOn { get; set; }
    }
}