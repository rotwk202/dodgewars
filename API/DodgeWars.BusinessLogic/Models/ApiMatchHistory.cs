﻿using System;
using System.Collections.Generic;

namespace DodgeWars.BusinessLogic.Models
{
    public class ApiMatchHistory
    {
        public DateTime PlayedOn { get; set; }

        public int GameId { get; set; }

        public string MapCodeName { get; set; }

        public string MapDisplayName { get; set; }

        public bool IsNeutralHost { get; set; }

        public List<ApiPlayer> Players { get; set; }
    }
}
