﻿namespace DodgeWars.BusinessLogic.Models
{
    public class CalculationResult
    {
        public int NicknameId { get; set; }

        public int GlobalPointsDelta { get; set; }

        public int MonthlyPointsDelta { get; set; }

        public bool IsLoser { get; set; }
    }
}
