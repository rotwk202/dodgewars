﻿namespace DodgeWars.BusinessLogic.Models
{
    public class ApiLastGamesRequest
    {
        public int GamesCount { get; set; }
    }
}
