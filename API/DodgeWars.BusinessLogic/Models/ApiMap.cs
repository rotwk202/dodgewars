﻿namespace DodgeWars.BusinessLogic.Models
{
    public class ApiMap
    {
        public string CodeName { get; set; }
        public string DisplayName { get; set; }
        public byte[] Image { get; set; }
        public int ReplayMapId { get; set; }

        public string ReplayMapTextId { get; set; }
    }
}
