﻿using System;
using System.Collections.Generic;

namespace DodgeWars.BusinessLogic.Models
{
    public class ApiUnreportedMatchDetails
    {
        public string MapName { get; set; }

        public DateTime RaisedOn { get; set; }

        public DateTime PlayedOn { get; set; }

        public List<ApiPlayer> Players { get; set; }
    }
}
