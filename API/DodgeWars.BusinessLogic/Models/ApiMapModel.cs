﻿namespace DodgeWars.BusinessLogic.Models
{
    public class ApiMapModel
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
