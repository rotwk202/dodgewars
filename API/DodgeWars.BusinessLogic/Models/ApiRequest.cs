﻿using System;

namespace DodgeWars.BusinessLogic.Models
{
    public class ApiRequest
    {
        public DateTime RequestTime { get; set; }
    }
}
