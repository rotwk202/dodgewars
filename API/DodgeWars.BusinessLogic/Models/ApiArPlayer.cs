﻿using System;

namespace DodgeWars.BusinessLogic.Models
{
    public class ApiArPlayer
    {
        public string Nickname { get; set; }

        public int Team { get; set; }

        public bool IsLooser { get; set; }

        public string Army { get; set; }

        public bool IsObserver { get; set; }

        public long? LastReplayFileLength { get; set; }

        public DateTime PlayedOn { get; set; }

        public int GameId { get; set; }

        public int ReplayMapId { get; set; }

        public byte[] ReplayFile { get; set; }

        public int PointsGained { get; set; }

        public bool? IsGameHost { get; set; }

        public double? Rating { get; set; }

        public double TeamRating { get; set; }
    }
}