﻿namespace DodgeWars.BusinessLogic.Models
{
    public class ApiNickModel
    {
        public int Id { get; set; }

        public string Nick { get; set; }
    }
}
