﻿namespace DodgeWars.BusinessLogic.Models
{
    public class ApiTeamGamesWarriorModel
    {
        public string Nickname { get; set; }

        public double TeamRating { get; set; }

        public int TeamGamesLadderWinsCount { get; set; }

        public int TeamGamesLadderLosesCount { get; set; }

        public int PointsOnTeamGamesLadder { get; set; }
    }
}
