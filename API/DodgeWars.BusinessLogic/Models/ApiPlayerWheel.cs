﻿namespace DodgeWars.BusinessLogic.Models
{
    public class ApiPlayerWheel
    {
        public string Nickname { get; set; }

        public double LadderOneRating { get; set; }

        public double TeamLaaderRating { get; set; }
    }
}