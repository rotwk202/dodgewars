﻿namespace DodgeWars.BusinessLogic.Models
{
    public class ApiRateRequest
    {
        public string Nickname { get; set; }

        public int PageNumber { get; set; }

        public int Take { get; set; }
    }
}
