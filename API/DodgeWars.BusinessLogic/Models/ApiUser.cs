﻿namespace DodgeWars.BusinessLogic.Models
{
    public class ApiUser
    {
        public string Email { get; set; }

        public string Password { get; set; }
    }
}