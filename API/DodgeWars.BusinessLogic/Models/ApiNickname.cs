﻿namespace DodgeWars.BusinessLogic.Models
{
    public class ApiNickname
    {
        public int UserId { get; set; }

        public string Nickname { get; set; }

        public double Rating { get; set; }

        public int CareerWinsCount { get; set; }

        public int CareerLosesCount { get; set; }

        public int PointsOnLadderOne { get; set; }
    }
}
