﻿namespace DodgeWars.BusinessLogic.Models
{
    public class ApiToken
    {
        public string Token { get; set; }
    }
}
