﻿using DodgeWars.BusinessLogic.Contracts;
using DodgeWars.BusinessLogic.Models;
using DodgeWars.DAL.Contracts;
using DodgeWars.DAL.Model;
using System.Collections.Generic;
using System.Linq;
using System;

namespace DodgeWars.BusinessLogic.Services
{
    public class RateService : IRateService
    {
        private readonly IRepository<Nickname> _nicknameRepository;
        private readonly IRepository<OneVsOneLadder> _ladderOneViewHandler;
        private readonly IRepository<TeamGamesLadder> _teamGamesLadderViewHandler;
        private readonly IRepository<AllMatchesHistory> _allMatchesHistoryViewHandler;
        private readonly IRepository<Game> _gameRepository;
        private readonly IRepository<Map> _mapRepository;
        private readonly IRepository<Replay> _replayRepository;

        public RateService(IRepository<Nickname> nicknameRepository,
            IRepository<OneVsOneLadder> ladderOneViewHandler,
            IRepository<Game> gameRepository,
            IRepository<AllMatchesHistory> allMatchesHistoryViewHandler,
            IRepository<Map> mapRepository,
            IRepository<TeamGamesLadder> teamGamesLadderViewHandler,
            IRepository<Replay> replayRepository)
        {
            _ladderOneViewHandler = ladderOneViewHandler;
            _gameRepository = gameRepository;
            _allMatchesHistoryViewHandler = allMatchesHistoryViewHandler;
            _mapRepository = mapRepository;
            _teamGamesLadderViewHandler = teamGamesLadderViewHandler;
            _nicknameRepository = nicknameRepository;
            _replayRepository = replayRepository;
        }

        public List<ApiNickname> GetLadderOneData()
        {
            var output = new List<ApiNickname>();
            var data = _ladderOneViewHandler
                .GetAll()
                .Where(x => x.Wins > 0 || x.Loses > 0)
                .ToList();

            foreach (var item in data)
            {
                output.Add(new ApiNickname
                {
                    Nickname = item.Nickname,
                    Rating = (double)item.IndividualRating,
                    CareerWinsCount = (int)item.Wins,
                    CareerLosesCount = (int)item.Loses,
                    PointsOnLadderOne = (int)item.IndividualRating
                });
            }

            return output.OrderByDescending(x => x.PointsOnLadderOne).ToList();
        }

        public List<ApiMatchHistory> GetMatchesHistory(string nickname, int pageNumber, int take)
        {
            var output = new List<ApiMatchHistory>();

            if (pageNumber > 1)
            {
            }

            var one = _allMatchesHistoryViewHandler
                .GetAll()
                .OrderByDescending(x => x.PlayedOn)
                .ToList()
                .Where(x => x.Nickname == nickname)
                .Select(x => x.Game_ID)
                .Distinct()
                .ToList();

            var gameIds = one
                .Skip(pageNumber - 1)
                .Take(take)
                .ToList();

            var totalDataToReadFrom = _allMatchesHistoryViewHandler
                .GetAll()
                .Where(x => gameIds.Contains(x.Game_ID))
                .ToList();

            foreach (var gameId in gameIds)
            {
                var gameEntries = totalDataToReadFrom.Where(x => x.Game_ID == gameId).ToList();
                var currentEntry = gameEntries.First();

                var gamePlayers = new List<ApiPlayer>();
                foreach (var ge in gameEntries)
                {
                    gamePlayers.Add(new ApiPlayer()
                    {
                        Nickname = ge.Nickname,
                        IsLooser = !ge.Won,
                        PointsGained = ge.PointsGained,
                        IsGameHost = ge.IsGameHost
                    });
                }

                output.Add(new ApiMatchHistory()
                {
                    GameId = currentEntry.Game_ID,
                    PlayedOn = currentEntry.PlayedOn,
                    MapCodeName = currentEntry.MapCodeName,
                    MapDisplayName = currentEntry.MapDisplayName,
                    Players = gamePlayers,
                });
            }

            SetNeutralHostInfo(ref output);

            return output.OrderByDescending(x => x.PlayedOn).ToList();
        }

        public List<ApiMatchHistory> GetLastGames(int gamesCount)
        {
            var output = new List<ApiMatchHistory>();

            var gameIds = _allMatchesHistoryViewHandler
                .GetAll()
                .OrderByDescending(x => x.PlayedOn)
                .ToList()
                .Select(x => x.Game_ID)
                .Distinct()
                .Take(gamesCount)
                .ToList();

            var dbGameRecords = _allMatchesHistoryViewHandler
                .GetAll()
                .Where(x => gameIds.Contains(x.Game_ID))
                .ToList();

            foreach (var gameId in gameIds)
            {
                var gameEntries = dbGameRecords.Where(x => x.Game_ID == gameId).ToList();
                var currentEntry = gameEntries.First();

                var gamePlayers = new List<ApiPlayer>();
                foreach (var ge in gameEntries)
                {
                    gamePlayers.Add(new ApiPlayer()
                    {
                        Nickname = ge.Nickname,
                        IsLooser = !ge.Won,
                        IsGameHost = ge.IsGameHost
                    });
                }

                output.Add(new ApiMatchHistory()
                {
                    GameId = currentEntry.Game_ID,
                    PlayedOn = currentEntry.PlayedOn,
                    MapCodeName = currentEntry.MapCodeName,
                    MapDisplayName = currentEntry.MapDisplayName,
                    Players = gamePlayers
                });
            }

            SetNeutralHostInfo(ref output);

            return output.OrderByDescending(x => x.PlayedOn).ToList();
        }

        public byte[] GetReplayFile(int gameIdentifier)
        {
            var game = _gameRepository.GetAll().FirstOrDefault(x => x.Identifier == gameIdentifier);
            if (game == null)
            {
                throw new NullReferenceException("Could not find game with given ID.");
            }

            var replay = _replayRepository.GetAll().FirstOrDefault(x => x.Id == game.Id);
            if (replay == null)
            {
                throw new NullReferenceException("Could not find replay of game with given ID.");
            }

            return replay.File;
        }

        public byte[] GetMapImage(string mapCodeName)
        {
            var map = _mapRepository.GetAll().FirstOrDefault(x => x.CodeName == mapCodeName);

            return map?.Image;
        }

        private static void SetNeutralHostInfo(ref List<ApiMatchHistory> apiMatchHistory)
        {
            foreach (var match in apiMatchHistory)
            {
                var isNeutralHost = match.Players
                    .Select(x => x.IsGameHost)
                    .ToList()
                    .TrueForAll(x => x == null);

                if (isNeutralHost)
                {
                    match.IsNeutralHost = true;
                }
            }
        }

        public List<ApiTeamGamesWarriorModel> GetTeamGamesLadderData()
        {
            var output = new List<ApiTeamGamesWarriorModel>();
            var data = _teamGamesLadderViewHandler
                .GetAll()
                .Where(x => x.Wins > 0 || x.Loses > 0)
                .ToList();

            foreach (var item in data)
            {
                output.Add(new ApiTeamGamesWarriorModel
                {
                    Nickname = item.Nickname,
                    TeamRating = (double)item.TeamRating,
                    TeamGamesLadderWinsCount = (int)item.Wins,
                    TeamGamesLadderLosesCount = (int)item.Loses,
                    PointsOnTeamGamesLadder = (int)item.TeamRating
                });
            }

            return output.OrderByDescending(x => x.PointsOnTeamGamesLadder).ToList();
        }

        public List<Playa> GetMonthlyLadderOneData(string dateString)
        {
            DateTime dateResult;
            if (!DateTime.TryParse(dateString, out dateResult))
            {
                throw new ArgumentException($"Unable to parse the given dateString: {dateString}");
            }

            return _nicknameRepository.GetMonthlyLadderOneData( string.Format("{0:yyyy-MM}", dateResult), (int)Constants.InitialRatingValue).ToList();
        }

        public List<Playa> GetMonthlyTeamLadderData(string dateString)
        {
            DateTime dateResult;
            if (!DateTime.TryParse(dateString, out dateResult))
            {
                throw new ArgumentException($"Unable to parse the given dateString: {dateString}");
            }

            return _nicknameRepository.GetMonthlyTeamLadderData(string.Format("{0:yyyy-MM}", dateResult), (int)Constants.InitialRatingValue).ToList();
        }

        public ApiGamesCountData GetGamesCountData()
        {
            var gamesAllTimeCount = _gameRepository.GetAll().Count();
            var gamesCurrentMonthCount = _gameRepository
                .GetAll()
                .Where(x => x.PlayedOn.Month == DateTime.Now.Month)
                .Count();

            return new ApiGamesCountData() { GamesAllTimeCount = gamesAllTimeCount, GamesCurrentMonthCount = gamesCurrentMonthCount };
        }

        public static void UpdateNicknamesIndividualRatings(IList<Nickname> dbNicknamesToUpdate, IList<CalculationResult> calculationResults)
        {
            var loserResult = calculationResults.FirstOrDefault(x => x.IsLoser);
            var winnerResult = calculationResults.FirstOrDefault(x => !x.IsLoser);

            var dbLoser = dbNicknamesToUpdate.FirstOrDefault(x => x.Id == loserResult.NicknameId);
            dbLoser.Rating += loserResult.GlobalPointsDelta;
            dbLoser.CurrentMonthlyRating += loserResult.MonthlyPointsDelta;

            var dbWinner = dbNicknamesToUpdate.FirstOrDefault(x => x.Id == winnerResult.NicknameId);
            dbWinner.Rating += winnerResult.GlobalPointsDelta;
            dbWinner.CurrentMonthlyRating += winnerResult.MonthlyPointsDelta;
        }

        public static void UpdateNicknamesTeamRatings(IList<Nickname> dbNicknamesToUpdate, IList<CalculationResult> calculationResults)
        {
            var losersIds = calculationResults.Where(x => x.IsLoser).Select(x => x.NicknameId).ToList();

            var dbLosers = dbNicknamesToUpdate.Where(x => losersIds.Contains(x.Id)).ToList();
            foreach (var loser in dbLosers)
            {
                var loserCalculatedResult = calculationResults.FirstOrDefault(x => x.NicknameId == loser.Id);
                loser.TeamRating += loserCalculatedResult.GlobalPointsDelta;
                loser.CurrentMonthlyTeamRating += loserCalculatedResult.MonthlyPointsDelta;
            }

            var dbWinners = dbNicknamesToUpdate.Where(x => !losersIds.Contains(x.Id)).ToList();
            foreach (var dbWinner in dbWinners)
            {
                var winnerCalculatedResult = calculationResults.FirstOrDefault(x => x.NicknameId == dbWinner.Id);
                dbWinner.TeamRating += winnerCalculatedResult.GlobalPointsDelta;
                dbWinner.CurrentMonthlyTeamRating += winnerCalculatedResult.MonthlyPointsDelta;
            }
        }
    }
}
