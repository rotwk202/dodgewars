﻿using DodgeWars.BusinessLogic.Contracts;
using DodgeWars.BusinessLogic.Models;
using DodgeWars.DAL.Contracts;
using DodgeWars.DAL.Model;
using DodgeWars.Tools;
using System;
using System.Linq;

namespace DodgeWars.BusinessLogic.Services
{
    public class AuthenticationService : IAuthenticationService
    {
        private readonly IRepository<User> _userRepository;

        public AuthenticationService(IRepository<User> userRepository)
        {
            _userRepository = userRepository;
        }

        public ApiToken GetToken(ApiUser apiUser, string clientName)
        {
            if (!_userRepository.GetAll().Any(x => x.Email == apiUser.Email))
            {
                throw new InvalidOperationException("There is no user with such e-mail address.");
            }

            if (string.IsNullOrEmpty(apiUser.Password))
            {
                throw new ArgumentException("Provided password either didn't exist or was empty.");
            }

            var dBUser = _userRepository.GetAll().FirstOrDefault(x => x.Email == apiUser.Email);
            var computedPasswordHash = PasswordEncryptor.CreateMD5(apiUser.Password);
            if (computedPasswordHash != dBUser.Password)
            {
                throw new ArgumentException("There is no user with such e-mail address or password provided was wrong.");
            }

            var now = DateTime.Now;
            var tokenSourceString = $"{now}{apiUser.Email}{apiUser.Password}";
            var token = PasswordEncryptor.CreateMD5(tokenSourceString);
            var tokenExpiresOn = now.AddHours(6);

            if(clientName == "console")
            {
                dBUser.TokenConsole = token;
                dBUser.TokenConsoleExpiresOn = tokenExpiresOn;
                _userRepository.Edit(dBUser);
            }
            if (clientName == "web")
            {
                dBUser.TokenWeb = token;
                dBUser.TokenWebExpiresOn = tokenExpiresOn;
                _userRepository.Edit(dBUser);
            }

            return new ApiToken { Token = token };
        }

        public bool IsTokenOutOfDate(string token, string clientName, DateTime requestTime)
        {
            DateTime? expirationDate = null;

            if (clientName == "console")
            {
                if (!_userRepository.GetAll().Any(x => x.TokenConsole == token))
                {
                    throw new ArgumentException("No such token was found");
                }
                expirationDate = _userRepository.GetAll().First(x => x.TokenConsole == token).TokenConsoleExpiresOn;
            }

            if (clientName == "web")
            {
                if (!_userRepository.GetAll().Any(x => x.TokenWeb == token))
                {
                    throw new ArgumentException("No such token was found");
                }
                expirationDate = _userRepository.GetAll().First(x => x.TokenWeb == token).TokenWebExpiresOn;
            }

            return !(expirationDate > requestTime);
        }
    }
}
