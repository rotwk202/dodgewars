﻿using System;
using System.Collections.Generic;
using System.Linq;
using DodgeWars.BusinessLogic.Contracts;
using DodgeWars.BusinessLogic.Models;
using DodgeWars.DAL.Contracts;
using DodgeWars.DAL.Model;
using DodgeWars.Tools;

namespace DodgeWars.BusinessLogic.Services
{
    public class AccountService : IAccountService
    {
        private readonly IRepository<User> _userRepository;
        private readonly IRepository<Nickname> _nicknameRepository;
        private readonly IRepository<WhoIsOnline> _whoIsOnlineViewHandler;
        private readonly IRepository<OneVsOneLadder> _oneVsOneViewHandler;
        private readonly IRepository<TeamGamesLadder> _teamGamesLadderViewHandler;
        private const int TokenValidityTimeInHours = 6;

        public AccountService(IRepository<User> userRepository,
            IRepository<Nickname> nicknameRepository,
            IRepository<WhoIsOnline> whoIsOnlineViewHandler,
            IRepository<OneVsOneLadder> oneVsOneViewHandler,
            IRepository<TeamGamesLadder> teamGamesLadderViewHandler)
        {
            _userRepository = userRepository;
            _nicknameRepository = nicknameRepository;
            _whoIsOnlineViewHandler = whoIsOnlineViewHandler;
            _oneVsOneViewHandler = oneVsOneViewHandler;
            _teamGamesLadderViewHandler = teamGamesLadderViewHandler;
        }

        public bool RegisterUser(ApiUser apiUser, string clientName)
        {
            if (_userRepository.GetAll().Any(x => x.Email == apiUser.Email))
            {
                throw new InvalidOperationException("User with such e-mail already exists.");
            }

            if (string.IsNullOrEmpty(apiUser.Password))
            {
                throw new ArgumentException("Provided password either didn't exist or was empty.");
            }

            if (clientName == "web")
            {
                var registrationDate = DateTime.Now;
                var user = new User
                {
                    Email = apiUser.Email,
                    Password = PasswordEncryptor.CreateMD5(apiUser.Password),
                    CreatedOn = registrationDate
                };
                _userRepository.Add(user);

                return true;
            }

            return false;
        }

        public bool CreateNickname(ApiNickname nickname)
        {
            var isNicknameReserved = _nicknameRepository.GetAll().Any(x => x.Nickname1 == nickname.Nickname);
            if (isNicknameReserved)
            {
                throw new InvalidOperationException($"Nickname {nickname.Nickname} is already in use");
            }

            var maxNicknameLength = _nicknameRepository.GetMaxNicknameLength();
            if (nickname.Nickname.Length > maxNicknameLength)
            {
                throw new InvalidOperationException(
                    $"Given nickname {nickname.Nickname} is too long. Maximum nickname length is {maxNicknameLength.ToString()}");
            }

            var newNickname = new Nickname
            {
                UserId = nickname.UserId,
                Nickname1 = nickname.Nickname,
                Rating = Constants.InitialRatingValue,
                TeamRating = Constants.InitialRatingValue,
                CurrentMonthlyRating = (int)Constants.InitialRatingValue,
                CurrentMonthlyTeamRating = (int)Constants.InitialRatingValue,
            };

            _nicknameRepository.Add(newNickname);

            return true;
        }

        public List<ApiNickname> GetUserNicknames(string token, string clientName)
        {
            var result = new List<ApiNickname>();

            if (clientName == "console")
            {
                var userId = _userRepository.GetAll().First(x => x.TokenConsole == token).Id;
                var dbNicknames = _nicknameRepository.GetAll().Where(x => x.UserId == userId).ToList();

                foreach (var nickname in dbNicknames)
                {
                    result.Add(new ApiNickname
                    {
                        UserId = nickname.UserId,
                        Nickname = nickname.Nickname1
                    });
                }
            }

            return result;
        }

        public int GetUserId(string token, string clientName)
        {
            if (clientName == "web")
            {
                if (!_userRepository.GetAll().Any(x => x.TokenWeb == token))
                {
                    throw new InvalidOperationException("No user was found with given token.");
                }

                var dBUser = _userRepository.GetAll().FirstOrDefault(x => x.TokenWeb == token);

                return dBUser.Id;
            }

            return -1;
        }

        public List<ApiNickModel> GetAllNicknames()
        {
            var output = new List<ApiNickModel>();

            var dbNicknames = _nicknameRepository
                .GetAll()
                .ToList();

            foreach (var nick in dbNicknames)
            {
                output.Add(new ApiNickModel
                {
                    Id = nick.Id,
                    Nick = nick.Nickname1
                });
            }

            return output;
        }

        public string GetMainNickname(string oneOfUserNicknames)
        {
            var mainOneVsOneNick = _oneVsOneViewHandler
                .GetAll()
                .Where(x => x.Nickname.Contains(oneOfUserNicknames))
                .OrderByDescending(x => (x.Wins + x.Loses))
                .FirstOrDefault();

            if (mainOneVsOneNick != null)
            {
                return mainOneVsOneNick.Nickname;
            }

            var teamLadderNick = _teamGamesLadderViewHandler
                .GetAll()
                .Where(x => x.Nickname.Contains(oneOfUserNicknames))
                .OrderByDescending(x => (x.Wins + x.Loses))
                .FirstOrDefault();

            if (teamLadderNick != null)
            {
                return teamLadderNick.Nickname;
            }

            var nickname = _nicknameRepository.GetAll().FirstOrDefault(x => x.Nickname1.Contains(oneOfUserNicknames));
            if (nickname != null)
            {
                return nickname.Nickname1;
            }

            return string.Empty;
        }

        public ApiPlayerWheel GetMainNicknameModel(string email)
        {
            var dbUser = _userRepository.GetAll().FirstOrDefault(x => x.Email == email);
            if (dbUser == null)
            {
                throw new NullReferenceException("No such user was found.");
            }

            var allUserNicknames = _nicknameRepository
                .GetAll()
                .Where(x => x.UserId == dbUser.Id)
                .Select(x => x.Nickname1)
                .ToList();

            var output = new ApiPlayerWheel();

            var mainOneVsOneNick = _oneVsOneViewHandler
                .GetAll()
                .Where(x => allUserNicknames.Contains(x.Nickname))
                .OrderByDescending(x => (x.Wins + x.Loses))
                .FirstOrDefault();

            if (mainOneVsOneNick != null)
            {
                output.Nickname = mainOneVsOneNick.Nickname;
                output.LadderOneRating = (double)mainOneVsOneNick.IndividualRating;
            }

            var teamLadderNick = _teamGamesLadderViewHandler
                .GetAll()
                .Where(x => allUserNicknames.Contains(x.Nickname))
                .OrderByDescending(x => (x.Wins + x.Loses))
                .FirstOrDefault();

            if (teamLadderNick != null)
            {
                output.TeamLaaderRating = (double)teamLadderNick.TeamRating;
            }

            return output;
        }
    }
}