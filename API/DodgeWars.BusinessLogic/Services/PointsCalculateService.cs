﻿using DodgeWars.BusinessLogic.Models;
using DodgeWars.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DodgeWars.BusinessLogic.Services
{
    public static class PointsCalculateService
    {
        private const int powerBasisElo = 10;
        private const double powerExponentDividerElo = 400;
        private const double factorElo = 32;
        private const int _maxPointsAllowedElo = 31;
        private const int _minPointsAllowedElo = 1;

        public static List<CalculationResult> GetIndividualRatingsDeltas(List<Nickname> dbNicknames, List<ApiPlayer> players)
        {
            var result = new List<CalculationResult>();

            CalculateGlobalPointsDelta(dbNicknames, players, ref result);
            CalculateMonthlyPointsDelta(dbNicknames, players, ref result);

            return result;
        }

        public static List<CalculationResult> GetTeamRatingDeltas(List<Nickname> dbNicknames, List<ApiPlayer> players)
        {
            var result = new List<CalculationResult>();

            CalculateTeamGlobalPointsDelta(dbNicknames, players, ref result);
            CalculateTeamMonthlyPointsDelta(dbNicknames, players, ref result);

            return result;
        }

        //Individual

        private static void CalculateGlobalPointsDelta(List<Nickname> dbNicknames, List<ApiPlayer> players, ref List<CalculationResult> result)
        {
            var winnerNickname = players.Find(y => !y.IsLooser).Nickname;
            var loserNickname = players.Find(y => y.IsLooser).Nickname;
            var dbWinner = dbNicknames.Find(x => x.Nickname1 == winnerNickname);
            var dbLoser = dbNicknames.Find(x => x.Nickname1 == loserNickname);

            var winnerRating = (double)dbWinner.Rating;
            var looserRating = (double)dbLoser.Rating;

            var winnerPrice = Math.Pow(powerBasisElo, winnerRating / powerExponentDividerElo);
            var looserPrice = Math.Pow(powerBasisElo, looserRating / powerExponentDividerElo);

            var delta = Math.Abs(factorElo * (1 - (winnerPrice / (winnerPrice + looserPrice))));
            var deltaRoundedUp = decimal.Round((decimal)delta, 0);

            if (deltaRoundedUp > _maxPointsAllowedElo)
            {
                deltaRoundedUp = _maxPointsAllowedElo;
            }
            if (deltaRoundedUp < _minPointsAllowedElo)
            {
                deltaRoundedUp = _minPointsAllowedElo;
            }

            var newDbLoserRating = (double)(dbLoser.Rating - (double)deltaRoundedUp);
            var loserDelta = Math.Floor((double)(dbLoser.Rating - newDbLoserRating));

            if (dbLoser.Rating > Constants.LenienceRatingCap)
            {
                if (dbLoser.Rating - loserDelta < Constants.LenienceRatingCap)
                {
                    loserDelta = Math.Abs((double)dbLoser.Rating - Constants.LenienceRatingCap);
                }
            }
            else
            {
                loserDelta = 0;
            }

            var winnerDelta = (double)deltaRoundedUp + ((double)deltaRoundedUp / Constants.BonusDivisor * Convert.ToInt32(dbWinner.Rating < Constants.BonusMultiplierCap));
            result.Add(new CalculationResult() { NicknameId = dbLoser.Id, GlobalPointsDelta = -(int)loserDelta, IsLoser = true });
            result.Add(new CalculationResult() { NicknameId = dbWinner.Id, GlobalPointsDelta = (int)Math.Ceiling(winnerDelta) });
        }

        private static void CalculateMonthlyPointsDelta(List<Nickname> dbNicknames, List<ApiPlayer> players, ref List<CalculationResult> result)
        {
            var winnerNickname = players.Find(y => !y.IsLooser).Nickname;
            var loserNickname = players.Find(y => y.IsLooser).Nickname;
            var dbWinner = dbNicknames.Find(x => x.Nickname1 == winnerNickname);
            var dbLoser = dbNicknames.Find(x => x.Nickname1 == loserNickname);

            var winnerRating = (double)dbWinner.CurrentMonthlyRating;
            var looserRating = (double)dbLoser.CurrentMonthlyRating;

            var winnerPrice = Math.Pow(powerBasisElo, winnerRating / powerExponentDividerElo);
            var looserPrice = Math.Pow(powerBasisElo, looserRating / powerExponentDividerElo);

            var delta = Math.Abs(factorElo * (1 - (winnerPrice / (winnerPrice + looserPrice))));
            var deltaRoundedUp = decimal.Round((decimal)delta, 0);

            if (deltaRoundedUp > _maxPointsAllowedElo)
            {
                deltaRoundedUp = _maxPointsAllowedElo;
            }
            if (deltaRoundedUp < _minPointsAllowedElo)
            {
                deltaRoundedUp = _minPointsAllowedElo;
            }

            var newDbLoserRating = (double)(dbLoser.CurrentMonthlyRating - (double)deltaRoundedUp);
            var loserDelta = Math.Floor((double)(dbLoser.CurrentMonthlyRating - newDbLoserRating));

            if (dbLoser.CurrentMonthlyRating > Constants.LenienceRatingCap)
            {
                if (dbLoser.CurrentMonthlyRating - loserDelta < Constants.LenienceRatingCap)
                {
                    loserDelta = Math.Abs((double)dbLoser.CurrentMonthlyRating - Constants.LenienceRatingCap);
                }
            }
            else
            {
                loserDelta = 0;
            }

            var winnerDelta = (double)deltaRoundedUp + ((double)deltaRoundedUp / Constants.BonusDivisor * Convert.ToInt32(dbWinner.CurrentMonthlyRating < Constants.BonusMultiplierCap));

            result.Find(x => x.NicknameId == dbLoser.Id).MonthlyPointsDelta = -(int)loserDelta;
            result.Find(x => x.NicknameId == dbWinner.Id).MonthlyPointsDelta = (int)Math.Ceiling(winnerDelta);
        }

        //TEAM

        private static void CalculateTeamGlobalPointsDelta(List<Nickname> dbNicknames, List<ApiPlayer> players, ref List<CalculationResult> result)
        {
            var winnerNicknames = players
                .Where(y => !y.IsLooser)
                .Select(x => x.Nickname)
                .ToList();
            var dbWinners = dbNicknames.Where(x => winnerNicknames.Contains(x.Nickname1)).ToList();
            var sumOfWinnersTeamRating = dbWinners.Select(x => x.TeamRating).Sum();
            var averageWinnersRating = sumOfWinnersTeamRating / dbWinners.Count;

            var loosersNicknames = players
                .Where(y => y.IsLooser)
                .Select(x => x.Nickname)
                .ToList();
            var dbLoosers = dbNicknames.Where(x => loosersNicknames.Contains(x.Nickname1)).ToList();
            var sumOfLoosersTeamRating = dbLoosers.Select(x => x.TeamRating).Sum();
            var averageLoosersRating = sumOfLoosersTeamRating / dbLoosers.Count;

            var winnersTeamPrice = Math.Pow(powerBasisElo, averageWinnersRating / powerExponentDividerElo);
            var loosersTeamPrice = Math.Pow(powerBasisElo, averageLoosersRating / powerExponentDividerElo);

            var delta = Math.Abs(factorElo * (1 - (winnersTeamPrice / (winnersTeamPrice + loosersTeamPrice))));
            var deltaRoundedUp = decimal.Round((decimal)delta, 0);

            if (deltaRoundedUp > _maxPointsAllowedElo)
            {
                deltaRoundedUp = _maxPointsAllowedElo;
            }
            if (deltaRoundedUp < _minPointsAllowedElo)
            {
                deltaRoundedUp = _minPointsAllowedElo;
            }

            SetLosersTeamCalculationResults(dbLoosers, (double)deltaRoundedUp, result);
            SetWinnersTeamCalculationResults(dbWinners, (double)deltaRoundedUp, result);
        }

        private static void CalculateTeamMonthlyPointsDelta(List<Nickname> dbNicknames, List<ApiPlayer> players, ref List<CalculationResult> result)
        {
            var winnerNicknames = players
                .Where(y => !y.IsLooser)
                .Select(x => x.Nickname)
                .ToList();
            var dbWinners = dbNicknames.Where(x => winnerNicknames.Contains(x.Nickname1)).ToList();
            var sumOfWinnersTeamRating = dbWinners.Select(x => x.CurrentMonthlyTeamRating).Sum();
            var averageWinnersRating = sumOfWinnersTeamRating / dbWinners.Count;

            var loosersNicknames = players
                .Where(y => y.IsLooser)
                .Select(x => x.Nickname)
                .ToList();
            var dbLoosers = dbNicknames.Where(x => loosersNicknames.Contains(x.Nickname1)).ToList();
            var sumOfLoosersTeamRating = dbLoosers.Select(x => x.CurrentMonthlyTeamRating).Sum();
            var averageLoosersRating = sumOfLoosersTeamRating / dbLoosers.Count;

            var winnersTeamPrice = Math.Pow(powerBasisElo, (double)(averageWinnersRating / powerExponentDividerElo));
            var loosersTeamPrice = Math.Pow(powerBasisElo, (double)(averageLoosersRating / powerExponentDividerElo));

            var delta = Math.Abs(factorElo * (1 - (winnersTeamPrice / (winnersTeamPrice + loosersTeamPrice))));
            var deltaRoundedUp = decimal.Round((decimal)delta, 0);

            if (deltaRoundedUp > _maxPointsAllowedElo)
            {
                deltaRoundedUp = _maxPointsAllowedElo;
            }
            if (deltaRoundedUp < _minPointsAllowedElo)
            {
                deltaRoundedUp = _minPointsAllowedElo;
            }

            SetLosersMonthlyTeamCalculationResults(dbLoosers, (double)deltaRoundedUp, result);
            SetWinnersMonthlyTeamCalculationResults(dbWinners, (double)deltaRoundedUp, result);
        }

        private static double GetTeamWinnerDelta(double winnerTeamRating, double deltaRoundedUp)
        {
            return Math.Ceiling(
                deltaRoundedUp
                    + (deltaRoundedUp / Constants.BonusDivisor * Convert.ToInt32(winnerTeamRating < Constants.BonusMultiplierCap)));
        }

        private static void SetLosersTeamCalculationResults(List<Nickname> dbLosers, double deltaRoundedUp, List<CalculationResult> calculationResults)
        {
            deltaRoundedUp = Math.Floor(deltaRoundedUp);
            foreach (var loser in dbLosers)
            {
                var loserUpdatedTeamRating = loser.TeamRating;
                if (loser.TeamRating > Constants.LenienceRatingCap)
                {
                    if (loser.TeamRating - deltaRoundedUp < Constants.LenienceRatingCap)
                        loserUpdatedTeamRating = Constants.LenienceRatingCap;
                    else
                        loserUpdatedTeamRating -= deltaRoundedUp;
                }

                var delta = loser.TeamRating - loserUpdatedTeamRating;
                calculationResults.Add(new CalculationResult() { NicknameId = loser.Id, GlobalPointsDelta = -(int)delta, IsLoser = true });
            }
        }

        private static void SetWinnersTeamCalculationResults(List<Nickname> dbWinners, double deltaRoundedUp, List<CalculationResult> calculationResults)
        {
            foreach (var winner in dbWinners)
            {
                var winnerDelta = GetTeamWinnerDelta(winner.TeamRating, deltaRoundedUp);
                calculationResults.Add(new CalculationResult() { NicknameId = winner.Id, GlobalPointsDelta = (int)winnerDelta });
            }
        }

        private static void SetLosersMonthlyTeamCalculationResults(List<Nickname> dbLosers, double deltaRoundedUp, List<CalculationResult> calculationResults)
        {
            deltaRoundedUp = Math.Floor(deltaRoundedUp);
            foreach (var loser in dbLosers)
            {
                var loserUpdatedTeamRating = loser.CurrentMonthlyTeamRating;
                if (loser.CurrentMonthlyTeamRating > Constants.LenienceRatingCap)
                {
                    if (loser.CurrentMonthlyTeamRating - deltaRoundedUp < Constants.LenienceRatingCap)
                        loserUpdatedTeamRating = (int?)Constants.LenienceRatingCap;
                    else
                        loserUpdatedTeamRating -= (int?)deltaRoundedUp;
                }

                var delta = loser.CurrentMonthlyTeamRating - loserUpdatedTeamRating;
                calculationResults.Find(x => x.NicknameId == loser.Id).MonthlyPointsDelta = -(int)delta;
            }
        }

        private static void SetWinnersMonthlyTeamCalculationResults(List<Nickname> dbWinners, double deltaRoundedUp, List<CalculationResult> calculationResults)
        {
            foreach (var winner in dbWinners)
            {
                var winnerDelta = GetTeamWinnerDelta((double)winner.CurrentMonthlyTeamRating, deltaRoundedUp);
                calculationResults.Find(x => x.NicknameId == winner.Id).MonthlyPointsDelta = (int)winnerDelta;
            }
        }
    }
}