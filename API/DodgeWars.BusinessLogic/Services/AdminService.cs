﻿using DodgeWars.DAL.Contracts;
using DodgeWars.DAL.Model;
using DodgeWars.BusinessLogic.Contracts;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using DodgeWars.BusinessLogic.Models;
using System;
using DodgeWars.Tools;

namespace DodgeWars.BusinessLogic.Services
{
    public class AdminService : IAdminService
    {
        private readonly IRepository<Map> _mapRepository;
        private readonly IRepository<Nickname> _nicknameRepository;
        private readonly IRepository<Game> _gameRepository;
        private readonly IRepository<PlayerGame> _playerGameRepository;
        private readonly IRepository<Replay> _replayRepository;
        private readonly IRepository<Rating> _ratingRepository;

        public AdminService(IRepository<Map> mapRepository,
            IRepository<Nickname> nicknameRepository,
            IRepository<Game> gameRepository,
            IRepository<PlayerGame> playerGameRepository,
            IRepository<Replay> replayRepository,
            IRepository<Rating> ratingRepository)
        {
            _mapRepository = mapRepository;
            _nicknameRepository = nicknameRepository;
            _gameRepository = gameRepository;
            _playerGameRepository = playerGameRepository;
            _replayRepository = replayRepository;
            _ratingRepository = ratingRepository;
        }

        public void UploadMapImage(string mapCodeName)
        {
            var entityToUpdate = _mapRepository.GetAll().FirstOrDefault(x => x.CodeName == mapCodeName);
            // TODO: fix the default imagePath ?
            const string imagePath = @"C:\Dział rozwoju\DODGEWARS\obrazki 75x75 proc\erech.bmp";

            var imageBinaries = File.ReadAllBytes(imagePath);
            if (entityToUpdate != null)
            {
                entityToUpdate.Image = imageBinaries;
            }
            _mapRepository.Edit(entityToUpdate);
        }

        public bool UploadAllMaps(List<ApiMap> mapModels)
        {
            var dbMaps = new List<Map>();

            foreach (var mapModel in mapModels)
            {
                dbMaps.Add(new Map()
                {
                    CodeName = mapModel.CodeName,
                    DisplayName = mapModel.DisplayName,
                    ReplayMapTextId = mapModel.ReplayMapTextId,
                    ReplayMapId = MapsIdentifyUtility.GetMapId(mapModel.ReplayMapTextId),
                    Image = mapModel.Image
                });
            }

            _mapRepository.AddRange(dbMaps);

            return true;
        }

        public void ResetPlayersRatings()
        {
            var allNicknames = _nicknameRepository.GetAll().ToList();
            foreach (var nickname in allNicknames)
            {
                nickname.Rating = Constants.InitialRatingValue;
                nickname.TeamRating = Constants.InitialRatingValue;
                nickname.CurrentMonthlyRating = (int)Constants.InitialRatingValue;
                nickname.CurrentMonthlyTeamRating = (int)Constants.InitialRatingValue;
                _nicknameRepository.Edit(nickname);
            }
        }

        public void RemoveAllGames()
        {
            var dbPlayerGames = _playerGameRepository.GetAll();
            foreach (var dbPG in dbPlayerGames)
            {
                _playerGameRepository.Delete(dbPG);
            }

            var dbReplays = _replayRepository.GetAll();
            foreach (var dbR in dbReplays)
            {
                _replayRepository.Delete(dbR);
            }

            var dbGames = _gameRepository.GetAll();
            foreach (var dbG in dbGames)
            {
                _gameRepository.Delete(dbG);
            }

            var dbRatings = _ratingRepository.GetAll();
            foreach (var dbR in dbRatings)
            {
                _ratingRepository.Delete(dbR);
            }
        }

        public List<ApiGame> GetApiGameModelsFromExistingGames()
        {
            var result = new List<ApiGame>();
            var dbGamesSortedByPlayedOn = _gameRepository.GetAll().OrderBy(x => x.PlayedOn).ToList();

            foreach (var dbGame in dbGamesSortedByPlayedOn)
            {
                var dbGamePlayers = dbGame.PlayerGames;
                var apiPlayers = new List<ApiPlayer>();

                foreach (var dbGamePlayer in dbGamePlayers)
                {
                    apiPlayers.Add(new ApiPlayer()
                    {
                        Nickname = dbGamePlayer.Nickname.Nickname1,
                        Team = dbGamePlayer.Team,
                        IsLooser = dbGamePlayer.IsLooser,
                        IsObserver = false,
                        IsGameHost = dbGamePlayer.IsGameHost
                    });
                }

                var replay = _replayRepository.GetAll().FirstOrDefault(x => x.GameId == dbGame.Id);

                result.Add(new ApiGame()
                {
                    GameId = dbGame.Identifier,
                    ReplayMapTextId = dbGame.Map.ReplayMapTextId,
                    PlayedOn = dbGame.PlayedOn,
                    LastReplayFileLength = replay.ReplayFileSize,
                    ReplayFile = replay.File,
                    Players = apiPlayers
                });
            }

            return result;
        }

        public bool IsFirstGameInMonth(ApiGame previousGame, ApiGame gameToCheck)
        {
            if(previousGame == null && gameToCheck != null)
            {
                return true;
            }

            if (previousGame == null || gameToCheck == null)
            {
                throw new ArgumentNullException("At least one of given games was null");
            }

            var monthsDifference = gameToCheck.PlayedOn.Month - previousGame.PlayedOn.Month;

            return monthsDifference > 0;
        }

        public void ResetPlayersCurrentMonthlyRatings()
        {
            var allNicknames = _nicknameRepository.GetAll().ToList();

            foreach (var nickname in allNicknames)
            {
                nickname.CurrentMonthlyRating = (int)Constants.InitialRatingValue;
                nickname.CurrentMonthlyTeamRating = (int)Constants.InitialRatingValue;
                _nicknameRepository.Edit(nickname);
            }
        }
    }
}
