﻿using Common.Logging;
using DodgeWars.BusinessLogic.Contracts;
using DodgeWars.BusinessLogic.Models;
using DodgeWars.DAL.Contracts;
using DodgeWars.DAL.Model;
using DodgeWars.Tools;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DodgeWars.BusinessLogic.Services
{
    public class GameReportService : IGameReportService
    {
        private readonly IRepository<Nickname> _nicknameRepository;
        private readonly IRepository<Game> _gameRepository;
        private readonly IRepository<Map> _mapRepository;
        private readonly IRepository<PlayerGame> _playerGameRepository;
        private readonly IRepository<Replay> _replayRepository;
        private readonly IRepository<Rating> _ratingRepository;
        private readonly IRatingBuilder _ratingBuilder;
        private readonly ILog _logger;

        public GameReportService(IRepository<Nickname> nicknameRepository,
            IRepository<Game> gameRepository,
            IRepository<Map> mapRepository,
            IRepository<PlayerGame> playerGameRepository,
            IRepository<Replay> replayRepository,
            IRepository<Rating> ratingRepository,
            IRatingBuilder ratingBuilder)
        {
            _nicknameRepository = nicknameRepository;
            _gameRepository = gameRepository;
            _mapRepository = mapRepository;
            _playerGameRepository = playerGameRepository;
            _replayRepository = replayRepository;
            _ratingRepository = ratingRepository;
            _ratingBuilder = ratingBuilder;
            _logger = LogManager.GetLogger("GameReportControllerProdLog");
        }

        public Game AddGame(ApiGame apiGame)
        {
            if (apiGame.Players == null || apiGame.Players.Count == 0)
            {
                throw new ArgumentNullException("No players were found.");
            }

            var gameWasReported = GameWasReported(apiGame.GameId);
            if (!gameWasReported)
            {
                var replayFileSize = Convert.ToInt32(apiGame.LastReplayFileLength);
                var gameIdentifier = apiGame.GameId;
                var replayMapId = MapsIdentifyUtility.GetMapId(apiGame.ReplayMapTextId);
                var replayFile = apiGame.ReplayFile;
                var playedOn = apiGame.PlayedOn;

                if (_gameRepository.GetAll().Any(x => x.Identifier == gameIdentifier))
                {
                    throw new InvalidOperationException("Such a game has already been reported in the dodgewars.net system.");
                }

                var map = _mapRepository.GetAll().FirstOrDefault(x => x.ReplayMapId == replayMapId);
                if (map == null)
                {
                    throw new NullReferenceException("Map with given ID was not found. You are unable to play games on this map.");
                }

                var newGame = new Game
                {
                    MapId = map.Id,
                    PlayedOn = playedOn,
                    Identifier = gameIdentifier,
                };
                _gameRepository.Add(newGame);

                var newReplay = new Replay()
                {
                    Game = newGame,
                    File = replayFile,
                    ReplayFileSize = replayFileSize
                };
                _replayRepository.Add(newReplay);

                return newGame;
            }

            throw new InvalidOperationException("Such a game has already been reported in the dodgewars.net system.");
        }

        public void SetNewGameDetails(List<ApiArPlayer> players, Game game)
        {
            foreach (var player in players)
            {
                int nicknameId;
                try
                {
                    nicknameId = _nicknameRepository.GetAll().First(x => x.Nickname1 == player.Nickname).Id;
                }
                catch
                {
                    throw new NullReferenceException(string.Format("Nickname {0} was not found in the dodgewars.net system", player.Nickname));
                }

                _playerGameRepository.Add(new PlayerGame
                {
                    NicknameId = nicknameId,
                    Game = game,
                    Team = player.Team,
                    IsLooser = player.IsLooser,
                    IsGameHost = player.IsGameHost
                });
            }
        }

        public void SetNewGameDetails(List<ApiPlayer> players, Game game)
        {
            foreach (var player in players)
            {
                int nicknameId;
                try
                {
                    nicknameId = _nicknameRepository.GetAll().First(x => x.Nickname1 == player.Nickname).Id;
                }
                catch
                {
                    throw new NullReferenceException(string.Format("Nickname {0} was not found in the dodgewars.net system", player.Nickname));
                }

                _playerGameRepository.Add(new PlayerGame
                {
                    NicknameId = nicknameId,
                    Game = game,
                    Team = player.Team,
                    IsLooser = player.IsLooser,
                    IsGameHost = player.IsGameHost
                });
            }
        }

        public void UpdateIndividualLadderRatings(List<ApiPlayer> players, Game newGame)
        {
            var nicknames = players.Select(x => x.Nickname).ToList();
            var dbNicknames = _nicknameRepository.GetAll().Where(x => nicknames.Contains(x.Nickname1)).ToList();

            var calculationResults = PointsCalculateService.GetIndividualRatingsDeltas(dbNicknames, players);
            var updatedRatingRecords = _ratingBuilder.GetRatingRecords(calculationResults, newGame);
            RateService.UpdateNicknamesIndividualRatings(dbNicknames, calculationResults);

            _logger.Info($"GameReportService.UpdateIndividualLadderRatings(): OccuredOn: {updatedRatingRecords.FirstOrDefault().OccuredOn}, nickname Ids: {string.Join(",", updatedRatingRecords.Select(x => x.NicknameId)) }");
            _ratingRepository.AddRange(updatedRatingRecords.ToList());
        }

        public void UpdateTeamLadderPoints(List<ApiPlayer> players, Game newGame)
        {
            var nicknames = players.Select(x => x.Nickname).ToList();
            var dbNicknamesToUpdate = _nicknameRepository
                .GetAll()
                .Where(x => nicknames.Contains(x.Nickname1))
                .ToList();

            var calculationResults = PointsCalculateService.GetTeamRatingDeltas(dbNicknamesToUpdate, players);
            var updatedRatingRecords = _ratingBuilder.GetRatingRecords(calculationResults, newGame);
            RateService.UpdateNicknamesTeamRatings(dbNicknamesToUpdate, calculationResults);

            _logger.Info($"GameReportService.UpdateTeamLadderPoints(): OccuredOn: {updatedRatingRecords.FirstOrDefault().OccuredOn}, nickname Ids: {string.Join(",", updatedRatingRecords.Select(x => x.NicknameId)) }");
            _ratingRepository.AddRange(updatedRatingRecords.ToList());
        }

        private bool GameWasReported(int gameId)
        {
            var gameIdentifiers = _gameRepository.GetAll().Select(x => x.Identifier).ToList();
            return gameIdentifiers.Contains(gameId);
        }

        public List<ApiMapModel> GetAllMaps()
        {
            var output = new List<ApiMapModel>();

            var dbMaps = _mapRepository
                .GetAll()
                .ToList();

            foreach (var map in dbMaps)
            {
                output.Add(new ApiMapModel
                {
                    Id = map.Id,
                    Name = map.DisplayName
                });
            }

            return output;
        }
    }
}
