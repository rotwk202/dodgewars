﻿using DodgeWars.BusinessLogic.Models;
using System;
using System.Collections.Generic;

namespace DodgeWars.BusinessLogic.Contracts
{
    public interface IAccountService
    {
        bool RegisterUser(ApiUser apiUser, string clientName);
        List<ApiNickname> GetUserNicknames(string tokenModel, string clientName);
        bool CreateNickname(ApiNickname nickname);
        int GetUserId(string token, string clientName);
        List<ApiNickModel> GetAllNicknames();
        ApiPlayerWheel GetMainNicknameModel(string email);
    }
}
