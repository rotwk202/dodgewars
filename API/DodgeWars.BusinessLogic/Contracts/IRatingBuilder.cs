﻿using DodgeWars.BusinessLogic.Models;
using DodgeWars.DAL.Model;
using System.Collections.Generic;

namespace DodgeWars.BusinessLogic.Contracts
{
    public interface IRatingBuilder
    {
        IList<Rating> GetRatingRecords(IList<CalculationResult> calculationResults, Game newGame);
        IList<Rating> GetTeamRatingRecords(IList<CalculationResult> calculationResults, Game newGame);
    }
}
