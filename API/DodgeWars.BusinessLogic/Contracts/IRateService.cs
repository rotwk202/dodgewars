﻿using DodgeWars.BusinessLogic.Models;
using DodgeWars.DAL.Model;
using System.Collections.Generic;

namespace DodgeWars.BusinessLogic.Contracts
{
    public interface IRateService
    {
        List<ApiNickname> GetLadderOneData();
        List<ApiTeamGamesWarriorModel> GetTeamGamesLadderData();
        List<ApiMatchHistory> GetMatchesHistory(string nickname, int pageNumber, int take);
        List<ApiMatchHistory> GetLastGames(int gamesCount);
        byte[] GetMapImage(string mapCodeName);
        byte[] GetReplayFile(int gameId);
        List<Playa> GetMonthlyLadderOneData(string dateString);
        List<Playa> GetMonthlyTeamLadderData(string dateString);
        ApiGamesCountData GetGamesCountData();
    }
}
