﻿using DodgeWars.BusinessLogic.Models;
using System.Collections.Generic;

namespace DodgeWars.BusinessLogic.Contracts
{
    public interface IAdminService
    {
        void UploadMapImage(string mapCodeName);
        bool UploadAllMaps(List<ApiMap> mapModels);
        void ResetPlayersRatings();
        List<ApiGame> GetApiGameModelsFromExistingGames();
        void RemoveAllGames();
        bool IsFirstGameInMonth(ApiGame previousGame, ApiGame gameToCheck);
        void ResetPlayersCurrentMonthlyRatings();
    }
}
