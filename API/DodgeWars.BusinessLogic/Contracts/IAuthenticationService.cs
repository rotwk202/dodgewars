﻿using DodgeWars.BusinessLogic.Models;
using System;

namespace DodgeWars.BusinessLogic.Contracts
{
    public interface IAuthenticationService
    {
        ApiToken GetToken(ApiUser apiUser, string clientName);
        bool IsTokenOutOfDate(string token, string clientName, DateTime requestTime);
    }
}
