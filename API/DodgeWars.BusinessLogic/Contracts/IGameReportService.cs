﻿using DodgeWars.BusinessLogic.Models;
using DodgeWars.DAL.Model;
using System.Collections.Generic;

namespace DodgeWars.BusinessLogic.Contracts
{
    public interface IGameReportService
    {
        Game AddGame(ApiGame apiGame);
        void SetNewGameDetails(List<ApiPlayer> players, Game game);
        void UpdateIndividualLadderRatings(List<ApiPlayer> players, Game newGame);
        void UpdateTeamLadderPoints(List<ApiPlayer> players, Game newGame);
        List<ApiMapModel> GetAllMaps();
        void SetNewGameDetails(List<ApiArPlayer> players, Game game);
    }
}
