﻿using DodgeWars.BusinessLogic.Builders;
using DodgeWars.BusinessLogic.Contracts;
using DodgeWars.BusinessLogic.Services;
using DodgeWars.DAL.Contracts;
using DodgeWars.DAL.Model;
using DodgeWars.DAL.Services;
using log4net;
using Quartz;
using System;

namespace DodgeWars.BusinessLogic.Jobs
{
    public class RecalculateAllGames : IJob
    {
        private IRepository<PlayerGame> _playerGameRepository;
        private IRepository<Game> _gameRepository;
        private IRepository<Nickname> _nicknameRepository;
        private IRepository<Map> _mapRepository;
        private IRepository<Replay> _replayRepository;
        private IRepository<Rating> _ratingRepository;
        private IRepository<RatingType> _ratingTypeRepository;
        private IRepository<EventType> _eventTypeRepository;
        private IRatingBuilder _ratingBuilder;
        private IAdminService _adminService;
        private IGameReportService _reportService;
        private ILog _logger;

        public void Execute(IJobExecutionContext context)
        {
            _logger = LogManager.GetLogger("InfoLogger");
            try
            {
                using (var uow = new UnitOfWork())
                {
                    _logger.Info($"Beginning job RecalculateAllGames at: {DateTime.Now}");

                    _playerGameRepository = new Repository<PlayerGame>(uow);
                    _gameRepository = new Repository<Game>(uow);
                    _nicknameRepository = new Repository<Nickname>(uow);
                    _mapRepository = new Repository<Map>(uow);
                    _replayRepository = new Repository<Replay>(uow);
                    _ratingRepository = new Repository<Rating>(uow);
                    _ratingTypeRepository = new Repository<RatingType>(uow);
                    _eventTypeRepository = new Repository<EventType>(uow);

                    _ratingBuilder = new RatingBuilder(_ratingTypeRepository, _eventTypeRepository);

                    _adminService = new AdminService(null, _nicknameRepository, _gameRepository, _playerGameRepository, _replayRepository, _ratingRepository);
                    _reportService = new GameReportService(_nicknameRepository,
                        _gameRepository,
                        _mapRepository,
                        _playerGameRepository,
                        _replayRepository,
                        _ratingRepository,
                        _ratingBuilder
                        );

                    _adminService.ResetPlayersRatings();
                    uow.Commit();

                    var apiGameList = _adminService.GetApiGameModelsFromExistingGames();

                    _adminService.RemoveAllGames();
                    uow.Commit();

                    _playerGameRepository.ResetAutoIndexing("PlayerGame");
                    _replayRepository.ResetAutoIndexing("Replay");
                    _gameRepository.ResetAutoIndexing("Game");
                    _ratingRepository.ResetAutoIndexing("Rating");
                    uow.Commit();

                    for (int i = 0; i < apiGameList.Count; i++)
                    {
                        var apiGame = apiGameList[i];
                        var newGame = _reportService.AddGame(apiGame);
                        _reportService.SetNewGameDetails(apiGame.Players, newGame);
                        uow.Commit();

                        if (i > 0 && _adminService.IsFirstGameInMonth(apiGameList[i - 1], apiGame))
                        {
                            _adminService.ResetPlayersCurrentMonthlyRatings();
                            uow.Commit();
                        }

                        if (apiGame.Players.Count == 2)
                        {
                            _reportService.UpdateIndividualLadderRatings(apiGame.Players, newGame);
                        }
                        if (apiGame.Players.Count > 2)
                        {
                            _reportService.UpdateTeamLadderPoints(apiGame.Players, newGame);
                        }
                        uow.Commit();
                    }

                    _logger.Info($"RecalculateAllGames job completed succesfully at: {DateTime.Now}");
                }
            }
            catch (Exception ex)
            {
                _logger.Info($"RecalculateAllGames job: exception occured on {DateTime.Now}. Exception details: {ex}");
            }
        }
    }
}
