﻿using Common.Logging;
using DodgeWars.BusinessLogic.Contracts;
using DodgeWars.BusinessLogic.Services;
using DodgeWars.DAL.Contracts;
using DodgeWars.DAL.Model;
using DodgeWars.DAL.Services;
using Quartz;
using System;

namespace DodgeWars.BusinessLogic.Jobs
{
    public class ResetMonthlyRatings : IJob
    {
        private IRepository<PlayerGame> _playerGameRepository;
        private IRepository<Game> _gameRepository;
        private IRepository<Nickname> _nicknameRepository;
        private IRepository<Replay> _replayRepository;
        private IRepository<Rating> _ratingRepository;
        private IAdminService _adminService;
        private ILog _logger;

        public void Execute(IJobExecutionContext context)
        {
            _logger = LogManager.GetLogger("InfoLogger");
            try
            {
                using (var uow = new UnitOfWork())
                {
                    _playerGameRepository = new Repository<PlayerGame>(uow);
                    _gameRepository = new Repository<Game>(uow);
                    _nicknameRepository = new Repository<Nickname>(uow);
                    _replayRepository = new Repository<Replay>(uow);
                    _ratingRepository = new Repository<Rating>(uow);
                    _adminService = new AdminService(null, _nicknameRepository, _gameRepository, _playerGameRepository, _replayRepository, _ratingRepository);

                    _adminService.ResetPlayersCurrentMonthlyRatings();
                    uow.Commit();
                }
            }
            catch(Exception e)
            {
                _logger.Info($"ResetMonthlyRatings job: exception occured on {DateTime.Now}. Exception details: {e}");
            }
        }
    }
}
