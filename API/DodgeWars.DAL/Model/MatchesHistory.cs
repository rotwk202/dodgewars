//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DodgeWars.DAL.Model
{
    using System;
    using System.Collections.Generic;
    using Contracts;
    public partial class MatchesHistory : IEntity
    {
        public int Id { get; set; }
        public string Nickname { get; set; }
        public int Game_ID { get; set; }
        public string Map { get; set; }
        public bool Won { get; set; }
        public int Points_gained { get; set; }
    }
}
