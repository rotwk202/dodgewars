﻿namespace DodgeWars.DAL.Model
{
    public class Playa
    {
        public string Nickname { get; set; }
        public int Wins { get; set; }
        public int Loses { get; set; }
        public int MonthlyRating { get; set; }
    }
}
