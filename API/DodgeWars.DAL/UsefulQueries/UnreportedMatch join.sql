﻿select um.Id as 'UnreportedMatchId', n.Nickname, umd.IsRaisedAgainst
from UnreportedMatch um
join UnreportedMatchDetails umd on um.Id = umd.UnreportedMatchId
join Nickname n on n.Id = umd.NicknameId