﻿declare @nickToWorkOn nvarchar(10) = 'OhReaLLy'

declare @gameIdToKeep int;
set @gameIdToKeep =
(
	select top 1 g.Id
	from Game g
	join PlayerGame pg on g.Id = pg.GameId
	where NicknameId = (select Id from Nickname where Nickname like @nickToWorkOn)
	order by g.Id desc
)
declare @todayMinusFourteenDays DateTime = '2017-04-05 22:00:00'

delete from
PlayerGame
where GameId in
(
	select g.Id
	from Game g
	join PlayerGame pg on g.Id = pg.GameId
	where NicknameId = (select Id from Nickname where Nickname like @nickToWorkOn)
)
and GameId not in (@gameIdToKeep)

delete from Game
where Id in
(
	select g.Id
	from Game g
	join PlayerGame pg on g.Id = pg.GameId
	where NicknameId = (select Id from Nickname where Nickname like @nickToWorkOn)
)
and Id not in (@gameIdToKeep)

update Game
set PlayedOn = @todayMinusFourteenDays
where Id = @gameIdToKeep