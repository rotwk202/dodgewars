﻿using DodgeWars.DAL.Model;

namespace DodgeWars.DAL.Contracts
{
    public interface IUnitOfWork
    {
        DodgeWarsEntities DbContext { get; set; }
        void Commit();
    }
}
