﻿using DodgeWars.DAL.Model;
using System.Collections.Generic;
using System.Linq;

namespace DodgeWars.DAL.Contracts
{
    public interface IRepository<T> where T : class, IEntity
    {
        IQueryable<T> GetAll();
        T GetById(int id);
        void Delete(T entity);
        void Add(T entity);
        void Edit(T entity);
        void AddRange(List<T> entities);
        int GetMaxNicknameLength();
        List<Playa> GetMonthlyLadderOneData(string yearMonthPattern, int playersInitialRating);
        List<Playa> GetMonthlyTeamLadderData(string yearMonthPattern, int playersInitialRating);
        void ResetAutoIndexing(string tableName);
    }
}
