﻿using DodgeWars.DAL.Contracts;
using DodgeWars.DAL.Model;
using System;

namespace DodgeWars.DAL.Services
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        public DodgeWarsEntities DbContext { get; set; }

        public UnitOfWork()
        {
            DbContext = new DodgeWarsEntities();
        }

        public void Commit()
        {
            DbContext.SaveChanges();
        }

        public void Dispose()
        {
            DbContext.Dispose();
            DbContext = null;
        }
    }
}
