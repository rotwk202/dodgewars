﻿using DodgeWars.DAL.Contracts;
using DodgeWars.DAL.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;

namespace DodgeWars.DAL.Services
{
    public class Repository<T> : IRepository<T> where T : class, IEntity
    {
        private readonly IUnitOfWork _unitOfWork;

        public Repository(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IQueryable<T> GetAll()
        {
            return _unitOfWork.DbContext.Set<T>();
        }

        public T GetById(int id)
        {
            return _unitOfWork.DbContext.Set<T>().Find(id);
        }

        public void Delete(T entity)
        {
            _unitOfWork.DbContext.Set<T>().Attach(entity);
            _unitOfWork.DbContext.Set<T>().Remove(entity);
            _unitOfWork.DbContext.Entry(entity).State = EntityState.Deleted;
        }

        public void Add(T entity)
        {
            _unitOfWork.DbContext.Set<T>().Add(entity);
        }

        public void AddRange(List<T> entities)
        {
            _unitOfWork.DbContext.Set<T>().AddRange(entities);
        }

        public void Edit(T entity)
        {
            _unitOfWork.DbContext.Set<T>().Attach(entity);
            _unitOfWork.DbContext.Entry(entity).State = EntityState.Modified;
        }

        public int GetMaxNicknameLength()
        {
            return _unitOfWork.DbContext.Database.SqlQuery<short>("SELECT COL_LENGTH('Nickname','Nickname') as 'NVarChar'").FirstOrDefault() / 2;
        }

        public List<Playa> GetMonthlyLadderOneData(string yearMonthPattern, int playersInitialRating)
        {
            var parameters = new List<SqlParameter> {
                new SqlParameter("@YearMonthPatternParam", yearMonthPattern),
                new SqlParameter("@PlayersInitialRatingParam", playersInitialRating),
            };

            var sql = $@"DECLARE @RC int;
                        DECLARE @yearMonthPattern nvarchar(7);
                        DECLARE @playersInitialRating int;

                        set @yearMonthPattern = @YearMonthPatternParam;
                        set @playersInitialRating = @PlayersInitialRatingParam;

                        EXECUTE @RC = [dbo].[GetMonthlyIndividualLadder] 
                           @yearMonthPattern
                          ,@playersInitialRating
                        ";

            return _unitOfWork.DbContext.Database.SqlQuery<Playa>(sql, parameters.ToArray()).ToList();
        }

        public List<Playa> GetMonthlyTeamLadderData(string yearMonthPattern, int playersInitialRating)
        {
            var parameters = new List<SqlParameter> {
                new SqlParameter("@YearMonthPatternParam", yearMonthPattern),
                new SqlParameter("@PlayersInitialRatingParam", playersInitialRating),
            };

            var sql = $@"DECLARE @RC int;
                        DECLARE @yearMonthPattern nvarchar(7);
                        DECLARE @playersInitialRating int;

                        set @yearMonthPattern = @YearMonthPatternParam;
                        set @playersInitialRating = @PlayersInitialRatingParam;

                        EXECUTE @RC = [dbo].[GetMonthlyTeamLadder] 
                           @yearMonthPattern
                          ,@playersInitialRating
                        ";

            return _unitOfWork.DbContext.Database.SqlQuery<Playa>(sql, parameters.ToArray()).ToList();
        }

        public void ResetAutoIndexing(string tableName)
        {
            _unitOfWork.DbContext.Database.ExecuteSqlCommand($"DBCC CHECKIDENT ('[{tableName}]', RESEED, 0);");
        }
    }
}
