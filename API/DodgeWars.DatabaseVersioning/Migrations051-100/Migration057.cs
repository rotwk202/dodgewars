﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations051_100
{
    [Migration(57, "Update monthly ladder procedures to read the data from Rating table.")]
    public class Migration057 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"ALTER PROCEDURE [dbo].[GetMonthlyIndividualLadder] @yearMonthPattern nvarchar(7), @playersInitialRating int
AS
SELECT
    FORMAT(g.PlayedOn, 'yyyy-MM') as yearMonth,
    n.Nickname,
    SUM(CASE WHEN pg.IsLooser = 0 THEN 1 ELSE 0 END) AS Wins,
    SUM(CASE WHEN pg.IsLooser = 1 THEN 1 ELSE 0 END) as Loses,
    CASE
	WHEN SUM(ra.MonthlyPointsDelta) + @playersInitialRating < 1000 THEN 1000
		ELSE SUM(ra.MonthlyPointsDelta) + @playersInitialRating
	END AS MonthlyRating
FROM
    PlayerGame AS pg 
    LEFT JOIN Game AS g on g.Id = pg.GameId
    LEFT JOIN Nickname AS n ON n.Id = pg.NicknameId
	LEFT JOIN Rating ra ON ra.NicknameId = pg.NicknameId
		AND ra.OccuredOn = g.PlayedOn
		AND ra.EventTypeId = (select Id from EventType where Name = 'Game')
WHERE
    ra.RatingTypeId = (select [Id] from RatingType where [Name] = 'Individual')
    AND FORMAT(g.PlayedOn, 'yyyy-MM') = @yearMonthPattern
GROUP BY 
  FORMAT(g.PlayedOn, 'yyyy-MM'),
  n.Nickname
ORDER BY 
  1,
  MonthlyRating DESC,
  n.Nickname

GO

USE [restore.webio-prod-db.dodgewars]
GO

ALTER PROCEDURE [dbo].[GetMonthlyTeamLadder] @yearMonthPattern nvarchar(7), @playersInitialRating int
AS
SELECT
    FORMAT(g.PlayedOn, 'yyyy-MM') as yearMonth,
    n.Nickname,
    SUM(CASE WHEN pg.IsLooser = 0 THEN 1 ELSE 0 END) AS Wins,
    SUM(CASE WHEN pg.IsLooser = 1 THEN 1 ELSE 0 END) as Loses,
    CASE
	WHEN SUM(ra.MonthlyPointsDelta) + @playersInitialRating < 1000 THEN 1000
		ELSE SUM(ra.MonthlyPointsDelta) + @playersInitialRating
	END AS MonthlyRating
FROM
    PlayerGame AS pg 
    LEFT JOIN Game AS g on g.Id = pg.GameId
    LEFT JOIN Nickname AS n ON n.Id = pg.NicknameId
	LEFT JOIN Rating ra ON ra.NicknameId = pg.NicknameId
		AND ra.OccuredOn = g.PlayedOn
		AND ra.EventTypeId = (select Id from EventType where Name = 'Game')
WHERE
	ra.RatingTypeId = (select [Id] from RatingType where [Name] = 'Team')
	AND FORMAT(g.PlayedOn, 'yyyy-MM') = @yearMonthPattern
GROUP BY 
  FORMAT(g.PlayedOn, 'yyyy-MM'),
  n.Nickname
ORDER BY 
  1,
  MonthlyRating DESC,
  n.Nickname

GO
";

            Execute.Sql(sql);
        }
    }
}