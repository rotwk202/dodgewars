﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations051_100
{
    [Migration(54, "Introduce Replay table. Move Replay binaries from Game to Replay table.")]
    public class Migration054 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"CREATE TABLE [dbo].[Replay] 
(Id int NOT NULL identity(1,1), 
GameId int NOT null, 
ReplayFileSize int NOT null, 
constraint PK_Replay primary key (Id)) 
 
alter table [dbo].[Replay]
add constraint FK_Replay_Game
foreign key (GameId) references [Game](Id) 

ALTER TABLE [dbo].[Replay]
ADD [File] varbinary(max) NOT null 

DECLARE @replayFileSize int;
DECLARE @replayFile varbinary(max);

DECLARE @counter int;
SET @counter = (SELECT top(1) Id FROM Game ORDER by id asc);

WHILE @counter <= (SELECT top(1) Id FROM Game ORDER by id desc)
BEGIN

	SET @replayFileSize = (SELECT ReplayFileSize FROM Game where Id = @counter);
	SET @replayFile = (SELECT ReplayFile FROM Game where Id = @counter);

	INSERT INTO Replay (GameId, ReplayFileSize, [File])
	VALUES (@counter, @replayFileSize, @replayFile);
	
	SET @counter = (SELECT TOP 1 Id FROM Game WHERE Id > @counter)
end

ALTER TABLE [dbo].[Game]
DROP COLUMN [Description]

ALTER TABLE [dbo].[Game]
DROP COLUMN [ReplayFile]

ALTER TABLE [dbo].[Game]
DROP COLUMN [ReplayFileSize]

ALTER TABLE [dbo].[Game]
DROP COLUMN [ReplayName] 

GO

ALTER VIEW [dbo].[AllMatchesHistory]
AS 
select g.PlayedOn as 'PlayedOn',
	   n.Nickname as 'Nickname',
	   g.Identifier as 'Game ID',
	   m.CodeName as 'MapCodeName',
	   m.DisplayName as 'MapDisplayName',
	   re.ReplayFileSize as 'ReplayFileSize',
	   ~(pg.IsLooser) 'Won',
	   pg.PointsGained as 'PointsGained',
	   pg.IsGameHost as 'IsGameHost'
from PlayerGame pg
join Game g on pg.GameId = g.Id
join Map m on g.MapId = m.Id
join Nickname n on n.Id = pg.NicknameId
JOIN Replay re ON re.GameId = g.Id 

GO
";

            Execute.Sql(sql);
        }
    }
}