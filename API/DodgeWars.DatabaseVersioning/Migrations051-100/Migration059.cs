﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations051_100
{
    [Migration(59, "Update all maps ids.")]
    public class Migration059 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"update [dbo].[Map] set ReplayMapTextId = 'map mp anfalas ii' where CodeName = 'AnfalasII'
update [dbo].[Map] set ReplayMapId = 'C47A642D90C540A37B3F2C241B0C2D6D' where CodeName = 'AnfalasII'
update [dbo].[Map] set ReplayMapTextId = 'map mp anorien' where CodeName = 'Anorien'
update [dbo].[Map] set ReplayMapId = '54737221EA826D7A94B3754911F0582D' where CodeName = 'Anorien'
update [dbo].[Map] set ReplayMapTextId = 'map mp anorien ii' where CodeName = 'AnorienII'
update [dbo].[Map] set ReplayMapId = 'D469CA5A5B34CAA643BC720B82B0915C' where CodeName = 'AnorienII'
update [dbo].[Map] set ReplayMapTextId = 'map mp azanulbizar valley' where CodeName = 'AzalnubizarValley'
update [dbo].[Map] set ReplayMapId = 'C1EF32B1A1335DB2F71716DD70FC501E' where CodeName = 'AzalnubizarValley'
update [dbo].[Map] set ReplayMapTextId = 'map wor buckland' where CodeName = 'Buckland'
update [dbo].[Map] set ReplayMapId = 'F94360BCFFC6CA3558BB18DA4DA5DA0D' where CodeName = 'Buckland'
update [dbo].[Map] set ReplayMapTextId = 'map mp dale' where CodeName = 'Dale'
update [dbo].[Map] set ReplayMapId = '41C72153792AC8EDB731323C789B7108' where CodeName = 'Dale'
update [dbo].[Map] set ReplayMapTextId = 'map mp druadan forest' where CodeName = 'DruadanForest'
update [dbo].[Map] set ReplayMapId = 'B243B491C050BB610E2F1F52A225164D' where CodeName = 'DruadanForest'
update [dbo].[Map] set ReplayMapTextId = 'map mp erech' where CodeName = 'Erech'
update [dbo].[Map] set ReplayMapId = '004B9B131AE9A780CE29B247A4C51DB2' where CodeName = 'Erech'
update [dbo].[Map] set ReplayMapTextId = 'map mp erech ii' where CodeName = 'ErechII'
update [dbo].[Map] set ReplayMapId = 'FCF049CFD04CA2AC90C14C2D0EC89177' where CodeName = 'ErechII'
update [dbo].[Map] set ReplayMapTextId = 'map mp eregion' where CodeName = 'Eregion'
update [dbo].[Map] set ReplayMapId = '7041A57F70FD7F68CF06C0DC3FFC25D4' where CodeName = 'Eregion'
update [dbo].[Map] set ReplayMapTextId = 'map mp far harad' where CodeName = 'FarHarad'
update [dbo].[Map] set ReplayMapId = 'B12245FA4AEBA415776AC8B845E32072' where CodeName = 'FarHarad'
update [dbo].[Map] set ReplayMapTextId = 'map mp fords of isen' where CodeName = 'FordsOfIsen'
update [dbo].[Map] set ReplayMapId = 'A8DA63A2343B1818262829C9F64684D4' where CodeName = 'FordsOfIsen'
update [dbo].[Map] set ReplayMapTextId = 'map mp fords of rohan' where CodeName = 'FordsOfRohan'
update [dbo].[Map] set ReplayMapId = 'B764EBDBA4B61C642183BA919E265235' where CodeName = 'FordsOfRohan'
update [dbo].[Map] set ReplayMapTextId = 'map mp frostfall canyon' where CodeName = 'FrostFallCanyon'
update [dbo].[Map] set ReplayMapId = '6C46EBA1C4B4B8A0B988222B57531BBE' where CodeName = 'FrostFallCanyon'
update [dbo].[Map] set ReplayMapTextId = 'map mp greenleaf forest' where CodeName = 'GreenleafForest'
update [dbo].[Map] set ReplayMapId = '1429D34F73A5C46B9EDB37A8ACDEDAAA' where CodeName = 'GreenleafForest'
update [dbo].[Map] set ReplayMapTextId = 'map mp nanduhirion' where CodeName = 'Nanduhirion'
update [dbo].[Map] set ReplayMapId = '4CF38763BFCD8B00B5275979E9F963A0' where CodeName = 'Nanduhirion'
update [dbo].[Map] set ReplayMapTextId = 'map mp ost-in-edhil' where CodeName = 'OstInEdhil'
update [dbo].[Map] set ReplayMapId = 'A7C026B034D1E56F23CC1F1EFD3508A9' where CodeName = 'OstInEdhil'
update [dbo].[Map] set ReplayMapTextId = 'map mp plains of rohan' where CodeName = 'PlainsOfRohan'
update [dbo].[Map] set ReplayMapId = '0FA5CB5D1A536AFF2897D10B288C1A3E' where CodeName = 'PlainsOfRohan'
update [dbo].[Map] set ReplayMapTextId = 'map mp river running' where CodeName = 'RiverRunning'
update [dbo].[Map] set ReplayMapId = '1B8A99A2C177579095C66CA11E693DE5' where CodeName = 'RiverRunning'
update [dbo].[Map] set ReplayMapTextId = 'map wor rohan' where CodeName = 'Rohan'
update [dbo].[Map] set ReplayMapId = '2C805BEE3A9E0A3AD943072963B7226C' where CodeName = 'Rohan'
update [dbo].[Map] set ReplayMapTextId = 'map mp rohan fortifications' where CodeName = 'RohanFortifications'
update [dbo].[Map] set ReplayMapId = 'EBF0499B549C636F914709937B3B5B5C' where CodeName = 'RohanFortifications'
update [dbo].[Map] set ReplayMapTextId = 'map mp rohan ii' where CodeName = 'RohanII'
update [dbo].[Map] set ReplayMapId = '80764D80A0DDC042C1DA09EB9A3FE2B4' where CodeName = 'RohanII'
update [dbo].[Map] set ReplayMapTextId = 'map mp stonewain valley' where CodeName = 'StonevainValley'
update [dbo].[Map] set ReplayMapId = '091D36DB61A1EF485B890431FA1593F1' where CodeName = 'StonevainValley'
update [dbo].[Map] set ReplayMapTextId = 'map mp tombs of karna ii' where CodeName = 'TombsOfKarnaII'
update [dbo].[Map] set ReplayMapId = '26E683C246A7A7BFD9654837319A2FEF' where CodeName = 'TombsOfKarnaII'
update [dbo].[Map] set ReplayMapTextId = 'map wor tower hills' where CodeName = 'TowerHills'
update [dbo].[Map] set ReplayMapId = '05CC4713A2CA1D341984C9ACB7278EAD' where CodeName = 'TowerHills'
update [dbo].[Map] set ReplayMapTextId = 'map mp udun' where CodeName = 'Udun'
update [dbo].[Map] set ReplayMapId = 'AAC2658E10409CDD779A22B9AB0FA3B7' where CodeName = 'Udun'
update [dbo].[Map] set ReplayMapTextId = 'map mp wold' where CodeName = 'Wold'
update [dbo].[Map] set ReplayMapId = 'AB9B2B0E41651C78FA0DC009EEDF4A0D' where CodeName = 'Wold'
";

            Execute.Sql(sql);
        }
    }
}