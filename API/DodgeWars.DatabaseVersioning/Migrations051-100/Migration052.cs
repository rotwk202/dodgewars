﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations051_100
{
    [Migration(52, "Change monthly procedures to not go below 1000 points.")]
    public class Migration052 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"ALTER PROCEDURE [dbo].[GetMonthlyIndividualLadder] @yearMonthPattern nvarchar(7), @playersInitialRating int
AS
SELECT
    FORMAT(g.PlayedOn, 'yyyy-MM') as yearMonth,
    n.Nickname,
    SUM(CASE WHEN pg.IsLooser = 0 THEN 1 ELSE 0 END) AS Wins,
    SUM(CASE WHEN pg.IsLooser = 1 THEN 1 ELSE 0 END) as Loses,
    CASE
	WHEN SUM(pg.PointsGained) + @playersInitialRating < 1000 THEN 1000
		ELSE SUM(pg.PointsGained) + @playersInitialRating
	END AS MonthlyRating
FROM
    PlayerGame AS pg 
    LEFT JOIN Game AS g on g.Id = pg.GameId
    LEFT JOIN Nickname AS n ON n.Id = pg.NicknameId
    WHERE
    NOT EXISTS 
    (
        SELECT 1 FROM PlayerGame WHERE PlayerGame.GameId = pg.GameId  GROUP BY GameID HAVING count(DISTINCT(NicknameID)) > 2
    ) 
    AND FORMAT(g.PlayedOn, 'yyyy-MM') = @yearMonthPattern
GROUP BY 
  FORMAT(g.PlayedOn, 'yyyy-MM'),
  n.Nickname
ORDER BY 
  1,
  MonthlyRating DESC,
  n.Nickname
GO

ALTER PROCEDURE [dbo].[GetMonthlyTeamLadder] @yearMonthPattern nvarchar(7), @playersInitialRating int
AS
SELECT
    FORMAT(g.PlayedOn, 'yyyy-MM') as yearMonth,
    n.Nickname,
    SUM(CASE WHEN pg.IsLooser = 0 THEN 1 ELSE 0 END) AS Wins,
    SUM(CASE WHEN pg.IsLooser = 1 THEN 1 ELSE 0 END) as Loses,
    CASE 
		WHEN SUM(pg.PointsGained) + @playersInitialRating < 1000 THEN 1000
		ELSE SUM(pg.PointsGained) + @playersInitialRating
	END AS MonthlyRating
FROM
    PlayerGame AS pg 
    LEFT JOIN Game AS g on g.Id = pg.GameId
    LEFT JOIN Nickname AS n ON n.Id = pg.NicknameId
    WHERE
    NOT EXISTS 
    (
        SELECT 1 FROM PlayerGame WHERE PlayerGame.GameId = pg.GameId  GROUP BY GameID HAVING count(DISTINCT(NicknameID)) = 2
    ) 
    AND FORMAT(g.PlayedOn, 'yyyy-MM') = @yearMonthPattern
GROUP BY 
  FORMAT(g.PlayedOn, 'yyyy-MM'),
  n.Nickname
ORDER BY 
  1,
  MonthlyRating DESC,
  n.Nickname
";

            Execute.Sql(sql);
        }
    }
}