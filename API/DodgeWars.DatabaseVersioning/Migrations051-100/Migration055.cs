﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations051_100
{
    [Migration(55, "Introduce Rating table. Update related views and procedures.")]
    public class Migration055 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"/*
CREATE RATING TABLES
*/
create table [dbo].[EventType] 
(Id int identity(1,1) not null, 
[Name] varchar(30) not null 
 
constraint PK_EventType primary key (Id) 
) 
 
create table [dbo].[RatingType] 
(Id int identity(1,1) not null, 
[Name] varchar(15) not null 
 
constraint PK_RatingType primary key (Id) 
) 
 
create table [dbo].[Rating] 
(Id int identity(1,1) not null, 
EventTypeId int not null, 
NicknameId int not null, 
RatingTypeId int not null, 
PointsDelta int not null 
 
constraint PK_Rating primary key (Id) 
) 
 
alter table [dbo].[Rating] 
add constraint FK_Rating_EventType 
foreign key (EventTypeId) references [EventType](Id) 
 
alter table [dbo].[Rating] 
add constraint FK_Rating_Nickname 
foreign key (NicknameId) references [Nickname](Id) 
 
alter table [dbo].[Rating] 
add constraint FK_Rating_RatingType 
foreign key (RatingTypeId) references [RatingType](Id) 
 
INSERT INTO EventType ([Name]) 
VALUES ('Game'), ('Inactivity Penalty') 
 
INSERT INTO RatingType ([Name]) 
VALUES ('Individual'), ('Team') 
 
ALTER TABLE [dbo].[Rating] 
ADD OccuredOn [DateTime] NOT NULL 
Default GETDATE() 
GO 

/*
TEMP TABLE FIND 1V1 GAMES AND TEAM GAMES
*/

ALTER TABLE PlayerGame 
ADD LadderType varchar(30) 
GO

CREATE TABLE #Tmp 
(Id int NOT NULL identity(1,1), 
GameId int) 
 
--find 1v1 game IDs 
INSERT INTO #Tmp (GameId) 
select GameId 
from PlayerGame 
group by GameId 
having count( distinct NicknameId ) = 2 

DECLARE @counter int; 
SET @counter = (SELECT TOP 1 GameId FROM #Tmp t) 

WHILE @counter <= (SELECT top(1) GameId FROM #Tmp ORDER by id desc) 
BEGIN 
 
	UPDATE PlayerGame 
	SET LadderType = 'Individual' 
	WHERE PlayerGame.GameId = @counter 
	 
	SET @counter = (SELECT TOP 1 GameId FROM #Tmp WHERE GameId > @counter) 
end 
 
drop table #Tmp 
 
UPDATE PlayerGame 
SET PlayerGame.LadderType = 'Team' 
WHERE LadderType IS null

/*
MOVE DATA
*/

declare @playerGameId int;
SET @playerGameId = (SELECT TOP 1 Id FROM PlayerGame) 
 
DECLARE @eventTypeId int; 
SET @eventTypeId = (SELECT [Id] FROM EventType WHERE [Name] = 'Game') 
DECLARE @individualRatingTypeId int; 
SET @individualRatingTypeId = (SELECT [Id] FROM RatingType WHERE [Name] = 'Individual') 
DECLARE @teamRatingTypeId int; 
SET @teamRatingTypeId = (SELECT [Id] FROM RatingType WHERE [Name] = 'Team') 
 
DECLARE @ratingType int; 
DECLARE @nicknameId int; 
DECLARE @pointsDelta int; 
DECLARE @occuredOn datetime; 
 
WHILE @playerGameId <= (SELECT top(1) Id FROM PlayerGame ORDER by id desc) 
BEGIN 
 
	SET @ratingType = (SELECT [Id] FROM RatingType WHERE [Name] = (SELECT LadderType FROM PlayerGame WHERE Id = @playerGameId)); 
	SET @nicknameId = (SELECT [NicknameId] FROM PlayerGame WHERE Id = @playerGameId); 
	SET @pointsDelta = (SELECT [PointsGained] FROM PlayerGame WHERE Id = @playerGameId); 
	SET @occuredOn = (SELECT g.PlayedOn FROM PlayerGame pg JOIN Game g ON pg.GameId = g.Id WHERE pg.Id = @playerGameId); 
	INSERT INTO Rating (EventTypeId, NicknameId, RatingTypeId, PointsDelta, OccuredOn) 
	VALUES (@eventTypeId, @nicknameId, @ratingType, @pointsDelta, @occuredOn) 
	 
	SET @playerGameId = (SELECT TOP 1 Id FROM PlayerGame WHERE Id > @playerGameId) 
END 

ALTER TABLE [dbo].[PlayerGame] 
DROP COLUMN LadderType 

GO

ALTER VIEW [dbo].[AllMatchesHistory]
AS 
SELECT g.PlayedOn as 'PlayedOn',
	   n.Nickname as 'Nickname',
	   g.Identifier as 'Game ID',
	   m.CodeName as 'MapCodeName',
	   m.DisplayName as 'MapDisplayName',
	   re.ReplayFileSize as 'ReplayFileSize',
	   ~(pg.IsLooser) 'Won',
	   ra.PointsDelta as 'PointsGained',
	   pg.IsGameHost as 'IsGameHost'
from PlayerGame pg
 join Game g on pg.GameId = g.Id
 join Map m on g.MapId = m.Id
 join Nickname n on n.Id = pg.NicknameId
 JOIN Replay re ON re.GameId = g.Id 
join Rating ra ON ra.NicknameId = pg.NicknameId AND ra.OccuredOn = g.PlayedOn

GO

ALTER VIEW [dbo].[MatchesHistory]
AS 
select n.Nickname as 'Nickname',
	   g.Identifier as 'Game ID',
	   m.DisplayName as 'Map',
	   ~(pg.IsLooser) 'Won',
	   ra.PointsDelta as 'Points gained'
from PlayerGame pg
join Game g on pg.GameId = g.Id
join Map m on g.MapId = m.Id
join Nickname n on n.Id = pg.NicknameId
join Rating ra ON ra.NicknameId = pg.NicknameId
	AND ra.OccuredOn = g.PlayedOn
	and ra.EventTypeId = (select Id from EventType where [Name] = 'Game')

GO

ALTER VIEW [dbo].[OneVsOneLadder]
AS
SELECT
    ISNULL(n.Nickname, '') as Nickname,
    SUM(CASE WHEN pg.IsLooser = 0 THEN 1 ELSE 0 END) AS Wins,
    SUM(CASE WHEN pg.IsLooser = 1 THEN 1 ELSE 0 END) as Loses,
    CASE
	WHEN SUM(ra.PointsDelta) + 1000 < 1000 THEN 1000
		ELSE SUM(ra.PointsDelta) + 1000
	END AS IndividualRating
FROM
    PlayerGame AS pg 
    LEFT JOIN Game AS g on g.Id = pg.GameId
    LEFT JOIN Nickname AS n ON n.Id = pg.NicknameId
	LEFT JOIN Rating ra ON ra.NicknameId = pg.NicknameId
		AND ra.OccuredOn = g.PlayedOn
WHERE
	ra.RatingTypeId = (select [Id] from RatingType where [Name] = 'Individual')
GROUP BY 
  Nickname

GO

ALTER VIEW [dbo].[TeamGamesLadder]
AS
SELECT
    ISNULL(n.Nickname, '') as Nickname,
    SUM(CASE WHEN pg.IsLooser = 0 THEN 1 ELSE 0 END) AS Wins,
    SUM(CASE WHEN pg.IsLooser = 1 THEN 1 ELSE 0 END) as Loses,
    CASE
	WHEN SUM(ra.PointsDelta) + 1000 < 1000 THEN 1000
		ELSE SUM(ra.PointsDelta) + 1000
	END AS TeamRating
FROM
    PlayerGame AS pg 
    LEFT JOIN Game AS g on g.Id = pg.GameId
    LEFT JOIN Nickname AS n ON n.Id = pg.NicknameId
	LEFT JOIN Rating ra ON ra.NicknameId = pg.NicknameId
		AND ra.OccuredOn = g.PlayedOn
WHERE
	ra.RatingTypeId = (select [Id] from RatingType where [Name] = 'Team')
GROUP BY 
  Nickname

GO

ALTER PROCEDURE [dbo].[GetMonthlyIndividualLadder] @yearMonthPattern nvarchar(7), @playersInitialRating int
AS
SELECT
    FORMAT(g.PlayedOn, 'yyyy-MM') as yearMonth,
    n.Nickname,
    SUM(CASE WHEN pg.IsLooser = 0 THEN 1 ELSE 0 END) AS Wins,
    SUM(CASE WHEN pg.IsLooser = 1 THEN 1 ELSE 0 END) as Loses,
    CASE
	WHEN SUM(ra.PointsDelta) + @playersInitialRating < 1000 THEN 1000
		ELSE SUM(ra.PointsDelta) + @playersInitialRating
	END AS MonthlyRating
FROM
    PlayerGame AS pg 
    LEFT JOIN Game AS g on g.Id = pg.GameId
    LEFT JOIN Nickname AS n ON n.Id = pg.NicknameId
	LEFT JOIN Rating ra ON ra.NicknameId = pg.NicknameId
		AND ra.OccuredOn = g.PlayedOn
		AND ra.EventTypeId = (select Id from EventType where Name = 'Game')
WHERE
    ra.RatingTypeId = (select [Id] from RatingType where [Name] = 'Individual')
    AND FORMAT(g.PlayedOn, 'yyyy-MM') = @yearMonthPattern
GROUP BY 
  FORMAT(g.PlayedOn, 'yyyy-MM'),
  n.Nickname
ORDER BY 
  1,
  MonthlyRating DESC,
  n.Nickname

GO



ALTER PROCEDURE [dbo].[GetMonthlyTeamLadder] @yearMonthPattern nvarchar(7), @playersInitialRating int
AS
SELECT
    FORMAT(g.PlayedOn, 'yyyy-MM') as yearMonth,
    n.Nickname,
    SUM(CASE WHEN pg.IsLooser = 0 THEN 1 ELSE 0 END) AS Wins,
    SUM(CASE WHEN pg.IsLooser = 1 THEN 1 ELSE 0 END) as Loses,
    CASE
	WHEN SUM(ra.PointsDelta) + @playersInitialRating < 1000 THEN 1000
		ELSE SUM(ra.PointsDelta) + @playersInitialRating
	END AS MonthlyRating
FROM
    PlayerGame AS pg 
    LEFT JOIN Game AS g on g.Id = pg.GameId
    LEFT JOIN Nickname AS n ON n.Id = pg.NicknameId
	LEFT JOIN Rating ra ON ra.NicknameId = pg.NicknameId
		AND ra.OccuredOn = g.PlayedOn
		AND ra.EventTypeId = (select Id from EventType where Name = 'Game')
WHERE
	ra.RatingTypeId = (select [Id] from RatingType where [Name] = 'Team')
	AND FORMAT(g.PlayedOn, 'yyyy-MM') = @yearMonthPattern
GROUP BY 
  FORMAT(g.PlayedOn, 'yyyy-MM'),
  n.Nickname
ORDER BY 
  1,
  MonthlyRating DESC,
  n.Nickname

GO

alter table [dbo].[PlayerGame]
drop constraint DF_PlayerGame_PointsGained

alter table [dbo].[PlayerGame]
drop column PointsGained
";

            Execute.Sql(sql);
        }
    }
}