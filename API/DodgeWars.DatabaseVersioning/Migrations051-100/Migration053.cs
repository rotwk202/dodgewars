﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations051_100
{
    [Migration(53, "Cleanup: remove Army, Color, ErrorLog, PointsPenalty,UunreportedMatch, UnreportedMatchDetails tables.")]
    public class Migration053 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"alter table PlayerGame
drop constraint FK_PlayerGame_Color
alter table PlayerGame
drop column ColorId

alter table PlayerGame
drop constraint FK_PlayerGame_Army
alter table PlayerGame
drop column ArmyId

alter table PlayerGame
drop column IsProcessed

drop table Army
drop table Color

drop table ErrorLog

drop table UnreportedMatchDetails
drop table UnreportedMatch
drop table PointsPenalty


";

            Execute.Sql(sql);
        }
    }
}