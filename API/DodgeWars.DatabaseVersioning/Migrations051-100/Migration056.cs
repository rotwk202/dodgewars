﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations051_100
{
    [Migration(56, "Add current monthly ratings. Add MonthlyPointsDelta to Rating table.")]
    public class Migration056 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"ALTER TABLE [dbo].[Nickname]
ADD CurrentMonthlyRating int

ALTER TABLE [dbo].[Nickname]
ADD CurrentMonthlyTeamRating int

ALTER TABLE [dbo].[Rating]
ADD MonthlyPointsDelta int NOT NULL DEFAULT 0
";

            Execute.Sql(sql);
        }
    }
}