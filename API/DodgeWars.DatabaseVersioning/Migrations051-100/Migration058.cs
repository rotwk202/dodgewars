﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations051_100
{
    [Migration(58, "Update Map table to store MD5 of map file name.")]
    public class Migration058 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"alter table [dbo].[Map]
drop constraint DF_Map_ReplayMapId

alter table [dbo].[Map]
drop column ReplayMapId

alter table [dbo].[Map]
add [ReplayMapId] [char](32) NOT NULL default ''

alter table [dbo].[Map]
add [ReplayMapTextId] [varchar](100) NOT NULL default ''
";

            Execute.Sql(sql);
        }
    }
}