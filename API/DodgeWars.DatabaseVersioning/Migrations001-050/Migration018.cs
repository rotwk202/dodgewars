﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(18, "Add ErrorLog table primary key")]
    public class Migration018 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"alter table [dbo].[ErrorLog]
add constraint PK_ErrorLog PRIMARY KEY (Id)";

            Execute.Sql(sql);
        }
    }
}