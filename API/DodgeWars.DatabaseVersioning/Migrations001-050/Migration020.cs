﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(20, "Change Game table's Idenitifier column type to int")]
    public class Migration020 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"update [dbo].[Game]
set Identifier = ''

alter table [dbo].[Game]
alter column Identifier int not null";

            Execute.Sql(sql);
        }
    }
}