﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(23, "Setup console and web clients token columns")]
    public class Migration023 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"update [dbo].[User]
set Token = null,
TokenExpiresOn = null

EXEC sp_rename '[dbo].[User].Token', 'TokenConsole', 'COLUMN';
EXEC sp_rename '[dbo].[User].TokenExpiresOn', 'TokenConsoleExpiresOn', 'COLUMN';


alter table [dbo].[User]
add TokenWeb varchar(100);

alter table [dbo].[User]
add TokenWebExpiresOn datetime;";

            Execute.Sql(sql);
        }
    }
}