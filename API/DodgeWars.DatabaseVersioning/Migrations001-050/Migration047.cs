﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(47, "Add Nickname column to PointsPenalty table.")]
    public class Migration047 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"ALTER TABLE [dbo].[PointsPenalty]
ADD Nickname nvarchar(10) NOT NULL
CONSTRAINT C_DefaultNickname DEFAULT ''
";

            Execute.Sql(sql);
        }
    }
}