﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(6, "Add ReplayFileSize column to Game table")]
    public class Migration006 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"alter table [dbo].[Game]
add ReplayFileSize int not null";

            Execute.Sql(sql);
        }
    }
}