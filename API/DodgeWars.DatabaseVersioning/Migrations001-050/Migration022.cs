﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(22, "Add initial one vs one replay map ids")]
    public class Migration022 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"insert into [dbo].[Map](CodeName, DisplayName, Image, ReplayMapId)
values
('FordsOfIsen', 'Fords of Isen', null, 457864),
('Rohan', 'Rohan', null, 447231),
('RohanII', 'Rohan II', null, 411805),
('Erech', 'Erech', null, 535456),
('ErechII', 'Erech II', null, 322747)";

            Execute.Sql(sql);
        }
    }
}