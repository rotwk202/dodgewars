﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(46, "Add PointsPenalty table.")]
    public class Migration046 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"create table [dbo].[PointsPenalty]
(Id int Identity(1,1) not null,
OccuredOn DateTime not null,
LastGameId int not null,
LastGamePlayedOn DateTime not null,
LadderTypeCodeName varchar(30),
LadderTypeDisplayName varchar(45)

constraint PK_PointsPenalty primary key(Id))
";

            Execute.Sql(sql);
        }
    }
}