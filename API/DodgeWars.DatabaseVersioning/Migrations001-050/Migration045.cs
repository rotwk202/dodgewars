﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(45, "Add PlayerActivity view.")]
    public class Migration045 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"IF OBJECT_ID('dbo.PlayerActivity', 'V') IS NOT NULL
    DROP VIEW [dbo].[PlayerActivity]
GO


CREATE VIEW [dbo].[PlayerActivity]
AS 
select x.Nickname, 
	max(x.LastGameIdSingle) as LastGameIdSingle, 
	max(x.PlayedOnSingle) as LastSingleGamePlayedOn, 
	max(LastGameMultipleId) as LastGameIdMultiple, 
	max(PlayedOnMultiple) as LastTeamGanePlayedOn
from
(
select n.Nickname, max(pg.GameId) as 'LastGameIdSingle', max(g.PlayedOn) as 'PlayedOnSingle', null as LastGameMultipleId, null as PlayedOnMultiple
from [dbo].[Nickname] n
join [dbo].[PlayerGame] pg on pg.NicknameId = n.Id
join [dbo].[Game] g on g.Id = pg.GameId
where not exists
	(
		select pgi.GameId
		from [dbo].[PlayerGame] pgi
		where pgi.GameId = pg.GameId
		group by pgi.GameId
		having count(*) > 2
	)
group by n.Nickname

union

select n.Nickname, null as LastGameIdSingle, null as PlayedOnSingle, max(pg.GameId) as 'LastGameMultipleId', max(g.PlayedOn) as 'PlayedOnMultiple'
from [dbo].[Nickname] n
join [dbo].[PlayerGame] pg on pg.NicknameId = n.Id
join [dbo].[Game] g on g.Id = pg.GameId
where exists
	(
		select pgi.GameId
		from [dbo].[PlayerGame] pgi
		where pgi.GameId = pg.GameId
		group by pgi.GameId
		having count(*) > 2
	)
group by n.Nickname
) x
group by x.Nickname

GO
";

            Execute.Sql(sql);
        }
    }
}