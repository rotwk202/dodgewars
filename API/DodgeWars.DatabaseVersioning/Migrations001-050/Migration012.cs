﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(12, "Drop unused tables")]
    public class Migration012 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"drop table [dbo].[RawReportData]
drop table [dbo].[ClanNickname]
drop table [dbo].[Clan]";

            Execute.Sql(sql);
        }
    }
}