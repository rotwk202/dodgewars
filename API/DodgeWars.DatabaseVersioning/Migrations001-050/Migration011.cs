﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(11, "Add vGlobalLadder database view")]
    public class Migration011 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"IF OBJECT_ID ('vGlobalLadder', 'V') IS NOT NULL
DROP VIEW [dbo].[vGlobalLadder];
GO
CREATE VIEW [dbo].[vGlobalLadder]
AS 
	select x.Nickname,
			sum(x.CareerWinsCount) as 'CareerWinsCount',
			sum(x.CareerLosesCount) as 'CareerLosesCount',
			sum(x.DisplayRating) as 'DisplayRating'
	from(
		(select n.Nickname,
				count(*) as 'CareerLosesCount',
				0 as 'CareerWinsCount',
				0 as 'DisplayRating'
		from Nickname n
		join PlayerGame pg on pg.NicknameId = n.Id
		--where n.Id = 1002
				and pg.IsLooser = 1
		group by n.Nickname)

		union all

		(select n.Nickname,
				0 as 'CareerLosesCount',
				count(*) as 'CareerWinsCount',
				0 as 'DisplayRating'
		from Nickname n
		join PlayerGame pg on pg.NicknameId = n.Id
		--where n.Id = 1002
				and pg.IsLooser = 0
		group by n.Nickname)

		union all

		(select Nickname,
				0 as 'CareerLosesCount',
				0 as 'CareerWinsCount',
				DisplayRating as 'DisplayRating'
		from Nickname --where Id = 1002
		)	
		) x
	join Nickname nn on x.Nickname = nn.Nickname
	group by x.Nickname

GO";

            Execute.Sql(sql);
        }
    }
}