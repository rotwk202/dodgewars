﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(13, "Add test table to test webio migrations")]
    public class Migration013 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"create table [dbo].[TestTable]
(FirstName varchar(20),
LastName varchar(30))";

            Execute.Sql(sql);
        }
    }
}