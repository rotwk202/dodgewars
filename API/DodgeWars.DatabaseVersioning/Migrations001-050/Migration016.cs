﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(16, "Setd efault values to Points columns")]
    public class Migration016 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"update [dbo].[Nickname]
  set LadderOnePoints = 0
  where LadderOnePoints is null

  update [dbo].[Nickname]
  set LadderTwoPoints = 0
  where LadderTwoPoints is null";

            Execute.Sql(sql);
        }
    }
}