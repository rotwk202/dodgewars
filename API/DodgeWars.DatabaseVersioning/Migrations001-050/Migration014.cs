﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(14, "Drop test table to test webio migrations")]
    public class Migration014 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"drop table [dbo].[TestTable]";

            Execute.Sql(sql);
        }
    }
}