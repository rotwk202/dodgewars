﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(34, "Update Nickname table - remove Glicko columns.")]
    public class Migration034 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"alter table [dbo].[Nickname]
drop column LadderTwoPoints

alter table [dbo].[Nickname]
drop column LadderOnePoints

alter table [dbo].[Nickname]
drop column DisplayVolatality

alter table [dbo].[Nickname]
drop column DisplayRatingDeviation

alter table [dbo].[Nickname]
drop column DisplayRating

alter table [dbo].[Nickname]
drop column Volatality

alter table [dbo].[Nickname]
drop column RatingDeviation
";

            Execute.Sql(sql);
        }
    }
}