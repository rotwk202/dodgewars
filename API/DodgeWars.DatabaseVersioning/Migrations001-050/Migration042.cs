﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(42, "Add TwoVsTwo ladder")]
    public class Migration042 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"IF OBJECT_ID('dbo.TeamGamesLadder', 'V') IS NOT NULL
    DROP VIEW [dbo].[TeamGamesLadder]
GO

CREATE VIEW [dbo].[TeamGamesLadder]
AS 
select x.Nickname,
		sum(x.TeamGamesLadderWinsCount) as 'TeamGamesLadderWinsCount',
		sum(x.TeamGamesLadderLosesCount) as 'TeamGamesLadderLosesCount',
		sum(x.TeamRating) as 'TeamRating'
from
(
	(select n.Nickname,
			count(*) as 'TeamGamesLadderLosesCount',
			0 as 'TeamGamesLadderWinsCount',
			0 as 'TeamRating'
	from Nickname n
	join PlayerGame pg on pg.NicknameId = n.Id
			and pg.IsLooser = 1
			and pg.GameId in
					(select GameId
					from PlayerGame
					group by GameId
					having count( distinct NicknameId ) > 2)
	group by n.Nickname)
	union all
	(select n.Nickname,
			0 as 'TeamGamesLadderLosesCount',
			count(*) as 'TeamGamesLadderWinsCount',
			0 as 'TeamRating'
	from Nickname n
	join PlayerGame pg on pg.NicknameId = n.Id
			and pg.IsLooser = 0
			and pg.GameId in
				(select GameId
				from PlayerGame
				group by GameId
				having count( distinct NicknameId ) > 2)
	group by n.Nickname)
	union all
	(select Nickname,
			0 as 'TeamGamesLadderLosesCount',
			0 as 'TeamGamesLadderWinsCount',
			TeamRating as 'TeamRating'
	from Nickname)
) x
join Nickname nn on x.Nickname = nn.Nickname
group by x.Nickname

GO
";

            Execute.Sql(sql);
        }
    }
}