﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(10, "Add IsProcessed column to PlayerGame table")]
    public class Migration010 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"alter table [dbo].[PlayerGame]
add IsProcessed bit not null
";

            Execute.Sql(sql);
        }
    }
}