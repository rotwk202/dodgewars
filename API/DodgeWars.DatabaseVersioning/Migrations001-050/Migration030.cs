﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(30, "Add WhoIsOnline view.")]
    public class Migration030 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[WhoIsOnline]
AS 

SELECT  n.Nickname as 'Nickname',
		u.TokenWebExpiresOn as 'TokenWebExpiresOn'
  FROM [dbo].[User] u
  join [dbo].[Nickname] n on u.Id = n.UserId

Go
";

            Execute.Sql(sql);
        }
    }
}