﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(25, "Drop and recreate MatchesHistory view")]
    public class Migration025 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"DROP VIEW [dbo].[MatchesHistory]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[MatchesHistory]
AS 

select n.Nickname as 'Nickname',
	   pg.GameId as 'Game ID',
	   m.DisplayName as 'Map',
	   pg.IsLooser 'Won',
	   0 as 'Points gained',
	   g.ReplayFile as 'Replay file'
from PlayerGame pg
join Game g on pg.GameId = g.Id
join Map m on g.MapId = m.Id
join Nickname n on n.Id = pg.NicknameId

Go
";

            Execute.Sql(sql);
        }
    }
}