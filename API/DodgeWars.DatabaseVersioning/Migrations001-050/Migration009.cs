﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(9, "Add glicko display columns to Nickname table")]
    public class Migration009 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"alter table [dbo].[Nickname]
add DisplayRating float

alter table [dbo].[Nickname]
add DisplayRatingDeviation float

alter table [dbo].[Nickname]
add DisplayVolatality float";

            Execute.Sql(sql);
        }
    }
}