﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(37, "Add AllMatchesView.")]
    public class Migration037 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"IF OBJECT_ID('dbo.AllMatchesHistory', 'V') IS NOT NULL
    DROP VIEW [dbo].[AllMatchesHistory]
GO


CREATE VIEW [dbo].[AllMatchesHistory]
AS 
select g.PlayedOn as 'PlayedOn',
	   --pg.Nickname as 'Nickname',
	   n.Nickname as 'Nickname',
	   g.Identifier as 'Game ID',
	   m.CodeName as 'MapCodeName',
	   m.DisplayName as 'MapDisplayName',
	   g.ReplayFileSize as 'ReplayFileSize',
	   ~(pg.IsLooser) 'Won'
from PlayerGame pg
join Game g on pg.GameId = g.Id
join Map m on g.MapId = m.Id
join Nickname n on n.Id = pg.NicknameId

GO
";

            Execute.Sql(sql);
        }
    }
}