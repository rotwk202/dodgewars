﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(4, "Insert default values into Map, Army and Color entities")]
    public class Migration004 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"insert into [dbo].[Map] (CodeName, DisplayName, Image)
values ('Unknown', 'Uknown map', null)

insert into Army (CodeName, DisplayName)
values ('Random', 'Random')

insert into Color (CodeName, DisplayName)
values ('Unknown', 'Uknown color')";

            Execute.Sql(sql);
        }
    }
}