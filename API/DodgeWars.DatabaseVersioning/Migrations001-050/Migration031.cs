﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(31, "Add unreported match feature's tables.")]
    public class Migration031 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"create table [dbo].[UnreportedMatch]
(Id int identity(1,1) not null,
MapId int not null,
RaisedOn datetime not null,
PlayedOn datetime not null,
IsResolved bit not null,
ReplayFile varbinary(max) not null

CONSTRAINT [PK_UnreportedMatch] PRIMARY KEY(Id))

create table [dbo].[UnreportedMatchDetails]
(Id int identity(1,1) not null,
UnreportedMatchId int not null,
NicknameId int not null,
IsRaisedAgainst bit not null

CONSTRAINT [PK_UnreportedMatchDetails] PRIMARY KEY(Id))

alter table [dbo].[UnreportedMatch]
add constraint FK_UnreportedMatch_Map
foreign key (MapId) references [Map](Id)

alter table [dbo].[UnreportedMatchDetails]
add constraint FK_UnreportedMatchDetails_UnreportedMatch
foreign key (UnreportedMatchId) references [UnreportedMatch](Id)

alter table [dbo].[UnreportedMatchDetails]
add constraint FK_UnreportedMatchDetails_Nickname
foreign key (NicknameId) references [Nickname](Id)
";

            Execute.Sql(sql);
        }
    }
}