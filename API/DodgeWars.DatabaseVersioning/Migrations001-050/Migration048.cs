﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(48, "Extend X column to record what was player's rating at the moment of submitting AR to the system.")]
    public class Migration048 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"alter table [dbo].[UnreportedMatchDetails]
add CurrentCalculateLadderRating float not null
CONSTRAINT C_DefaultCurrentCalculateLadderRating DEFAULT 0
";

            Execute.Sql(sql);
        }
    }
}