﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(27, "Add PointsGained column to PlayerGame table.")]
    public class Migration027 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"  alter table [dbo].[PlayerGame]
add PointsGained int not null
constraint DF_PlayerGame_PointsGained DEFAULT 0
";

            Execute.Sql(sql);
        }
    }
}