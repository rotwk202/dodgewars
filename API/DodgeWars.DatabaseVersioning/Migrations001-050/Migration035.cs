﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(35, "Update OneVsOneLadder view.")]
    public class Migration035 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"DROP VIEW [dbo].[OneVsOneLadder]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[OneVsOneLadder]
AS 
select x.Nickname,
			sum(x.CareerWinsCount) as 'CareerWinsCount',
			sum(x.CareerLosesCount) as 'CareerLosesCount',
			sum(x.Rating) as 'Rating'
	from
	(
		(select n.Nickname,
				count(*) as 'CareerLosesCount',
				0 as 'CareerWinsCount',
				0 as 'Rating'
		from Nickname n
		join PlayerGame pg on pg.NicknameId = n.Id
				and pg.IsLooser = 1
				and pg.GameId in
						/*query - find One vs One games*/
						(select GameId
						from PlayerGame
						group by GameId
						having count( distinct NicknameId ) = 2)
		group by n.Nickname)
		union all
		(select n.Nickname,
				0 as 'CareerLosesCount',
				count(*) as 'CareerWinsCount',
				0 as 'Rating'
		from Nickname n
		join PlayerGame pg on pg.NicknameId = n.Id
				and pg.IsLooser = 0
				and pg.GameId in
					/*query - find One vs One games*/
					(select GameId
					from PlayerGame
					group by GameId
					having count( distinct NicknameId ) = 2)
		group by n.Nickname)
	) x
	join Nickname nn on x.Nickname = nn.Nickname
	group by x.Nickname

GO
";

            Execute.Sql(sql);
        }
    }
}