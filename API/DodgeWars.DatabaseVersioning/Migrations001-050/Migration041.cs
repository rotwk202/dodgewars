﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(41, "Extend Nickname table - add TeamRating field.")]
    public class Migration041 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"ALTER TABLE [dbo].[Nickname]
ADD TeamRating float NOT NULL
CONSTRAINT C_DefaultTeamRating DEFAULT 1600
";

            Execute.Sql(sql);
        }
    }
}