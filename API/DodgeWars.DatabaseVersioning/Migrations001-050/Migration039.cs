﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(39, "Add IsGameHost field to PlayerGame table.")]
    public class Migration039 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"alter table [dbo].[PlayerGame]
add IsGameHost bit
";

            Execute.Sql(sql);
        }
    }
}