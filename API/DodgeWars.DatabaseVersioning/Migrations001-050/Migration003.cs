﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(3, "Set [User].[Token] field to varchar data type")]
    public class Migration003 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"alter table [dbo].[User]
alter column [Token] varchar(100) null";

            Execute.Sql(sql);
        }
    }
}