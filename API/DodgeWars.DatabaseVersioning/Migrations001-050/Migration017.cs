﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(17, "Add ErrorLog table")]
    public class Migration017 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"create table [dbo].[ErrorLog]
(Id int not null identity(1,1),
OccuredOn DateTime not null,
Controller varchar(30) not null,
Method varchar(40) not null,
[Message] varchar(max) not null)";

            Execute.Sql(sql);
        }
    }
}