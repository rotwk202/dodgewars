﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(8, "Add glicko columns to Nickname table")]
    public class Migration008 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"alter table [dbo].[Nickname]
add Rating float

alter table [dbo].[Nickname]
add RatingDeviation float

alter table [dbo].[Nickname]
add Volatality float";

            Execute.Sql(sql);
        }
    }
}