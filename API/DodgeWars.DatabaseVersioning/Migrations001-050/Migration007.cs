﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(7, "Add Identifier column to Game table")]
    public class Migration007 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"alter table [dbo].[Game]
add Identifier varchar(100) not null";

            Execute.Sql(sql);
        }
    }
}