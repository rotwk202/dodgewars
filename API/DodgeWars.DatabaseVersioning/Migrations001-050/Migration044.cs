﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(44, "WhoIsOnline view - list only players who played at least 1 game.")]
    public class Migration044 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"IF OBJECT_ID('dbo.WhoIsOnline', 'V') IS NOT NULL
    DROP VIEW [dbo].[WhoIsOnline]
GO


CREATE VIEW [dbo].[WhoIsOnline]
AS 
SELECT distinct n.Nickname as 'Nickname',
	u.TokenWebExpiresOn as 'TokenWebExpiresOn'
FROM [dbo].[User] u
join [dbo].[Nickname] n on u.Id = n.UserId
join [dbo].[PlayerGame] pg on n.Id = pg.NicknameId
where n.Nickname in
	(
		select n.Nickname
		from [dbo].[Nickname] n
		join [dbo].[PlayerGame] pg on n.Id = pg.NicknameId
		group by n.Id, n.Nickname
		having count(*) > 0
	)

GO

";

            Execute.Sql(sql);
        }
    }
}