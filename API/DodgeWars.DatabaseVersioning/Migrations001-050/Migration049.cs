﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(49, "Set IsGameHost properties from NULL to false.")]
    public class Migration049 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"update [dbo].[PlayerGame]
set IsGameHost = 0
where IsGameHost is null
";

            Execute.Sql(sql);
        }
    }
}