﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(21, "Add ReplayMapId column to Map table")]
    public class Migration021 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"alter table [dbo].[Map]
add ReplayMapId int not null
constraint DF_Map_ReplayMapId DEFAULT 0";

            Execute.Sql(sql);
        }
    }
}