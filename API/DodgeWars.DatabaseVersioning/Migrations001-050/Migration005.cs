﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(5, "Change maximum Nickname length to 10")]
    public class Migration005 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"alter table [dbo].[Nickname]
alter column Nickname nvarchar(10) not null";

            Execute.Sql(sql);
        }
    }
}