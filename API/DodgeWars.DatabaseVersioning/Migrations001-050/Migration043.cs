﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(43, "Add 2v2 maps")]
    public class Migration043 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"insert into [dbo].[Map] (CodeName, DisplayName, Image, ReplayMapId)
values
('Udun', 'Udun', null, 296936),
('Buckland', 'Buckland', null, 548492),
('AnorienII', 'Anorien II', null, 312481),
('OstInEdhil', 'Ost-In-Edhil', null, 411749),
('TowerHills', 'Tower Hills', null, 636282),
('AnfalasII', 'Anfalas II', null, 5411627),
('Dale', 'Dale', null, 388476),
('DruadanForest', 'Druadan Forest', null, 571418),
('Eregion', 'Eregion', null, 465912),
('FarHarad', 'Far Harad', null, 456500),
('FordsOfRohan', 'Fords Of Rohan', null, 325973),
('Nanduhirion', 'Nanduhirion', null, 656021),
('Wold', 'Wold', null, 545066)
";

            Execute.Sql(sql);
        }
    }
}