﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(24, "Add MatchesHistory view")]
    public class Migration024 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[MatchesHistory]
AS 

select pg.GameId as 'Game ID',
	   m.DisplayName as 'Map',
	   pg.IsLooser 'Won',
	   0 as 'Points gained',
	   g.ReplayFile as 'Replay file'
from PlayerGame pg
join Game g on pg.GameId = g.Id
join Map m on g.MapId = m.Id

GO
";

            Execute.Sql(sql);
        }
    }
}