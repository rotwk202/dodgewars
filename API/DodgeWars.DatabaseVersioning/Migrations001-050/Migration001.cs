﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(1, "Create gameblog.public database. Set basic entities and their relationships.")]
    public class Migration001 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"--use [master]
--go

--if exists(select * from sys.databases where [Name] = 'dodgewars.public')
--begin
--	alter database [dodgewars.public]
--	set single_user
--	with rollback immediate
--	drop database [dodgewars.public]
--end

--go

----wroserver
--create database [dodgewars.public]
--on
--(name = um_dat,
--filename = 'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\dodgewars.public.mdf',
--size = 10,
--maxsize = 20,
--filegrowth = 5
--)
--go
";

            Execute.Sql(sql);
        }
    }
}
