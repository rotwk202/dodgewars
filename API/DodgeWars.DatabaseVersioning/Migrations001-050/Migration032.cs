﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(32, "Insert armies to Army table.")]
    public class Migration032 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"update [dbo].[Army]
set CodeName = 'Unknown', DisplayName = 'Unknown'
where CodeName = 'Random'

insert into [dbo].[Army] (CodeName, DisplayName)
values
('Men', 'Men'),
('Elves', 'Elves'),
('Dwarves', 'Dwarves'),
('Angmar', 'Angmar'),
('Goblins', 'Goblins'),
('Mordor', 'Mordor'),
('Isengard', 'Isengard')
";

            Execute.Sql(sql);
        }
    }
}