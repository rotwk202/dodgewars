﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(29, "Drop and recreate MatchesHistory view - use game Identifier.")]
    public class Migration029 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"DROP VIEW [dbo].[MatchesHistory]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[MatchesHistory]
AS 

select n.Nickname as 'Nickname',
	   g.Identifier as 'Game ID',
	   m.DisplayName as 'Map',
	   pg.IsLooser 'Won',
	   pg.PointsGained as 'Points gained'
from PlayerGame pg
join Game g on pg.GameId = g.Id
join Map m on g.MapId = m.Id
join Nickname n on n.Id = pg.NicknameId

Go
";

            Execute.Sql(sql);
        }
    }
}