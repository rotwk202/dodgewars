﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(2, "Create basic tables and set relationships")]
    public class Migration002 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"create table [dbo].[User]
(Id int not null identity(1,1),
Email nvarchar(150) not null,
Password char(32) not null,
CreatedOn datetime not null,
Token binary(16),
TokenExpiresOn datetime,
constraint PK_User primary key (Id))

create table [dbo].[Nickname]
(Id int not null identity(1,1),
UserId int not null,
Nickname nvarchar(50) not null,
MonthPoints int,
constraint PK_Nickname primary key (Id))

create table [dbo].[Game]
(Id int not null identity(1,1),
MapId int not null,
Description nvarchar(1000),
PlayedOn datetime not null,
ReplayFile varbinary(max),
ReplayName nvarchar(100),
constraint PK_Game primary key (Id))

create table [dbo].[Clan]
(Id int not null identity(1,1),
Tag varchar(10) not null,
Name nvarchar(30) not null,
constraint PK_Clan primary key (Id))

create table [dbo].[PlayerGame]
(Id int not null identity(1,1),
NicknameId int not null,
GameId int not null,
ColorId int not null,
ArmyId int not null,
Team int not null,
IsLooser bit not null,
constraint PK_PlayerGame primary key (Id))

create table [dbo].[ClanNickname]
(Id int not null identity(1,1),
ClanId int not null,
NicknameId int not null
constraint PK_ClanNickname primary key (Id))

create table [dbo].[RawReportData]
(Id int identity(1,1) not null,
UserId int not null,
ReportedOn datetime not null,
Nickname nvarchar(50) not null,
Team int not null,
IsLooser bit not null
constraint PK_RawReportData Primary Key (Id))

create table [dbo].[Army]
(Id int not null identity(1,1),
CodeName varchar(20) not null,
DisplayName nvarchar(30) not null,
constraint PK_Army primary key (Id))

create table [dbo].[Color]
(Id int not null identity(1,1),
CodeName varchar(20) not null,
DisplayName nvarchar(30) not null,
constraint PK_Color primary key (Id))

create table [dbo].[Map]
(Id int not null identity(1,1),
CodeName varchar(30) not null,
DisplayName nvarchar(50) not null,
Image varbinary(max) null
constraint PK_Map primary key (Id))

alter table [dbo].[Nickname]
add constraint FK_Nickname_User
foreign key (UserId) references [User](Id)

alter table [dbo].[ClanNickname]
add constraint FK_ClanNickname_Clan
foreign key (ClanId) references [Clan](Id)

alter table [dbo].[ClanNickname]
add constraint FK_ClanNickname_Nickname
foreign key (NicknameId) references [Nickname](Id)

alter table [dbo].[Game]
add constraint FK_Game_Map
foreign key (MapId) references [Map](Id)

alter table [dbo].[PlayerGame]
add constraint FK_PlayerGame_Nickname
foreign key (NicknameId) references [Nickname](Id)

alter table [dbo].[PlayerGame]
add constraint FK_PlayerGame_Game
foreign key (GameId) references [Game](Id)

alter table [dbo].[PlayerGame]
add constraint FK_PlayerGame_Color
foreign key (ColorId) references [Color](Id)

alter table [dbo].[PlayerGame]
add constraint FK_PlayerGame_Army
foreign key (ArmyId) references [Army](Id)
";

            Execute.Sql(sql);
        }
    }
}
