﻿using FluentMigrator;

namespace DodgeWars.DatabaseVersioning.Migrations001_050
{
    [Migration(15, "Add ladder points columns")]
    public class Migration015 : BaseMigration
    {
        public override void Up()
        {
            var sql = @"alter table [dbo].[Nickname]
  drop column MonthPoints

  alter table [dbo].[Nickname]
  add LadderOnePoints int

  alter table [dbo].[Nickname]
  add LadderTwoPoints int";

            Execute.Sql(sql);
        }
    }
}