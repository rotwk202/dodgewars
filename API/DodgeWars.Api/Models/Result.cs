﻿using System.Collections.Generic;

namespace DodgeWars.Api.Models
{
    public class Result<T>
    {
        public string Message { get; set; }

        public List<T> Data { get; set; }
    }
}