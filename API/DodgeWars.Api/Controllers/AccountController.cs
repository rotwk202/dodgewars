﻿using DodgeWars.Api.Models;
using DodgeWars.BusinessLogic.Contracts;
using DodgeWars.BusinessLogic.Models;
using DodgeWars.DAL.Contracts;
using System;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Linq;
using DodgeWars.Tools;
using Newtonsoft.Json;
using log4net;

namespace DodgeWars.Api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AccountController : BaseApiController
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IAccountService _accountService;
        private readonly IAuthenticationService _authenticationService;
        private readonly ILog _logger;

        public AccountController(
                IUnitOfWork unitOfWork,
                IAccountService accountService,
                IAuthenticationService authenticationService)
        {
            _unitOfWork = unitOfWork;
            _accountService = accountService;
            _authenticationService = authenticationService;
            _logger = LogManager.GetLogger("EventLogger");
        }

        [HttpPost]
        [Route("api/Account/RegisterUser")]
        public IHttpActionResult RegisterUser(ApiUser user)
        {
            var errorMessage = string.Empty;
            var innerExceptionMessage = string.Empty;

            try
            {
                if (user == null)
                {
                    throw new ArgumentNullException(nameof(user), "No user was provided.");
                }

                var clientName = Request.Headers.GetValues("ClientName").First();
                var wasSuccessfullyRegistered = _accountService.RegisterUser(user, clientName);
                if (wasSuccessfullyRegistered)
                {
                    _unitOfWork.Commit();
                    var result = new Result<string> { Message = "The user has been successfully registered.", Data = null };
                    return Ok(JsonConvert.SerializeObject(result));
                }
            }
            catch (Exception e)
            {
                errorMessage = e.Message;
                _logger.Info($@"{DateTime.Now} AccountController.RegisterUser exception:{Environment.NewLine}{e}");
            }

            return BadRequest(errorMessage);
        }

        [HttpPost]
        [Route("api/Account/GetToken")]
        public IHttpActionResult GetToken(ApiUser user)
        {
            ApiToken tokenModel = null;
            var errorMessage = string.Empty;
            var innerExceptionMessage = string.Empty;

            try
            {
                if (user == null)
                {
                    throw new ArgumentNullException(nameof(user), "User was not provided.");
                }

                if (string.IsNullOrEmpty(user.Email) || string.IsNullOrEmpty(user.Password))
                {
                    throw new ArgumentException("Either Email or Password were not provided.");
                }

                var clientName = Request.Headers.GetValues("ClientName").First();
                tokenModel = _authenticationService.GetToken(user, clientName);
            }
            catch (Exception e)
            {
                errorMessage = e.Message;
                _logger.Info($@"{DateTime.Now} AccountController.GetToken exception:{Environment.NewLine}{e}");
            }

            if (tokenModel != null)
            {
                _unitOfWork.Commit();
                return Ok(tokenModel);
            }

            return BadRequest(errorMessage);
        }

        [HttpPost]
        [Route("api/Account/CreateNickname")]
        public IHttpActionResult CreateNickname([FromBody] ApiCreateNicknameRequest nicknameRequest)
        {
            var errorMessage = string.Empty;
            var innerExceptionMessage = string.Empty;

            try
            {
                if (string.IsNullOrEmpty(nicknameRequest.NicknameRequest))
                {
                    throw new ArgumentNullException(nameof(nicknameRequest), "Nickname request was not provided");
                }

                var token = Request.Headers.GetValues("Token").First();
                var clientName = Request.Headers.GetValues("ClientName").First();

                if (_authenticationService.IsTokenOutOfDate(token, clientName, DateTime.Now))
                {
                    errorMessage = "Your token has expired.";
                    return Unauthorized(errorMessage);
                }

                var nicknameRequestModel = new ApiNickname
                {
                    UserId = _accountService.GetUserId(token, clientName),
                    Nickname = nicknameRequest.NicknameRequest
                };

                var wasNicknameSuccessfullyCreated = _accountService.CreateNickname(nicknameRequestModel);

                if (wasNicknameSuccessfullyCreated)
                {
                    _unitOfWork.Commit();
                    var result = new Result<string>
                    {
                        Message =
                        $"Nickname {nicknameRequest.NicknameRequest} has been successfully created.",
                        Data = null
                    };

                    return Ok(JsonConvert.SerializeObject(result));
                }
            }
            catch (Exception e)
            {
                errorMessage = e.Message;
                _logger.Info($@"{DateTime.Now} AccountController.CreateNickname exception:{Environment.NewLine}{e}");
            }

            return BadRequest(errorMessage);
        }

        [HttpGet]
        [Route("api/Account/GetUserNicknames")]
        public IHttpActionResult GetUserNicknames()
        {
            string errorMessage;
            var innerExceptionMessage = string.Empty;

            try
            {
                var token = Request.Headers.GetValues("Token").First();
                var clientName = Request.Headers.GetValues("ClientName").First();

                if (_authenticationService.IsTokenOutOfDate(token, clientName, DateTime.Now))
                {
                    errorMessage = "Your token has expired.";
                    return Unauthorized(errorMessage);
                }
                var nicknamesList = _accountService.GetUserNicknames(token, clientName);
                var result = new Result<ApiNickname> { Message = "Nicknames successfully retrieved", Data = nicknamesList };

                return Ok(result);
            }
            catch (Exception e)
            {
                errorMessage = e.Message;
                _logger.Info($@"{DateTime.Now} AccountController.GetUserNicknames exception:{Environment.NewLine}{e}");
            }

            return BadRequest(errorMessage);
        }

        [HttpPost]
        public IHttpActionResult IsTokenExpired([FromBody] ApiRequest requestModel)
        {
            bool isTokenExpired;
            var innerExceptionMessage = string.Empty;

            try
            {
                var token = Request.Headers.GetValues("Token").First();
                var clientName = Request.Headers.GetValues("ClientName").First();
                isTokenExpired = _authenticationService.IsTokenOutOfDate(token, clientName, requestModel.RequestTime);
            }
            catch (Exception e)
            {
                _logger.Info($@"{DateTime.Now} AccountController.IsTokenExpired exception:{Environment.NewLine}{e}");
                return BadRequest(e.Message);
            }

            return Ok(isTokenExpired);
        }

        [HttpGet]
        [Route("api/Account/GetAllNicknames")]
        public IHttpActionResult GetAllNicknames()
        {
            string errorMessage;
            var innerExceptionMessage = string.Empty;

            try
            {
                var allNicknames = _accountService.GetAllNicknames();
                var result = new Result<ApiNickModel> { Message = "Nicknames successfully retrieved", Data = allNicknames };

                return Ok(result);
            }
            catch (Exception e)
            {
                errorMessage = e.Message;
                _logger.Info($@"{DateTime.Now} AccountController.GetAllNicknames exception:{Environment.NewLine}{e}");
            }

            return BadRequest(errorMessage);
        }

        [HttpGet]
        [Route("api/Account/GetMainNickname")]
        public IHttpActionResult GetMainNickname()
        {
            string errorMessage;
            var innerExceptionMessage = string.Empty;

            try
            {
                var email = Request.Headers.GetValues("Email").First();
                var mainNickname = _accountService.GetMainNicknameModel(email);
                return Ok(mainNickname);
            }
            catch (Exception e)
            {
                errorMessage = e.Message;
                _logger.Info($@"{DateTime.Now} AccountController.GetMainNickname exception:{Environment.NewLine}{e}");
            }

            return BadRequest(errorMessage);
        }
    }
}
