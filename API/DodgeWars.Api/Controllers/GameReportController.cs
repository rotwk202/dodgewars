﻿using DodgeWars.BusinessLogic.Contracts;
using DodgeWars.BusinessLogic.Models;
using DodgeWars.DAL.Contracts;
using System;
using System.Web.Http;
using System.Linq;
using DodgeWars.Api.Models;
using System.Web.Http.Cors;
using log4net;
using Newtonsoft.Json;
using DodgeWars.Tools;
using WebGrease.Css.Extensions;

namespace DodgeWars.Api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class GameReportController : BaseApiController
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IGameReportService _reportService;
        private readonly IAuthenticationService _authenticationService;
        private readonly ILog _logger;

        public GameReportController() { }

        public GameReportController(
            IUnitOfWork unitOfWork,
            IGameReportService reportService,
            IAuthenticationService authenticationService)
        {
            _unitOfWork = unitOfWork;
            _reportService = reportService;
            _authenticationService = authenticationService;
            _logger = LogManager.GetLogger("GameReportControllerProdLog");
        }

        [HttpPost]
        [Route("api/GameReport/ReportLoss")]
        public IHttpActionResult ReportLoss([FromBody] ApiGame apiGame)
        {
            string errorMessage;

            try
            {
                var token = Request.Headers.GetValues("Token").FirstOrDefault();
                var clientName = Request.Headers.GetValues("ClientName").FirstOrDefault();

                if (apiGame == null)
                {
                    throw new ArgumentNullException(nameof(apiGame), "Game details were not provided.");
                }

                if (_authenticationService.IsTokenOutOfDate(token, clientName, DateTime.Now))
                {
                    errorMessage = "Your token has expired.";
                    return Unauthorized(errorMessage);
                }
                _logger.Info($@"{Environment.NewLine} GameReportController.ReportLoss: going to report game.");
                GameLogUtility.LogGameInfo(_logger, JsonConvert.SerializeObject(apiGame));
                apiGame.Players.RemoveAll(x => x.IsObserver);
                var newGame = _reportService.AddGame(apiGame);
                _reportService.SetNewGameDetails(apiGame.Players, newGame);
                _logger.Info($@"GameReportController.ReportLoss: Added game, set all game details correctly.");
                _logger.Info($@"GameReportController.ReportLoss: newGame.PlayedOn: {newGame.PlayedOn}, newGame.Identifier: {newGame.Identifier}, nickname Ids: {string.Join(",", newGame.PlayerGames.Select(x => x.NicknameId).ToList())}");
                _unitOfWork.Commit();

                if (apiGame.Players.Count == 2)
                {
                    _reportService.UpdateIndividualLadderRatings(apiGame.Players, newGame);
                    _logger.Info($@"{DateTime.Now} GameReportController.ReportLoss: Updated individual ladder ratings correctly.");
                }
                if (apiGame.Players.Count > 2)
                {
                    _reportService.UpdateTeamLadderPoints(apiGame.Players, newGame);
                    _logger.Info($@"{DateTime.Now} GameReportController.ReportLoss: Updated team ladder ratings correctly.");
                }

                _unitOfWork.Commit();

                var result = new Result<string> { Message = "The match was successfully reported", Data = null };
                return Ok(result);
            }
            catch (Exception e)
            {
                errorMessage = e.Message;
                _logger.Info($@"{Environment.NewLine} GameReportController.ReportLoss exception:{Environment.NewLine}{e}");
            }

            return BadRequest(errorMessage);
        }

        [HttpGet]
        [Route("api/GameReport/GetAllMaps")]
        public IHttpActionResult GetAllMaps()
        {
            string errorMessage;
            var innerExceptionMessage = string.Empty;

            try
            {
                var allMaps = _reportService.GetAllMaps();
                var result = new Result<ApiMapModel> { Message = "Maps successfully retrieved", Data = allMaps };

                return Ok(result);
            }
            catch (Exception e)
            {
                errorMessage = e.Message;
                _logger.Info($@"{DateTime.Now} GameReportController.GetAllMaps exception:{Environment.NewLine}{e}");
            }

            return BadRequest(errorMessage);
        }
    }
}
