﻿using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;

namespace DodgeWars.Api.Controllers
{
    public class BaseApiController : ApiController
    {
        protected internal ResponseMessageResult Unauthorized(string message)
        {
            return ResponseMessage(Request.CreateErrorResponse(HttpStatusCode.Unauthorized, message));
        }
    }
}
