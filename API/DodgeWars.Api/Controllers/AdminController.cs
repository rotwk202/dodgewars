﻿using DodgeWars.Api.Models;
using DodgeWars.BusinessLogic.Contracts;
using DodgeWars.BusinessLogic.Jobs;
using DodgeWars.BusinessLogic.Models;
using DodgeWars.DAL.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace DodgeWars.Api.Controllers
{
    public class AdminController : BaseApiController
    {
        private readonly IAdminService _adminService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IGameReportService _reportService;

        public AdminController(IAdminService adminService,
            IUnitOfWork unitOfWork,
            IGameReportService reportService)
        {
            _adminService = adminService;
            _unitOfWork = unitOfWork;
            _reportService = reportService;
        }

        [HttpPut]
        [Route("api/Admin/UploadMapImage/{mapCodeName}")]
        public IHttpActionResult UploadMapImage(string mapCodeName)
        {
            try
            {
                _adminService.UploadMapImage(mapCodeName);
                _unitOfWork.Commit();

                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpPost]
        [Route("api/Admin/UploadMaps")]
        public IHttpActionResult UploadImage(List<ApiMap> mapModels)
        {
            var successfullyAdded = _adminService.UploadAllMaps(mapModels);

            if (successfullyAdded)
            {
                _unitOfWork.Commit();
            }

            return Ok("Maps were uploaded successfully.");
        }

        [HttpPost]
        [Route("api/Admin/ProcessARGame")]
        public IHttpActionResult ProcessARGame([FromBody] ApiGame apiArGame)
            {
            string errorMessage;

            try
            {
                if (apiArGame == null)
                {
                    throw new ArgumentNullException("apiGame", "Game details were not provided.");
                }

                apiArGame.Players.RemoveAll(x => x.IsObserver);
                var newGame = _reportService.AddGame(apiArGame);
                _reportService.SetNewGameDetails(apiArGame.Players, newGame);

                _unitOfWork.Commit();

                if (apiArGame.Players.Count == 2)
                {
                    _reportService.UpdateIndividualLadderRatings(apiArGame.Players, newGame);
                }
                if (apiArGame.Players.Count > 2)
                {
                    _reportService.UpdateTeamLadderPoints(apiArGame.Players, newGame);
                }

                _unitOfWork.Commit();

                var result = new Result<string> { Message = "The match was successfully reported", Data = null };
                return Ok(result);
            }
            catch (Exception e)
            {
                if (e.InnerException != null)
                {
                }
                errorMessage = e.Message;
            }

            return BadRequest(errorMessage);
        }

        [HttpGet]
        [Route("api/Admin/RecalculateAllGames")]
        public IHttpActionResult RecalculateAllGames()
        {
            var job = new RecalculateAllGames();
            job.Execute(null);

            return Ok();
        }
    }
}
