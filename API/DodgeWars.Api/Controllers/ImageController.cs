﻿using DodgeWars.BusinessLogic.Contracts;
using System.Web.Http;
using System.Web.Http.Cors;

namespace DodgeWars.Api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ImageController : BaseApiController
    {
        private readonly IRateService _rateService;

        public ImageController(IRateService rateService)
        {
            _rateService = rateService;
        }

        [HttpGet]
        [Route("api/Image/GetMapImage/{mapCodeName}")]
        public IHttpActionResult GetMapImage(string mapCodeName)
        {
            var image = _rateService.GetMapImage(mapCodeName);
            return Ok(image);
        }
    }
}
