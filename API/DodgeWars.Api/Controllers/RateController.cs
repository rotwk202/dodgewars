﻿using DodgeWars.Api.Models;
using DodgeWars.BusinessLogic.Contracts;
using DodgeWars.BusinessLogic.Models;
using DodgeWars.DAL.Model;
using log4net;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Results;

namespace DodgeWars.Api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RateController : BaseApiController
    {
        private readonly IRateService _rateService;
        private readonly ILog _logger;

        public RateController(IRateService rateService)
        {
            _rateService = rateService;
            _logger = LogManager.GetLogger("EventLogger");
        }

        [HttpGet]
        [Route("api/Rate/GetLadderOneData")]
        public IHttpActionResult GetLadderOneData()
        {
            string errorMessage;
            try
            {
                var displayData = _rateService.GetLadderOneData();
                var result = new Result<ApiNickname>() { Message = "Display data was successfully retrieved.", Data = displayData };

                return Ok(result);
            }
            catch (Exception e)
            {
                errorMessage = e.Message;
                _logger.Info($@"{DateTime.Now} RateController.GetLAdderOneData exception:{Environment.NewLine}{e}");
            }

            return BadRequest(errorMessage);
        }

        [HttpGet]
        [Route("api/Rate/GetTeamGamesLadderData")]
        public IHttpActionResult GetTeamGamesLadderData()
        {
            string errorMessage;
            try
            {
                var displayData = _rateService.GetTeamGamesLadderData();
                var result = new Result<ApiTeamGamesWarriorModel>() { Message = "Display data was successfully retrieved.", Data = displayData };

                return Ok(result);
            }
            catch (Exception e)
            {
                errorMessage = e.Message;
                _logger.Info($@"{DateTime.Now} RateController.GetTeamGamesLadderData exception:{Environment.NewLine}{e}");
            }

            return BadRequest(errorMessage);
        }

        [HttpPost]
        [Route("api/Rate/GetMatchesHistory")]
        public IHttpActionResult GetMatchesHistory(ApiRateRequest request)
        {
            string errorMessage;
            try
            {
                if(request.PageNumber < 1)
                {
                    throw new ArgumentException("Given page nuber was wrong.");
                }

                var matchesHistory = _rateService.GetMatchesHistory(request.Nickname, request.PageNumber, request.Take);
                var result = new Result<ApiMatchHistory>() { Message = "Warrior history data was successfully retrieved.", Data = matchesHistory };

                return Ok(result);
            }
            catch (Exception e)
            {
                errorMessage = e.Message;
                _logger.Info($@"{DateTime.Now} RateController.GetMatchesHistory exception:{Environment.NewLine}{e}");
            }

            return BadRequest(errorMessage);
        }

        [HttpPost]
        [Route("api/Rate/GetLastGames")]
        public IHttpActionResult GetLastGames(ApiLastGamesRequest requestData)
        {
            string errorMessage;
            try
            {
                var theLastGames = _rateService.GetLastGames(requestData.GamesCount);
                var result = new Result<ApiMatchHistory>() { Message = "The last games were successfully retrieved.", Data = theLastGames };

                return Ok(result);
            }
            catch (Exception e)
            {
                errorMessage = e.Message;
                _logger.Info($@"{DateTime.Now} RateController.GetLastGames exception:{Environment.NewLine}{e}");
            }

            return BadRequest(errorMessage);
        }

        //TODO: sa tu obiekty Disposable, ktorym nie robie Dispose, bo wtedy ficzer nie dziala
        [HttpGet]
        [Route("api/Rate/ExampleFile/{gameIdentifier}")]
        public IHttpActionResult ExampleFile(int gameIdentifier)
        {
            var replayFile = _rateService.GetReplayFile(gameIdentifier);

            var stream = new MemoryStream(replayFile);
            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK) { Content = new StreamContent(stream) };

            httpResponseMessage.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = "NotNecessary.BfME2Replay"
            };
            httpResponseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");

            return ResponseMessage(httpResponseMessage);
        }

        //TODO: sa tu obiekty Disposable, ktorym nie robie Dispose, bo wtedy ficzer nie dziala

        [HttpGet]
        [Route("api/Rate/DownloadReplay/{gameIdentifier}")]
        public IHttpActionResult DownloadReplay(int gameIdentifier)
        {
            var replayFile = _rateService.GetReplayFile(gameIdentifier);

            var stream = new MemoryStream(replayFile);
            var httpResponseMessage = new HttpResponseMessage(HttpStatusCode.OK) { Content = new StreamContent(stream) };

            httpResponseMessage.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = "NotNecessary.BfME2Replay"
            };

            httpResponseMessage.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            ResponseMessageResult responseMessageResult = ResponseMessage(httpResponseMessage);

            return responseMessageResult;
        }

        [HttpGet]
        [Route("api/Rate/GetMonthlyLadderOneData/{dateString}")]
        public IHttpActionResult GetMonthlyLadderOneData(string dateString)
        {
            string errorMessage;
            try
            {
                var displayData = _rateService.GetMonthlyLadderOneData(dateString);
                var result = new Result<Playa>() { Message = "Display data was successfully retrieved.", Data = displayData };

                return Ok(result);
            }
            catch (Exception e)
            {
                errorMessage = e.Message;
                _logger.Info($@"{DateTime.Now} RateController.GetMonthlyLadderOneData exception:{Environment.NewLine}{e}");
            }

            return BadRequest(errorMessage);
        }

        [HttpGet]
        [Route("api/Rate/GetMonthlyTeamLadderData/{dateString}")]
        public IHttpActionResult GetMonthlyTeamLadderData(string dateString)
        {
            string errorMessage;
            try
            {
                var displayData = _rateService.GetMonthlyTeamLadderData(dateString);
                var result = new Result<Playa>() { Message = "Display data was successfully retrieved.", Data = displayData };

                return Ok(result);
            }
            catch (Exception e)
            {
                errorMessage = e.Message;
                _logger.Info($@"{DateTime.Now} RateController.GetMonthlyTeamLadderData exception:{Environment.NewLine}{e}");
            }

            return BadRequest(errorMessage);
        }

        [HttpGet]
        [Route("api/Rate/GetGamesCountData")]
        public IHttpActionResult GetGamesCountData()
        {
            string errorMessage;
            try
            {
                var gamesCountData = _rateService.GetGamesCountData();
                return Ok(gamesCountData);
            }
            catch (Exception e)
            {
                errorMessage = e.Message;
                _logger.Info($@"{DateTime.Now} RateController.GetGamesCountData exception:{Environment.NewLine}{e}");
            }

            return BadRequest(errorMessage);
        }
    }
}
