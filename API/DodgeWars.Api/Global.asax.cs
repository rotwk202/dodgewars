﻿using DodgeWars.BusinessLogic.Jobs;
using Quartz;
using Quartz.Impl;
using System;
using System.Diagnostics;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using Quartz.Spi;

namespace DodgeWars.Api
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            log4net.Config.XmlConfigurator.Configure();

            ISchedulerFactory schedFact = new StdSchedulerFactory();
            IScheduler scheduler = schedFact.GetScheduler();
            scheduler.Start();

            ScheduleResetMonthlyRatingsJob(scheduler);
        }

        private void ScheduleResetMonthlyRatingsJob(IScheduler scheduler)
        {
            IJobDetail job = JobBuilder.Create<ResetMonthlyRatings>()
                .WithIdentity("ResetMonthlyRatings")
                .Build();

            var now = DateTime.Now;
            ITrigger trigger = TriggerBuilder
                .Create()
                .StartNow()
                .WithSchedule(CronScheduleBuilder.MonthlyOnDayAndHourAndMinute(1, 3, 0))
                .Build();

            DiagnoseTrigger(trigger);
        }

        private void DiagnoseTrigger(ITrigger trigger)
        {
            var times = TriggerUtils.ComputeFireTimes(trigger as IOperableTrigger, null, 10);
            foreach (var time in times) Debug.WriteLine(time.ToLocalTime());
        }
    }
}
