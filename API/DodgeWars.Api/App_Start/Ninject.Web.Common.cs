[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(DodgeWars.Api.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(DodgeWars.Api.App_Start.NinjectWebCommon), "Stop")]

namespace DodgeWars.Api.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using Ninject.Web.Common.WebHost;

    using DodgeWars.DAL.Contracts;
    using DodgeWars.DAL.Services;
    using DodgeWars.BusinessLogic.Contracts;
    using DodgeWars.BusinessLogic.Services;
    using DodgeWars.DAL.Model;
    using DodgeWars.BusinessLogic.Builders;

    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
                RegisterServices(kernel);

                System.Web.Http.GlobalConfiguration.Configuration.DependencyResolver = new NinjectDependencyResolver(kernel);

                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IUnitOfWork>().To<UnitOfWork>().InRequestScope();

            kernel.Bind<IRateService>().To<RateService>().InRequestScope();
            kernel.Bind<IGameReportService>().To<GameReportService>().InRequestScope();
            kernel.Bind<IAccountService>().To<AccountService>().InRequestScope();
            kernel.Bind<IAuthenticationService>().To<AuthenticationService>().InRequestScope();
            kernel.Bind<IAdminService>().To<AdminService>().InRequestScope();
            kernel.Bind<IRatingBuilder>().To<RatingBuilder>().InRequestScope();

            BindRepositories(kernel);
        }

        private static void BindRepositories(IKernel kernel)
        {
            kernel.Bind<IRepository<Game>>().To<Repository<Game>>().InRequestScope();
            kernel.Bind<IRepository<Map>>().To<Repository<Map>>().InRequestScope();
            kernel.Bind<IRepository<Nickname>>().To<Repository<Nickname>>().InRequestScope();
            kernel.Bind<IRepository<PlayerGame>>().To<Repository<PlayerGame>>().InRequestScope();
            kernel.Bind<IRepository<User>>().To<Repository<User>>().InRequestScope();
            kernel.Bind<IRepository<OneVsOneLadder>>().To<Repository<OneVsOneLadder>>().InRequestScope();
            kernel.Bind<IRepository<MatchesHistory>>().To<Repository<MatchesHistory>>().InRequestScope();
            kernel.Bind<IRepository<WhoIsOnline>>().To<Repository<WhoIsOnline>>().InRequestScope();
            kernel.Bind<IRepository<AllMatchesHistory>>().To<Repository<AllMatchesHistory>>().InRequestScope();
            kernel.Bind<IRepository<TeamGamesLadder>>().To<Repository<TeamGamesLadder>>().InRequestScope();
            kernel.Bind<IRepository<PlayerActivity>>().To<Repository<PlayerActivity>>().InRequestScope();
            kernel.Bind<IRepository<Replay>>().To<Repository<Replay>>().InRequestScope();
            kernel.Bind<IRepository<Rating>>().To<Repository<Rating>>().InRequestScope();
            kernel.Bind<IRepository<RatingType>>().To<Repository<RatingType>>().InRequestScope();
            kernel.Bind<IRepository<EventType>>().To<Repository<EventType>>().InRequestScope();
        }
    }
}