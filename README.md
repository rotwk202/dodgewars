# DodgeWars

Rise of the Witch-king 2.02 gaming platform that allows users to play ranked games, whether it is 1v1 or 4v4. It is designed to provide users with a reliable ladder system which keeps track of all ranked games - similar to ClanWars - but far more user-friendly.
Gamereplays.org Rise of the Witch-king 2.02 website: https://www.gamereplays.org/riseofthewitchking/

## Project development

This is more of a hobby project. It's open source, so anybody willing to join and develop is welcome.