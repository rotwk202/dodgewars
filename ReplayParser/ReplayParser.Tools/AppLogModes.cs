﻿using System;

namespace ReplayParser.Tools
{
    [Flags]
    public enum AppLogModes
    {
        NoLogging,
        InfoLogging
    }
}
