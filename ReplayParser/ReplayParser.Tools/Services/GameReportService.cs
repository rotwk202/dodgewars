﻿using Newtonsoft.Json;
using ReplayParser.Tools.Contracts;
using ReplayParser.Tools.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using log4net;
using ReplayParser.Tools.ParserTools;

namespace ReplayParser.Tools.Services
{
	public class GameReportService : IGameReportService
    {
        private List<Player> _playersInGame;
        private ApiGame _apiGame;
        private readonly ApiService _apiService;
        private readonly IReplayInfoService _replayInfoService;
        private string _reporterNickname;
        private readonly IGameDataService _gameDataService;
        private readonly string _lastReplayPath;
        private readonly ILog _logger;

        public GameReportService(string lastReplayPath)
        {
            _apiService = new ApiService();
            _lastReplayPath = lastReplayPath;
            _gameDataService = new GameDataService(_lastReplayPath);
            _replayInfoService = new ReplayInfoService(lastReplayPath);
            _logger = LogManager.GetLogger("InfoLogger");
            _logger.Info($"GameReportService _lastReplayPath: {_lastReplayPath}");
        }

        public bool ReportGame()
        {
            _gameDataService.LoadReplayData();
            _playersInGame = _gameDataService.ReplayData.HeaderFields.Players;

            _gameDataService.SetGameHostProperties(_playersInGame);
            _gameDataService.SetIsObserverProperties(_playersInGame);

            try
            {
                if (IsReportValid())
                {
                    SetReportModel();
                    SubmitReportToApi();

                    return true;
                }

                return false;
            }
            catch (Exception exception)
            {
                _logger.Info($"GameReportService.ReportGame() exception: {exception}");
                return false;
            }
        }

        private bool IsReportValid()
        {
            var userNicknames = GetUserNicknames(CoreAppDomainService._dodgeWarsUser, CoreAppDomainService._token);
            _logger.Info($@"{DateTime.Now} GameReportService.IsReportValid userNicknames: {string.Join(", ", userNicknames.Select(x => x.Nickname).ToList())}");

            if (!_gameDataService.IsFFAGame(_playersInGame))
			{
				SetReporterNickname(userNicknames, _playersInGame);
				_gameDataService.DetermineGameLoosers(_reporterNickname, _playersInGame);

				return true;
			}

            return false;
        }

        private void SetReportModel()
        {
            _apiGame = new ApiGame();

            _replayInfoService.LoadReplayFileInfo();

            _apiGame.PlayedOn = DateTimeOffset.FromUnixTimeSeconds(_gameDataService.ReplayData.TimestampEnd).UtcDateTime;
            _apiGame.LastReplayFileLength = _replayInfoService.FileLength;
            _apiGame.GameId = int.Parse(_gameDataService.ReplayData.HeaderFields.Seed);
            _apiGame.ReplayMapTextId = _gameDataService.ReplayData.HeaderFields.MapName.Replace("s/", string.Empty);

            int arrayIndex = 1;
            foreach (var player in _playersInGame)
            {
                if (!player.IsObserver)
                {
                    player.Army = "Unknown";
                    arrayIndex++;
                }
            }

            var replayFile = File.ReadAllBytes(_lastReplayPath);
            _apiGame.ReplayFile = replayFile;
            _apiGame.Players = _playersInGame;
            _replayInfoService.FileModTime = null;
        }

        private void SubmitReportToApi()
        {
            if (_apiService.IsTokenExpiredBoolean(CoreAppDomainService._token, DateTime.Now))
            {
                CoreAppDomainService._token = _apiService.GetToken(CoreAppDomainService._dodgeWarsUser);
            }

            var apiGameJson = JsonConvert.SerializeObject(_apiGame);
            if(CoreAppDomainService._appLogMode == AppLogModes.InfoLogging)
            {
                var indexOfReplayString = apiGameJson.IndexOf("\"ReplayFile\"");
                var indexOfPlayers = apiGameJson.IndexOf("\"Players");
                var truncated = apiGameJson.Remove(indexOfReplayString, indexOfPlayers - indexOfReplayString);
                var logContent = $"GameReportService.SubmitReportToApi - content: {truncated}";
                _logger.Info(logContent);
            }

            _apiService.ReportLoss(apiGameJson, CoreAppDomainService._token);
        }

        private void SetReporterNickname(List<ApiNickname> userNicknames, List<Player> gamePlayers)
        {
            if (userNicknames == null || userNicknames.Count == 0)
            {
                throw new ArgumentNullException("No nicknames bound to this dodgewars account were found.");
            }
            foreach (var nickname in userNicknames.Select(x => x.Nickname).ToList())
            {
                if (gamePlayers.Any(x => x.Nickname == nickname))
                {
                    //UWAGA: api musi byc inteligentne i nie pozwolic na powtorzenie sie tego samego nicka
                    //u tego samego albo innego gracza w systemie. w przeciwnym razie apka rzuci request
                    _reporterNickname = nickname;
                }
            }
            if (string.IsNullOrEmpty(_reporterNickname))
            {
                throw new InvalidOperationException("You probably have tried to cheat/troll someone. Please make sure your in-game name matches one of your nicknames registered in dodgewars.net system.");
            }
        }

        private List<ApiNickname> GetUserNicknames(ApiUser dodgeWarsUser, string token)
        {
            var nicknamesJson = _apiService.GetUserNicknames(dodgeWarsUser, token);
            if (string.IsNullOrEmpty(nicknamesJson))
            {
                throw new NullReferenceException("Unable to get user nicknames.");
            }

            Result<ApiNickname> result = JsonConvert.DeserializeObject<Result<ApiNickname>>(nicknamesJson);

            return result.Data;
        }
	}
}
