﻿using System;
using System.IO;

namespace ReplayParser.Tools.Services
{
    public static class ReplayNameFormatService
    {
        public static string GetReplayNameFormatToSave(string playersInGame, DateTime replaySavedOn, string gameId)
        {
            var datePart = replaySavedOn.ToString("dd.MM.yyyy");
            var timePart = replaySavedOn.ToString("HH:mm:ss");
            var timePartSavable = timePart.Replace(":", " ");

            var dateTimeString = datePart + "-" + timePartSavable;
            var newFileName = "DW_" + gameId + "__" + dateTimeString + "__" + playersInGame + ".BfME2Replay";
            newFileName = GetSafeFilename(newFileName);

            return newFileName;
        }

        private static string GetSafeFilename(string fileName)
        {
            return string.Join("]", fileName.Split(Path.GetInvalidFileNameChars()));
        }
    }
}
