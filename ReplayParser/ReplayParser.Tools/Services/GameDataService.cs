﻿using log4net;
using ReplayParser.Tools.Contracts;
using ReplayParser.Tools.Model;
using ReplayParser.Tools.ParserTools;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace ReplayParser.Tools.Services
{
    public class GameDataService : IGameDataService
    {
        private const int _observerIdentifier = -2;
        private const int _notSetTeamIdentifier = -1;
        private string _lastReplayPath;
        private ReplayData _replayData;

        public ReplayData ReplayData { get { return _replayData; } }

        private readonly ILog _logger;

        public GameDataService(string lastReplayPath)
        {
            _lastReplayPath = lastReplayPath;
            _logger = LogManager.GetLogger("InfoLogger");
        }

        public void LoadReplayData()
        {
            var lockingProcesses = FileProcessLockChecker.WhoIsLocking(_lastReplayPath);
            _logger.Info($"GameDataService.LoadReplayData(): BEFORE TRYING TO ACCESS FILE Process.GetCurrentProcess().Id: {Process.GetCurrentProcess().Id }");
            foreach (var process in lockingProcesses)
            {
                _logger.Info($"GameDataService.LoadReplayData(): This process is locking the file before trying to access it. Process name: {process.ProcessName}; Process ID: {process.Id}");
            }

            using (var replayParser = new ParserTools.ReplayParser(_lastReplayPath))
            {
                var lockingProcessesBeforeParse = FileProcessLockChecker.WhoIsLocking(_lastReplayPath);
                _logger.Info($"GameDataService.LoadReplayData(): Processes locking flie before parsing: { string.Join(",", lockingProcessesBeforeParse.Select(x => x.Id).ToList()) }");
                _replayData = replayParser.Parse();
                var lockingProcessesAfterParse = FileProcessLockChecker.WhoIsLocking(_lastReplayPath);
                _logger.Info($"GameDataService.LoadReplayData(): Processes locking flie after parsing: { string.Join(",", lockingProcessesAfterParse.Select(x => x.Id).ToList()) }");
            }

            var lockingProcessesAfterAccess = FileProcessLockChecker.WhoIsLocking(_lastReplayPath);
            _logger.Info($"GameDataService.LoadReplayData(): AFTER USING STATEMENT Process.GetCurrentProcess().Id: {Process.GetCurrentProcess().Id }");
            foreach (var process in lockingProcessesAfterAccess)
            {
                _logger.Info($"GameDataService.LoadReplayData(): This process is locking the file AFTER USING STATEMENT. Process name: {process.ProcessName}; Process ID: {process.Id}");
            }

        }

        public void SetGameHostProperties(List<Player> playersInGame)
        {
            var firstPlayerInReplayFile = playersInGame.FirstOrDefault();
            firstPlayerInReplayFile.IsGameHost = true;

            foreach (var p in playersInGame)
            {
                if(p.Nickname != firstPlayerInReplayFile.Nickname)
                {
                    p.IsGameHost = false;
                }
            }
        }

        public void SetIsObserverProperties(List<Player> playersInGame)
        {
            foreach (var player in playersInGame)
            {
                player.IsObserver = player.ArmyReplayId == _observerIdentifier;
            }
        }

        public bool IsFFAGame(List<Player> playersInGame)
        {
            var numberOfNonSetTeams = playersInGame.Count(x => x.Team == _notSetTeamIdentifier);
            var activeGameParticipants = playersInGame.Count(x => x.ArmyReplayId != _observerIdentifier);
            return (activeGameParticipants == numberOfNonSetTeams) && (activeGameParticipants > 2);
        }

        public string ConcatPlayerListToString(List<Player> playersInGame)
        {
            var sb = new StringBuilder();

            foreach (var player in playersInGame)
            {
                sb.Append(player.Nickname);
                sb.Append("-");
            }

            return sb.ToString();
        }

        public void DetermineGameLoosers(string reporterNickname, List<Player> playersInGame)
        {
            if (string.IsNullOrEmpty(reporterNickname))
            {
                throw new ArgumentNullException(nameof(reporterNickname));
            }
            if(!playersInGame.Any(x => x.Nickname == reporterNickname))
            {
                throw new InvalidOperationException("You probably have tried to cheat/troll someone. Please make sure your in-game name matches one of your nicknames registered in dodgewars.net system.");
            }

            var activeGameParticipants = playersInGame.Count(x => x.ArmyReplayId != _observerIdentifier);
            bool isOneVsOneMatch = activeGameParticipants == 2;

            if (isOneVsOneMatch)
            {
                var loosingPlayerNickname = playersInGame.First(x => x.Nickname == reporterNickname && !x.IsObserver).Nickname;
                foreach (var player in playersInGame)
                {
                    player.IsLooser = player.Nickname == loosingPlayerNickname;
                }
            }
            else
            {
                int loosingTeam = playersInGame.First(x => x.Nickname == reporterNickname).Team;
                foreach (var player in playersInGame)
                {
                    player.IsLooser = player.Team == loosingTeam;
                }
            }
        }

        public bool IsSelfReport(List<Player> playersInGame, List<ApiNickname> userNicknames)
        {
            var gameLoosers = playersInGame
                .Where(x => x.IsLooser && !x.IsObserver)
                .ToList();
            var gameWinners = playersInGame
                .Where(x => !x.IsLooser && !x.IsObserver)
                .ToList();

            var userDbNicknames = userNicknames.Select(x => x.Nickname).ToList();

            bool occuredOnLoosersSite = false;
            bool occuredOnWinnersSite = false;

            //occured on loosers site
            occuredOnLoosersSite = gameLoosers.Any(x => userDbNicknames.Contains(x.Nickname));

            //occured on winners site
            occuredOnWinnersSite = gameWinners.Any(x => userDbNicknames.Contains(x.Nickname));

            return occuredOnLoosersSite && occuredOnWinnersSite;
        }
    }
}
