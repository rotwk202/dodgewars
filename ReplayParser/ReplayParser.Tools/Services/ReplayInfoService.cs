﻿using ReplayParser.Tools.Contracts;
using System;
//using System.ComponentModel;
using System.IO;
//using System.Runtime.InteropServices;

namespace ReplayParser.Tools.Services
{
    public class ReplayInfoService : IReplayInfoService
    {
        private readonly string _fullFilePath;
        private static DateTime? _fileModTime;
        private static long? _fileLength;

        //TODO: usunac te settery?
        public DateTime? FileModTime { get { return _fileModTime; } set { _fileModTime = null; } }
        public long? FileLength { get { return _fileLength; } set { _fileLength = null; } }

        public ReplayInfoService(string fullFilePath)
        {
            _fullFilePath = fullFilePath;
        }

        public void LoadReplayFileInfo()
        {
            FileInfo fi = new FileInfo(_fullFilePath);
            //var lrFileLengthOnDisk = GetFileSizeOnDisk(_fullFilePath);

            _fileModTime = fi.LastWriteTime;
            _fileLength = fi.Length;
        }

        public bool IsReplayLockedCheckOnce()
        {
            bool isLocked = true;

            try
            {
                using (Stream stream = new FileStream(_fullFilePath, FileMode.Open))
                {
                    isLocked = false;
                }
            }
            catch
            {
                //check here why it failed and ask user to retry if the file is in use.
            }

            return isLocked;
        }

        public bool IsReplayLocked()
        {
            bool isLocked = true;

            while (isLocked)
            {
                try
                {
                    using (Stream stream = new FileStream(_fullFilePath, FileMode.Open))
                    {
                        isLocked = false;
                    }
                }
                catch
                {
                    //check here why it failed and ask user to retry if the file is in use.
                }
            }

            return isLocked;
        }

        //[DllImport("kernel32.dll")]
        //static extern uint GetCompressedFileSizeW([In, MarshalAs(UnmanagedType.LPWStr)] string lpFileName,
        //   [Out, MarshalAs(UnmanagedType.U4)] out uint lpFileSizeHigh);

        //[DllImport("kernel32.dll", SetLastError = true, PreserveSig = true)]
        //static extern int GetDiskFreeSpaceW([In, MarshalAs(UnmanagedType.LPWStr)] string lpRootPathName,
        //   out uint lpSectorsPerCluster, out uint lpBytesPerSector, out uint lpNumberOfFreeClusters,
        //   out uint lpTotalNumberOfClusters);

        //private long GetFileSizeOnDisk(string file)
        //{
        //    FileInfo info = new FileInfo(file);

        //    int result = GetDiskFreeSpaceW(info.Directory.Root.FullName, out uint sectorsPerCluster, out uint bytesPerSector, out uint dummy, out dummy);
        //    if (result == 0)
        //    {
        //        throw new Win32Exception();
        //    }

        //    uint clusterSize = sectorsPerCluster * bytesPerSector;
        //    uint losize = GetCompressedFileSizeW(file, out uint hosize);
        //    long size = (long)hosize << 32 | losize;

        //    return ((size + clusterSize - 1) / clusterSize) * clusterSize;
        //}
    }
}
