﻿using Microsoft.Win32;

namespace ReplayParser.Tools.Services
{
    public static class GameLanguageService
    {
        private const string GameSubKey = @"SOFTWARE\Electronic Arts\Electronic Arts\The Lord of the Rings, The Rise of the Witch-king";
        private const string GameLanguageKeyName = "Language";

        public static string GetGameLanguage()
        {
            string result = null;
            using (var view32 = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32))
            {
                using (var key = view32.OpenSubKey(GameSubKey, false))
                {
                    result = key.GetValue(GameLanguageKeyName).ToString().ToLower();
                }
            }
            return result;
        }
    }
}
