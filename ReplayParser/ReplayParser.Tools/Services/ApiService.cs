﻿using ReplayParser.Tools.Model;
using System;
using System.IO;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using ReplayParser.Tools.Contracts;
using System.Configuration;

namespace ReplayParser.Tools.Services
{
    public class ApiService : IApiService
    {
        private static readonly string _apiServerEndpoint = ConfigurationManager.AppSettings["TargetServer"];
        private readonly Uri _apiServer;
        private const string _getTokenRoute = "api/Account/GetToken";
        private const string _getUserNicknamesRoute = "api/Account/GetUserNicknames";
        private const string _reportLossRoute = "api/GameReport/ReportLoss";
        private const string _isTokenExpiredRoute = "api/Account/IsTokenExpired";
        private const string _clientName = "console";

        public ApiService()
        {
            _apiServer = new Uri(_apiServerEndpoint);
        }

        public string GetToken(ApiUser user)
        {
            var url = new Uri(_apiServer, _getTokenRoute).ToString();
            var serializedJson = JsonConvert.SerializeObject(user);
            var encoding = new UTF8Encoding(false);
            var data = encoding.GetBytes(serializedJson);

            WebRequest request = WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Headers.Add("ClientName", _clientName);
            request.ContentLength = data.Length;
            request.Timeout = 10000;
            using (var sr = request.GetRequestStream())
            {
                sr.Write(data, 0, data.Length);
            }

            // Get the response.
            string responseFromServer = string.Empty;
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream dataStream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(dataStream))
                {
                    responseFromServer = reader.ReadToEnd();
                }
            }
            catch (WebException wex)
            {
                throw wex;
            }

            //return responseFromServer.Replace("\"", string.Empty);
            return responseFromServer;
        }

        // TODO: dokonczyc implementacje tej metody
        public string GetUserNicknames(ApiUser user, string token)
        {
            var url = new Uri(_apiServer, _getUserNicknamesRoute).ToString();
            WebRequest request = WebRequest.Create(url);
            request.Method = "GET";
            request.ContentType = "application/json";
            request.ContentLength = 0;
            request.Headers.Add("Token", token);
            request.Headers.Add("ClientName", _clientName);
            // Get the response.
            string responseFromServer = string.Empty;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream dataStream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(dataStream))
            {
                responseFromServer = reader.ReadToEnd();
            }

            return responseFromServer.Replace(@"\", string.Empty).TrimStart('"').TrimEnd('"');
        }

        public void ReportLoss(string apiGameJson, string token)
        {
            var url = new Uri(_apiServer, _reportLossRoute).ToString();
            var encoding = new UTF8Encoding(false);
            var data = encoding.GetBytes(apiGameJson);

            WebRequest request = WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Headers.Add("Token", token);
            request.Headers.Add("ClientName", _clientName);
            request.ContentLength = data.Length;
            using (var sr = request.GetRequestStream())
            {
                sr.Write(data, 0, data.Length);
            }

            // Get the response.
            // string responseFromServer = string.Empty;
            // TODO: no response? What should be here?
            try
            {
                using (var response = request.GetResponse())
                {
                }
            }
            catch (WebException wex)
            {
                if (wex.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)wex.Response)
                    {
                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                        {
                            string error = reader.ReadToEnd();
                            throw new InvalidOperationException(error);
                        }
                    }
                }
                else
                {
                    throw wex;
                }
            }
        }

        public string IsTokenExpired(string token, DateTime requestTime)
        {
            if (string.IsNullOrEmpty(token))
            {
                throw new ArgumentException("No token was passed in.");
            }

            // TODO: requestTime can never be null. It is Value type. Maybe it meant to be nullable (DateTime?)?
            if (requestTime == null)
            {
                throw new ArgumentException("No datetime was provided.");
            }

            var url = new Uri(_apiServer, _isTokenExpiredRoute).ToString();
            //var serializedJson = JsonConvert.SerializeObject(requestTime);
            var encoding = new UTF8Encoding(false);
            var requestModel = new ApiRequest() { RequestTime = requestTime };
            var jsonModel = JsonConvert.SerializeObject(requestModel);
            var data = encoding.GetBytes(jsonModel);

            WebRequest request = WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Headers.Add("Token", token);
            request.Headers.Add("ClientName", _clientName);
            request.ContentLength = data.Length;
            request.Timeout = 10000;
            using (var sr = request.GetRequestStream())
            {
                sr.Write(data, 0, data.Length);
            }

            // Get the response.
            string responseFromServer = string.Empty;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream dataStream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(dataStream))
            {
                responseFromServer = reader.ReadToEnd();
            }

            return responseFromServer.Replace(@"\", string.Empty).TrimStart('"').TrimEnd('"');
        }

        public bool IsTokenExpiredBoolean(string token, DateTime requestTime)
        {
            if (string.IsNullOrEmpty(token))
            {
                throw new ArgumentException("No token was passed in.");
            }

            // TODO: requestTime can never be null. It is Value type. Maybe it meant to be nullable (DateTime?)?
            if (requestTime == null)
            {
                throw new ArgumentException("No datetime was provided.");
            }

            var url = new Uri(_apiServer, _isTokenExpiredRoute).ToString();
            //var serializedJson = JsonConvert.SerializeObject(requestTime);
            var encoding = new UTF8Encoding(false);
            var requestModel = new ApiRequest() { RequestTime = requestTime };
            var jsonModel = JsonConvert.SerializeObject(requestModel);
            var data = encoding.GetBytes(jsonModel);

            WebRequest request = WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.Headers.Add("Token", token);
            request.Headers.Add("ClientName", _clientName);
            request.ContentLength = data.Length;
            request.Timeout = 10000;
            using (var sr = request.GetRequestStream())
            {
                sr.Write(data, 0, data.Length);
            }

            // Get the response.
            string responseFromServer = string.Empty;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            using (Stream dataStream = response.GetResponseStream())
            using (StreamReader reader = new StreamReader(dataStream))
            {
                responseFromServer = reader.ReadToEnd();
            }
            var responseString = responseFromServer.Replace(@"\", string.Empty).TrimStart('"').TrimEnd('"');

            return Convert.ToBoolean(responseString);
        }
    }
}
