﻿using log4net;
using ReplayParser.Tools.Contracts;
using ReplayParser.Tools.Model;
using ReplayParser.Tools.ParserTools;
using ReplayParser.Tools.Properties;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ReplayParser.Tools.Services
{
	public class ReplayEventService
    {
        private readonly string _lastReplayPath;
        private readonly IGameReportService _gameReportService;
        private readonly IGameDataService _gameDataService;
        private readonly IReplayInfoService _replayInfoService;
        public event EventHandler GameReportedSuccessfully;
        private readonly ILog _logger;

        public ReplayEventService(string lastReplayPath)
        {
            _logger = LogManager.GetLogger("InfoLogger");
            _lastReplayPath = lastReplayPath;
            _gameDataService = new GameDataService(_lastReplayPath);
            _replayInfoService = new ReplayInfoService(lastReplayPath);
            _gameReportService = new GameReportService(_lastReplayPath);
            _logger.Info($@"{DateTime.Now} ReplayEventService.Constructor _lastReplayPath: {_lastReplayPath}");
        }

        public void OnRoTWKLossButtonPress(object source, HotKeyEventArgsRoTWKLoss e)
        {
            _logger.Info($@"{DateTime.Now} ReplayEventService.OnRoTWKLossButtonPress report loss button pressed.");
            var lossReportedCorrectly = _gameReportService.ReportGame();
            EventHandler handler = GameReportedSuccessfully;

            if (lossReportedCorrectly)
            {
                PlayerLoseNotification();
                handler?.Invoke(this, EventArgs.Empty);
            }
        }

        public void OnReplayFileChange(object source, FileSystemEventArgs e)
        {
            CallSaveEventOnce(source);
        }

        private void CallSaveEventOnce(object source)
        {
            var watcher = source as FileSystemWatcher;
            try
            {
                watcher.EnableRaisingEvents = false;
                if (_replayInfoService.IsReplayLocked())
                {
                    return;
                }

                _gameDataService.LoadReplayData();
                _replayInfoService.LoadReplayFileInfo();
                var replaySavedOn = DateTimeOffset.FromUnixTimeSeconds(_gameDataService.ReplayData.TimestampEnd).UtcDateTime;

                var playersInGame = _gameDataService.ReplayData.HeaderFields.Players;

                var players = _gameDataService.ConcatPlayerListToString(playersInGame);
                var gameID = _gameDataService.ReplayData.HeaderFields.Seed;
                var newFileName = ReplayNameFormatService.GetReplayNameFormatToSave(players, replaySavedOn, gameID);

                var replayDir = Path.GetDirectoryName(_lastReplayPath);

                try
                {
                    File.Copy(_lastReplayPath, Path.Combine(replayDir, newFileName));
                }
                catch (Exception ex)
                {
                    _logger.Info($"WARNING: Could not backup replay: {newFileName }. Exception: {ex}");
                }
            }
            finally
            {
                watcher.EnableRaisingEvents = true;
            }
        }

        private void PlayerLoseNotification()
        {
            var allResources = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceNames();
            if (allResources.Length > 0)
            {
                System.Media.SoundPlayer player = new System.Media.SoundPlayer(Resources.eva_defeated_notification);
                player.Play();
            }
        }
    }
}
