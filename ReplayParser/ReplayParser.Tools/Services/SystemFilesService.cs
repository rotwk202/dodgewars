﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ReplayParser.Tools.Services
{
    public static class SystemFilesService
    {
        private static readonly string _appDataRoamingFolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
        private static readonly Dictionary<string, string> _filePathsDict;

        private const string EnglishFilePath = @"My The Lord of the Rings, The Rise of the Witch-king Files\Replays\Last Replay.BfME2Replay";
        private const string GermanRotwKFilePath = @"Meine Der Herr der Ringe™, Aufstieg des Hexenkönigs™-Dateien\Replays\Letzte Wiederholung.BfME2Replay";
        private const string SpanishRotwKFilePath = @"Mis archivos de El Señor de los Anillos, El Resurgir del Rey Brujo\Replays\Última repetición.BfME2Replay";
        private const string FrenchRotwKFilePath = @"Mes fichiers de LSDA, L'Avènement du Roi-sorcier™\Replays\Dernière Redif..BfME2Replay";
        private const string ItalianRotwKFilePath = @"File de Il Signore degli Anelli™ - L'Ascesa del Re Stregone™\Replays\Ultimo replay.BfME2Replay";
        private const string RussianRotwKFilePath = @"Властелин Колец, Под знаменем Короля-чародея - Мои файлы\Replays\Последний видеоролик.BfME2Replay";
        private const string PolishRotwKFilePath = @"Moje pliki gry Władca Pierścieni, Król Nazguli\Replays\Ostatnia powtórka.BfME2Replay";
        private const string DutchRotwKFilePath = @"Mijn The Lord of the Rings, The Rise of the Witch-king-bestanden\Replays\Laatste herhaling.BfME2Replay";

        static SystemFilesService()
        {
            _filePathsDict = new Dictionary<string, string>
            {
                { "english", Path.Combine(_appDataRoamingFolder, EnglishFilePath) },
                { "german", Path.Combine(_appDataRoamingFolder, GermanRotwKFilePath) },
                { "spanish", Path.Combine(_appDataRoamingFolder, SpanishRotwKFilePath) },
                { "french", Path.Combine(_appDataRoamingFolder, FrenchRotwKFilePath) },
                { "italian", Path.Combine(_appDataRoamingFolder, ItalianRotwKFilePath) },
                { "russian", Path.Combine(_appDataRoamingFolder, RussianRotwKFilePath) },
                { "polish", Path.Combine(_appDataRoamingFolder, PolishRotwKFilePath) },
                { "dutch", Path.Combine(_appDataRoamingFolder, DutchRotwKFilePath) }
            };
        }

        public static string GetLastReplayFilePath(string gameLanguage)
        {
            string result = null;
            result = _filePathsDict[gameLanguage];
            return result;
        }
    }
}
