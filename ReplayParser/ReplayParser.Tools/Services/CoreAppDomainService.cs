﻿using Newtonsoft.Json;
using ReplayParser.Tools.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using ReplayParser.Tools.Contracts;
using System.Windows.Forms;
using Microsoft.Win32;
using log4net;
using System.Configuration;

namespace ReplayParser.Tools.Services
{
    public class CoreAppDomainService : ICoreAppDomainService
    {
        private readonly IApiService _apiService;
        public static ApiUser _dodgeWarsUser;
        public static string _token = string.Empty;
        private string _lastReplayPath = string.Empty;
        private string _gameLanguage = string.Empty;
        private const Keys _reportLossButtonRoTWK = Keys.F6;
        public static AppLogModes _appLogMode;
        public static string _appLogFilePath;
        private readonly ILog _logger;
        private ReplayEventService _replayEventService;
        private readonly string _myCredentialsFilePath;
        public event EventHandler UpdateGameReportMessage;
        private FileSystemWatcher _watcher;

        public CoreAppDomainService(IApiService apiService)
        {
            _apiService = apiService;
            _logger = LogManager.GetLogger("InfoLogger");
            _myCredentialsFilePath = ConfigurationManager.AppSettings["UserCredentialsFilePath"];
            _lastReplayPath = ConfigurationManager.AppSettings["LastReplayFilePath"];
            _gameLanguage = ConfigurationManager.AppSettings["GameLanguage"];
        }

        private void SetLoggingFlag()
        {
            _appLogMode = AppLogModes.InfoLogging;
        }

        public bool LogInUser(bool isRememberMeChecked, string login, string password)
        {
            _dodgeWarsUser = new ApiUser();

            if(!string.IsNullOrEmpty(login) && !string.IsNullOrEmpty(password))
            {
                _dodgeWarsUser.Email = login;
                _dodgeWarsUser.Password = password;
                _token = GetToken(_dodgeWarsUser);

                if (!string.IsNullOrEmpty(_token))
                {
                    if(isRememberMeChecked)
                    {
                        WriteUserCredentialsToFile(_dodgeWarsUser.Email, _dodgeWarsUser.Password);
                    }
                    return true;
                }
                return false;
            }
            else
            {
                SetUserCredentialsFromFile(_dodgeWarsUser);

                if (_dodgeWarsUser.Email != null && _dodgeWarsUser.Password != null)
                {
                    _token = GetToken(_dodgeWarsUser);

                    if (!string.IsNullOrEmpty(_token))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public void StartProgram()
        {
            SetLoggingFlag();
            SetReplayFilePath();
            InitializeService();
            RegisterGlobalHotkeys();
            WatchReplayFileChanges();
        }

        private void RegisterGlobalHotkeys()
        {
            HotKeyRoTWKLossListener.RegisterHotKey(_reportLossButtonRoTWK, KeyModifiersRoTWKLoss.NoRepeat);
            HotKeyRoTWKLossListener.HotKeyPressed += _replayEventService.OnRoTWKLossButtonPress;
            _logger.Info($@"{DateTime.Now} CoreAppDomainService.RegisterGlobalHotkeys global hotkeu registered successfully.");
        }

        private void InitializeService()
        {
            _replayEventService = new ReplayEventService(_lastReplayPath);
            _replayEventService.GameReportedSuccessfully += NotifyAppForm;
        }

        private void NotifyAppForm(object sender, EventArgs e)
        {
            UpdateGameReportMessage?.Invoke(this, e);
        }

        private void WatchReplayFileChanges()
        {
            _watcher = new FileSystemWatcher
            {
                Path = Directory.GetParent(_lastReplayPath).FullName,
                NotifyFilter = NotifyFilters.LastWrite,
                Filter = Path.GetFileName(_lastReplayPath)
            };

            _watcher.Changed += _replayEventService.OnReplayFileChange;
            _watcher.EnableRaisingEvents = true;
        }

        private void SetReplayFilePath()
        {
            if(!string.IsNullOrEmpty(_lastReplayPath))
            {
                MessageBox.Show($"Set last replay file path to: {_lastReplayPath}");
                return;
            }

            try
            {
                if (string.IsNullOrEmpty(_gameLanguage))
                {
                    _gameLanguage = GameLanguageService.GetGameLanguage();
                }
                    
                _logger.Info($"CoreAppDomainService.SetReplayFilePath() gameLanguage: {_gameLanguage}");
                if(string.IsNullOrEmpty(_gameLanguage))
                {
                    throw new NullReferenceException("Unable to find the game language. Please use the configuration file, setting 'GameLanguage'");
                }

                _lastReplayPath = SystemFilesService.GetLastReplayFilePath(_gameLanguage);
                if (string.IsNullOrEmpty(_lastReplayPath))
                {
                    throw new NullReferenceException("Unable to find the last replay file path. Please provide it on configuration file.");
                }

                _logger.Info($"The LastReplay file directory was successfully tracked. The file name is: {_lastReplayPath}");
                MessageBox.Show($"Set last replay file path to: {_lastReplayPath}");
            }
            catch (Exception e)
            {
                _logger.Info($"CoreAppDomainService.SetReplayFilePath() exception: {e}");
                MessageBox.Show(e.Message);
                Environment.Exit(-1);
            }
        }

        private void SetUserCredentialsFromFile(ApiUser dodgewarsUser)
        {
            var data = GetUserCredentialsFromFile();
            if (data.Count == 0)
            {
                return;
            }
            dodgewarsUser.Email = data[0];
            dodgewarsUser.Password = data[1];
        }

        private string GetToken(ApiUser dodgeWarsUser)
        {
            Result<string> failResult;

            var output = string.Empty;
            try
            {
                output = CallApiForToken(dodgeWarsUser);
            }
            catch (WebException wex)
            {
                if (wex.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)wex.Response)
                    using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                    {
                        failResult = JsonConvert.DeserializeObject<Result<string>>(reader.ReadToEnd());
                        //Console.WriteLine("\n*********************\n" + failResult.Message + "\n*********************\n");
                        return output;
                        //TOASK: kompilator nie pozwolil rzucic tego dalej to Program.cs. krzyczy nieobsluzony wyjatek
                        //throw new InvalidOperationException("\n*********************\n" + failResult.Message + "\n*********************\n");
                    }
                }
                else
                {
                    //TOASK: kompilator nie pozwolil rzucic tego dalej to Program.cs. krzyczy nieobsluzony wyjatek
                    //throw new Exception("\n*********************\n" + wex.InnerException.Message + "\n*********************\n");
                    if (wex.InnerException != null)
                    {
                        //Console.WriteLine("\n*********************\n" + wex.InnerException.Message + "\n*********************\n");
                    }
                    else
                    {
                        //Console.WriteLine("\n*********************\n" + wex.Message + ". Destination server is likely unreachable." + "\n*********************\n");
                    }
                    return output;
                }
            }

            var tokenModel = CustomSerializer.Deserialize<ApiTokenDetail>(output);
            return tokenModel.Token;
        }

        private string CallApiForToken(ApiUser dodgeWarsUser)
        {
            try
            {
                return _apiService.GetToken(dodgeWarsUser);
            }
            catch (WebException wex)
            {
                throw wex;
            }
        }

        private void WriteUserCredentialsToFile(string email, string password)
        {
            var content = string.Format("{0},{1}", email, password);
            using (var fileStream = File.OpenWrite(_myCredentialsFilePath))
            {
                using (TextWriter tw = new StreamWriter(fileStream))
                {
                    tw.WriteLine(content);
                    tw.Close();
                }
            }
        }

        public bool IsTokenOutOfDate(string token, DateTime requestTime)
        {
            var stringFlag = _apiService.IsTokenExpired(token, requestTime);

            return Convert.ToBoolean(stringFlag);
        }

        public List<ApiNickname> GetUserNicknames(ApiUser dodgeWarsUser, string token)
        {
            var nicknamesJson = _apiService.GetUserNicknames(dodgeWarsUser, token);
            if(string.IsNullOrEmpty(nicknamesJson))
            {
                _logger.Info($@"{DateTime.Now} CoreAppDomainService.GetUserNicknames no nicknames were found.");
                return null;
            }

            Result<ApiNickname> result = JsonConvert.DeserializeObject<Result<ApiNickname>>(nicknamesJson);

            return result.Data;
        }

        private List<string> GetUserCredentialsFromFile()
        {
            var output = new List<string>();
            if (!File.Exists(_myCredentialsFilePath))
            {
                return output;
            }

            var content = File.ReadAllLines(_myCredentialsFilePath);

            if(content.Length == 0)
            {
                return output;
            }

            string c = content[0];

            return c.Split(',').ToList();
        }

        public void SetReporterNickname(ref string reporterNickname, List<ApiNickname> userNicknames, List<Player> gamePlayers)
        {
            if (userNicknames == null || userNicknames.Count == 0)
            {
                throw new ArgumentNullException("No nicknames bound to this dodgewars account were found.");
            }
            foreach (var nickname in userNicknames.Select(x => x.Nickname).ToList())
            {
                if (gamePlayers.Any(x => x.Nickname == nickname))
                {
                    //UWAGA: api musi byc inteligentne i nie pozwolic na powtorzenie sie tego samego nicka
                    //u tego samego albo innego gracza w systemie. w przeciwnym razie apka rzuci request
                    reporterNickname = nickname;
                }
            }
            if (string.IsNullOrEmpty(reporterNickname))
            {
                throw new InvalidOperationException("You probably have tried to cheat/troll someone. Please make sure your in-game name matches one of your nicknames registered in dodgewars.net system.");
            }
        }

        private void RemoveCredentialsFile()
        {
            if (File.Exists(_myCredentialsFilePath))
            {
                File.Delete(_myCredentialsFilePath);
            }
        }

        public void RememberMe(bool isRememberMeChecked)
        {
            if (_dodgeWarsUser is null) _dodgeWarsUser = new ApiUser();
            if (isRememberMeChecked)
            {
                SetUserCredentialsFromFile(_dodgeWarsUser);
            }
            else
            {
                RemoveCredentialsFile();
            }
        }

        public bool IsAppStartupSet()
        {
            var value = Registry.GetValue(@"HKEY_CURRENT_USER\SOFTWARE\\Microsoft\Windows\CurrentVersion\Run", "ReplayParser", null);

            return value != null;
        }

        public void SetApplicationStartup(bool isAppStartupChecked)
        {
            RegistryKey rk = Registry.CurrentUser.OpenSubKey
                (@"SOFTWARE\\Microsoft\Windows\CurrentVersion\Run", true);

            if (isAppStartupChecked)
            {
                rk.SetValue("ReplayParser", Application.ExecutablePath);
            }
            else
            {
                rk.DeleteValue("ReplayParser", false);
            }
        }
    }
}
