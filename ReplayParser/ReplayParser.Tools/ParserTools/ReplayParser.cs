﻿using log4net;
using ReplayParser.Tools.Model;
using ReplayParser.Tools.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;

namespace ReplayParser.Tools.ParserTools
{
    public class ReplayParser : IDisposable
    {
        private const string validFileType = "BFME2RPL";
        private readonly ILog _logger;
        private const int _nickIndex = 0;
        private const int _teamIndex = 7;
        private const int _armyIndex = 5;

        private FileStream _fileStream;
        private BinaryReader _reader;

        public ReplayParser(string fullPath)
        {
            _logger = LogManager.GetLogger("InfoLogger");
            CreateReader(fullPath);
        }

        private void CreateReader(string fullPath)
        {
            try
            {
                var fileInfo = new FileInfo(fullPath);
                _logger.Info($"Try block: fileInfo.Length: {fileInfo.Length}");
                _fileStream = fileInfo.Open(FileMode.Open, FileAccess.Read, FileShare.Read);
                _fileStream.Position = 0;
                _reader = new BinaryReader(_fileStream, Encoding.ASCII);
                _logger.Info($"ReplayParser.CreateReader try block: fileStream.Length: {_fileStream.Length}, fileStream.Position: {_fileStream.Position}");
            }
            catch (IOException ex)
            {
                _logger.Info($"ReplayParser.CreateReader: error trying to access filestream: {ex}");
                var lockingProcesses = FileProcessLockChecker.WhoIsLocking(fullPath);
                _logger.Info($"ReplayParser.CreateReader(): Process.GetCurrentProcess().Id: {Process.GetCurrentProcess().Id }");
                foreach (var process in lockingProcesses)
                {
                    _logger.Info($"ReplayParser.CreateReader(): catch block this process is locking the file. Process name: {process.ProcessName}; Process ID: {process.Id}");
                }

                if (_fileStream != null)
                {
                    _fileStream.Dispose();
                }
                Thread.Sleep(50);
            }
        }

        public void Dispose()
        {
            _reader?.Dispose();
            _fileStream?.Dispose();
        }

        public ReplayData Parse()
        {
            var replayData = new ReplayData();

            string fileType = new string(_reader.ReadChars(8));
            if (fileType != validFileType)
            {
                return replayData;
            }

            replayData.ReplayFileType = fileType;
            replayData.TimestampStart = _reader.ReadUInt32();
            replayData.TimestampEnd = _reader.ReadUInt32();
            if(replayData.TimestampEnd == 0)
            {
                replayData.TimestampEnd = replayData.TimestampStart;
            }

            replayData.Unknown1 = _reader.ReadBytes(21);
            replayData.FileName = Read2ByteString();

            replayData.DateTimeArray[0] = _reader.ReadUInt16();
            replayData.DateTimeArray[1] = _reader.ReadUInt16();
            replayData.DateTimeArray[2] = _reader.ReadUInt16();
            replayData.DateTimeArray[3] = _reader.ReadUInt16();
            replayData.DateTimeArray[4] = _reader.ReadUInt16();
            replayData.DateTimeArray[5] = _reader.ReadUInt16();
            replayData.DateTimeArray[6] = _reader.ReadUInt16();
            replayData.DateTimeArray[7] = _reader.ReadUInt16();

            replayData.Version = Read2ByteString();
            replayData.BuildDate = Read2ByteString();
            replayData.VersionMinor = _reader.ReadUInt16();
            replayData.VersionMajor = _reader.ReadUInt16();

            replayData.MagicHash = _reader.ReadBytes(13);

            replayData.HeaderRawData = Read1ByteString();

            if (!string.IsNullOrWhiteSpace(replayData.HeaderRawData))
            {
                ParseHeader(replayData);
            }

            replayData.Unknown2 = _reader.ReadUInt16();
            replayData.Unknown3[0] = _reader.ReadUInt32();
            replayData.Unknown3[1] = _reader.ReadUInt32();
            replayData.Unknown3[2] = _reader.ReadUInt32();
            replayData.Unknown3[3] = _reader.ReadUInt32();
            replayData.Unknown3[4] = _reader.ReadUInt32();
            replayData.Unknown3[5] = _reader.ReadUInt32();
            return replayData;
        }

        private void ParseHeader(ReplayData replayData)
        {
            string[] headerRawData = replayData.HeaderRawData.Split(';');

            var headerFields = new HeaderFields
            {
                MapName = headerRawData[0],
                MapCrc = headerRawData[1].Replace("MC=", string.Empty),
                MapFileSize = headerRawData[2].Replace("MS=", string.Empty),
                Seed = headerRawData[3].Replace("SD=", string.Empty),
                MatchId = headerRawData[4].Replace("GSID=", string.Empty),
                Gt = headerRawData[5].Replace("GT=", string.Empty),
                Si = headerRawData[6].Replace("SI=", string.Empty),
                Gr = headerRawData[7].Replace("GR=", string.Empty),
                Unknown1 = headerRawData[9]
            };

            headerFields.Players = ParsePlayers(headerRawData[8]);

            replayData.HeaderFields = headerFields;
        }

        private List<Player> ParsePlayers(string playerData)
        {
            var players = new List<Player>();

            if (string.IsNullOrWhiteSpace(playerData) || string.Equals(playerData.Substring(1, 2), "S=", StringComparison.InvariantCultureIgnoreCase))
            {
                return players;
            }

            foreach (string token in playerData.Substring(2, playerData.Length - 2).Split(':'))
            {
                if (string.IsNullOrWhiteSpace(token))
                {
                    continue;
                }

                bool isPlayer = string.Equals(token.Substring(0, 1), "H", StringComparison.InvariantCultureIgnoreCase);

                if (!isPlayer)
                {
                    continue;
                }

                players.Add(ParsePlayerData(token));
            }
            return players;
        }

        private Player ParsePlayerData(string token)
        {
            var playerData = token.Split(',');
            return new Player
            {
                Nickname = playerData[_nickIndex].Substring(1, playerData[_nickIndex].Length - 1),
                ArmyReplayId = int.Parse(playerData[_armyIndex]),
                Team = int.Parse(playerData[_teamIndex])
            };
        }

        private string ParseHexIPAddress(string ipHexString)
        {
            uint ipAsUint = uint.Parse(ipHexString, NumberStyles.AllowHexSpecifier);

            if (BitConverter.IsLittleEndian)
            {
                ipAsUint = BitConverter.ToUInt32(BitConverter.GetBytes(ipAsUint).Reverse().ToArray(), 0);
            }

            return new IPAddress(ipAsUint).ToString();
        }

        private string Read1ByteString()
        {
            var stringBuilder = new StringBuilder();

            while (true)
            {
                var character = _reader.ReadChar();

                if (character == '\0')
                {
                    break;
                }

                stringBuilder.Append(character);
            }

            return stringBuilder.ToString();
        }

        private string Read2ByteString()
        {
            var stringBuilder = new StringBuilder();

            while (true)
            {
                short characterValue = _reader.ReadInt16();

                if (characterValue == 0)
                {
                    break;
                }

                stringBuilder.Append(Convert.ToChar(char.ConvertFromUtf32(characterValue)));
            }

            return stringBuilder.ToString();
        }
    }
}