﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReplayParser.Tools.Model
{
    public class ApiTokenDetail
    {
        public string Token { get; set; }

        public string TokenExpiresOn { get; set; }
    }
}
