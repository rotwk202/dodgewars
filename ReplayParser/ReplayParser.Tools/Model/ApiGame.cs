﻿using System;
using System.Collections.Generic;

namespace ReplayParser.Tools.Model
{
    public class ApiGame
    {
        public int GameId { get; set; }

        public string ReplayMapTextId { get; set; }

        public long? LastReplayFileLength { get; set; }

        public DateTime PlayedOn { get; set; }

        public byte[] ReplayFile { get; set; }

        public List<Player> Players { get; set; }
    }
}
