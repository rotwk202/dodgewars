﻿using System.Collections.Generic;

namespace ReplayParser.Tools.Model
{
    public class Result<T>
    {
        public bool IsSuccessfull { get; set; }

        public string Message { get; set; }

        public List<T> Data { get; set; }
    }
}
