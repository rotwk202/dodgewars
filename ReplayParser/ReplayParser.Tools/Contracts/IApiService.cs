﻿using ReplayParser.Tools.Model;
using System;

namespace ReplayParser.Tools.Contracts
{
    public interface IApiService
    {
        string GetToken(ApiUser user);
        string GetUserNicknames(ApiUser user, string token);
        void ReportLoss(string playerInGameJson, string token);
        string IsTokenExpired(string token, DateTime requestTime);
        bool IsTokenExpiredBoolean(string token, DateTime requestTime);
    }
}
