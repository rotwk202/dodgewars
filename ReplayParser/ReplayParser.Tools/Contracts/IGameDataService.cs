﻿using ReplayParser.Tools.Model;
using ReplayParser.Tools.ParserTools;
using System.Collections.Generic;

namespace ReplayParser.Tools.Contracts
{
    public interface IGameDataService
    {
        void DetermineGameLoosers(string reporterNickname, List<Player> playersInGame);
        bool IsSelfReport(List<Player> playersInGame, List<ApiNickname> userNicknames);
		bool IsFFAGame(List<Player> playersInGame);
        string ConcatPlayerListToString(List<Player> playersInGame);
        void SetGameHostProperties(List<Player> playersInGame);
        void SetIsObserverProperties(List<Player> playersInGame);
        void LoadReplayData();

        ReplayData ReplayData { get; }
    }
}
