﻿namespace ReplayParser.Tools.Contracts
{
	public interface IGameReportService
    {
        bool ReportGame();
    }
}
