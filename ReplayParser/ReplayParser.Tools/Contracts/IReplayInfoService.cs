﻿using System;

namespace ReplayParser.Tools.Contracts
{
    public interface IReplayInfoService
    {
        DateTime? FileModTime { get; set; }
        long? FileLength { get; set; }
        void LoadReplayFileInfo();
        bool IsReplayLockedCheckOnce();
        bool IsReplayLocked();
    }
}
