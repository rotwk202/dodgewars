﻿using ReplayParser.Tools.Model;
using System;
using System.Collections.Generic;

namespace ReplayParser.Tools.Contracts
{
    public interface ICoreAppDomainService
    {
        bool LogInUser(bool isRememberMeChecked, string login, string password);
        void StartProgram();
        bool IsTokenOutOfDate(string token, DateTime requestTime);
        List<ApiNickname> GetUserNicknames(ApiUser dodgeWarsUser, string token);
        void SetReporterNickname(ref string reporterNickname, List<ApiNickname> userNicknames, List<Player> gamePlayers);
        void RememberMe(bool isRememberMeChecked);
        bool IsAppStartupSet();
        void SetApplicationStartup(bool isAppStartupChecked);
        event EventHandler UpdateGameReportMessage;
    }
}
