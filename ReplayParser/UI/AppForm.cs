﻿using log4net.Config;
using ReplayParser.Tools.Contracts;
using ReplayParser.Tools.Services;
using System;
using System.Windows.Forms;

namespace UI
{
    public partial class AppForm : Form
    {
        private static IApiService _apiService;
        private static ICoreAppDomainService _domainService;
        private readonly string _loggedInMessage = "Logged in";
        private readonly string _guestMessage = "Unauthorized";

        public AppForm()
        {
            XmlConfigurator.Configure();
            _apiService = new ApiService();
            _domainService = new CoreAppDomainService(_apiService);
            _domainService.UpdateGameReportMessage += UpdateGameReportMessage;
            InitializeComponent();

            UpdateWinStartupLabel();
            UpdateCBWindowsStartup();

            if (_domainService.LogInUser(cbRememberMe.Checked, null, null))
            {
                lblLoggedIn.Text = _loggedInMessage;
                tbLogin.Enabled = false;
                tbPassword.Enabled = false;
                cbRememberMe.Enabled = false;
            }
            else
            {
                lblLoggedIn.Text = _guestMessage;
            }

            _domainService.StartProgram();
        }

        private void UpdateCBWindowsStartup()
        {
            if (_domainService.IsAppStartupSet())
            {
                cbWindowsStartup.Checked = true;
            }
        }

        private void UpdateGameReportMessage(object sender, EventArgs e)
        {
            SetText(DateTime.Now.ToShortTimeString());
        }

        private delegate void SetTextCallback(string text);

        private void SetText(string text)
        {
            if (LblMessage.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                Invoke(d, new object[] { text });
            }
            else
            {
                LblMessage.Text = "Last game reported at: " + text;
            }
        }

        private void UpdateWinStartupLabel()
        {
            if (_domainService.IsAppStartupSet())
            {
                lblStartupStatus.Text = "Starts on Windows startup";
            }
            else
            {
                lblStartupStatus.Text = "Not set";
            }
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            if (_domainService.LogInUser(cbRememberMe.Checked, tbLogin.Text, tbPassword.Text))
            {
                lblLoggedIn.Text = _loggedInMessage;
                if (cbRememberMe.Checked)
                {
                    tbLogin.Enabled = false;
                    tbPassword.Enabled = false;
                    cbRememberMe.Enabled = false;
                }
            }
            else
            {
                lblLoggedIn.Text = _guestMessage;
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            _domainService.RememberMe(cbRememberMe.Checked);
        }

        private void tbLogin_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(tbLogin.Text) && !string.IsNullOrWhiteSpace(tbPassword.Text))
            {
                btnLogin.Enabled = true;
            }
        }

        private void tbPassword_TextChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(tbLogin.Text) && !string.IsNullOrWhiteSpace(tbPassword.Text))
            {
                btnLogin.Enabled = true;
            }
        }

        private void cbWindowsStartup_CheckedChanged(object sender, EventArgs e)
        {
            _domainService.SetApplicationStartup(cbWindowsStartup.Checked);
            UpdateWinStartupLabel();
        }
    }
}
