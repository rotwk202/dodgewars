﻿using System;

namespace DW.Manager.App.Models
{
    public class ApiArGame
    {
        public byte[] ReplayFile { get; set; }
        public DateTime PlayedOn { get; set; }
        public int UnreportedMatchId { get; set; }
    }
}
