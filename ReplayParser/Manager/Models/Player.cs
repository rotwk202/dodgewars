﻿namespace DW.Manager.App.Models
{
    public class Player
    {
        public string Nickname { get; set; }

        public int Team { get; set; }

        public bool IsLooser { get; set; }

        public int ArmyReplayId { get; set; }

        public string Army { get; set; }

        public bool IsObserver { get; set; }

        public bool? IsGameHost { get; set; }
    }
}
