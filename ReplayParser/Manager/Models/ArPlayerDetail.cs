﻿namespace DW.Manager.App.Models
{
    public class ArPlayerDetail
    {
        public string Nickname { get; set; }
        public bool IsLooser { get; set; }
        public bool? IsGameHost { get; set; }
    }
}
