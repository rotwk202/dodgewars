﻿namespace DW.Manager.App.Models
{
    public class ApiArPlayer
    {
        public string Nickname { get; set; }

        public int Team { get; set; }

        public bool IsLooser { get; set; }

        public string Army { get; set; }

        public bool IsObserver { get; set; }

        public bool? IsGameHost { get; set; }

        public double? Rating { get; set; }

        public double TeamRating { get; set; }
    }
}
