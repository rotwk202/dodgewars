﻿using System;
using System.Collections.Generic;

namespace DW.Manager.App.Models
{
    public class ApiArGameDetails
    {
        public int GameId { get; set; }

        public int ReplayMapId { get; set; }

        public long? LastReplayFileLength { get; set; }

        public DateTime PlayedOn { get; set; }

        public byte[] ReplayFile { get; set; }

        public List<ApiPlayer> Players { get; set; }
    }
}
