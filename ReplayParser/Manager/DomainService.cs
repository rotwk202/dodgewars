﻿using DW.Manager.App.Models;
using DW.Manager.App.Services;
using Newtonsoft.Json;
using System;
using System.Configuration;
using System.IO;

namespace DW.Manager.App
{
    public class DomainService
    {
        private readonly ApiService _apiService;
        private readonly MapService _mapService;

        public DomainService()
        {
            _apiService = new ApiService();
            _mapService = new MapService();
        }

        public bool ProcessAR()
        {
            Console.Write("Provide the replay name you'd like to process (including replay file extension): ");
            var fileName = Console.ReadLine();

            Console.Write("Provide file name where you saved AR players details (including .txt extension): ");
            var arPlayersListFileName = Console.ReadLine();
            Console.Write("Enter host nickname (don't put anything if it was neutral host): ");
            var hostNickname = Console.ReadLine();

            var newArGameModel = new ApiGame();
            var arReplayFilePath = Path.Combine(ConfigurationManager.AppSettings["replayFileDirectory"], fileName);
            ARService arService = new ARService(arReplayFilePath);
            var arPlayers = arService.GetArPlayerDetails(arPlayersListFileName);

            try
            {
                if (arPlayers != null)
                {
                    var arGamePlayedOn = arService.GetARGamePlayedOnProp(Path.GetFileName(arReplayFilePath));
                    newArGameModel = arService.CreateARGameModel(arPlayers, arGamePlayedOn, arReplayFilePath, hostNickname);
                    var apiGameJson = JsonConvert.SerializeObject(newArGameModel);
                    var arProcessFinalMessage = _apiService.ProcessARGame(apiGameJson);

                    Console.WriteLine(arProcessFinalMessage);
                }
                else
                {
                    Console.WriteLine("There was a problem setting the AR players model from file.");
                }
            }
            catch (InvalidOperationException ioe)
            {
                Console.WriteLine($"Error occured: { ioe.Message }, Game Idendifier (inside replay ID): { newArGameModel.GameId }");
            }

            return false;
        }

        public void UploadsMaps()
        {
            Console.Write("Enter file name containig map simple properties (including .txt extension): ");
            var fileName = Console.ReadLine();
            var rawUserMapDataFilePath = Path.Combine(ConfigurationManager.AppSettings["mapsSimplePropertiesDirectory"], fileName);

            try
            {
                var mapModels = _mapService.GetMapModels(rawUserMapDataFilePath);
                var response = _apiService.UploadMaps(mapModels);
                Console.WriteLine(response);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void RecalculateGamesOnDemand()
        {
            _apiService.RecalculateAllGames();
        }
    }
}
