﻿using System;

namespace DW.Manager.App
{
    public class Program
    {
        private static DomainService _domainService;

        static void Main(string[] args)
        {
            _domainService = new DomainService();

            Console.WriteLine("Choose 1 to process AR.");
            Console.WriteLine("Choose 2 to uploads maps.");
            Console.WriteLine("Choose 3 to recalculate games od demand.");
            Console.Write("Your choice: ");

            var userChoice = Console.ReadLine();

            if (userChoice == "1")
            {
                _domainService.ProcessAR();
            }
            if (userChoice == "2")
            {
                _domainService.UploadsMaps();
            }
            if (userChoice == "3")
            {
                _domainService.RecalculateGamesOnDemand();
            }

            while (true) { }
        }
    }
}
