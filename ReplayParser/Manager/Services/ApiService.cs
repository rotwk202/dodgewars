﻿using DW.Manager.App.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;

namespace DW.Manager.App.Services
{
    public class ApiService
    {
        private readonly string apiServerEndpoint = ConfigurationManager.AppSettings["targetServer"];
        private readonly Uri _apiServer;

        //private const string _getDownloadReplay = "api/Admin/DownloadARReplay/";
        private const string _uploadMapsRoute = "api/Admin/UploadMaps/";
        private const string _processArRoute = "api/Admin/ProcessARGame/";
        private const string _recalcGamesRoute = "api/Admin/RecalculateAllGames/";
        //private const string _clientName = "console";

        public ApiService()
        {
            _apiServer = new Uri(apiServerEndpoint);
        }

        public string ProcessARGame(string apiGameJson)
        {
            var url = new Uri(_apiServer, _processArRoute).ToString();
            var encoding = new UTF8Encoding(false);
            var data = encoding.GetBytes(apiGameJson);

            WebRequest request = WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = data.Length;
            using (var sr = request.GetRequestStream())
            {
                sr.Write(data, 0, data.Length);
            }

            string responseFromServer = string.Empty;
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                {
                    responseFromServer = reader.ReadToEnd();
                }
            }
            catch (WebException wex)
            {
                if (wex.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)wex.Response)
                    {
                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                        {
                            string error = reader.ReadToEnd();
                            throw new InvalidOperationException(error);
                        }
                    }
                }
                else
                {
                    throw wex;
                }
            }

            return responseFromServer;
        }

        public string UploadMaps(List<ApiMap> mapModels)
        {
            var url = new Uri(_apiServer, _uploadMapsRoute).ToString();
            var serializedJson = JsonConvert.SerializeObject(mapModels);
            var encoding = new UTF8Encoding(false);
            var data = encoding.GetBytes(serializedJson);

            WebRequest request = WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/json";
            request.ContentLength = data.Length;
            using (var sr = request.GetRequestStream())
            {
                sr.Write(data, 0, data.Length);
            }
            // Get the response.
            string responseFromServer = string.Empty;
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream dataStream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(dataStream))
                {
                    responseFromServer = reader.ReadToEnd();
                }
            }
            catch (WebException wex)
            {
                throw wex;
            }

            return responseFromServer;
        }

        public void RecalculateAllGames()
        {
            var url = new Uri(_apiServer, _recalcGamesRoute).ToString();
            var encoding = new UTF8Encoding(false);

            WebRequest request = WebRequest.Create(url);
            request.Method = "GET";
            request.ContentType = "application/json";
            request.Timeout = 10000;

            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                }
            }
            catch (WebException wex)
            {
                throw wex;
            }
        }
    }
}
