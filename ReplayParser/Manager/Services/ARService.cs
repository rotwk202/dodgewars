﻿using DW.Manager.App.Models;
using Newtonsoft.Json;
using ReplayParser.Tools.Contracts;
using ReplayParser.Tools.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;

namespace DW.Manager.App.Services
{
    public class ARService
    {
        private readonly GameDataService _gameDataService;
        private readonly IReplayInfoService _replayInfoService;
        private readonly string _arReplayFilePath;

        public ARService(string arReplayFilePath)
        {
            _arReplayFilePath = arReplayFilePath;
            _replayInfoService = new ReplayInfoService(_arReplayFilePath);
            _gameDataService = new GameDataService(_arReplayFilePath);
        }

        public ApiGame CreateARGameModel(List<ArPlayerDetail> arPlayers, DateTime arGamePlayedOn,
            string arRepFilePath, string hostNickname)
        {
            var newArModel = new ApiGame();
            var looserNicknames = arPlayers
                .Where(x => x.IsLooser)
                .Select(x => x.Nickname)
                .ToList();

            _gameDataService.LoadReplayData();
            _replayInfoService.LoadReplayFileInfo();

            newArModel.PlayedOn = arGamePlayedOn;
            newArModel.LastReplayFileLength = _replayInfoService.FileLength;
            newArModel.GameId = int.Parse(_gameDataService.ReplayData.HeaderFields.Seed);
            newArModel.ReplayMapTextId = _gameDataService.ReplayData.HeaderFields.MapName.Replace("s/", string.Empty);

            var _playersInGame = _gameDataService.ReplayData.HeaderFields.Players;
            _gameDataService.SetIsObserverProperties(_playersInGame);
            _gameDataService.DetermineGameLoosers(looserNicknames.FirstOrDefault(), _playersInGame);

            int arrayIndex = 1;
            foreach (var player in _playersInGame)
            {
                if (!player.IsObserver)
                {
                    player.Army = "Unknown";
                    arrayIndex++;
                }
            }

            newArModel.ReplayFile = File.ReadAllBytes(arRepFilePath);

            var apiPlayersInGame = new List<ApiPlayer>();
            foreach (var p in _playersInGame)
            {
                var newApiArPlayer = new ApiPlayer()
                {
                    Nickname = p.Nickname,
                    Team = p.Team,
                    Army = p.Army,
                    IsObserver = p.IsObserver,
                    IsGameHost = false,
                    IsLooser = p.IsLooser
                };
                apiPlayersInGame.Add(newApiArPlayer);
            }

            SetIsGameHostProperties(apiPlayersInGame, hostNickname);
            newArModel.Players = apiPlayersInGame;

            return newArModel;
        }

        public DateTime GetARGamePlayedOnProp(string arFileName)
        {
            var start = arFileName.IndexOf("__");
            var end = arFileName.LastIndexOf("__");
            var dateTimeString = arFileName.Substring(start, end - start).Replace("_", " ");

            var dashStart = dateTimeString.IndexOf("-");
            var timeString = dateTimeString.Substring(dashStart, dateTimeString.Length - dashStart).Replace("-", "").Replace(" ", ":");
            var dateString = dateTimeString.Substring(0, dateTimeString.IndexOf("-"));

            var formattedDateTimeString = dateString + " " + timeString;
            var success = DateTime.TryParse(formattedDateTimeString, out DateTime result);

            return result;
        }

        public List<ArPlayerDetail> GetArPlayerDetails(string arDetailsFileName)
        {
            var output = new List<ArPlayerDetail>();
            if (string.IsNullOrEmpty(arDetailsFileName))
            {
                throw new NullReferenceException("You did not enter file name of AR players details.");
            }

            var arDetailsFilePath = Path.Combine(ConfigurationManager.AppSettings["arPlayersDetails"], arDetailsFileName);
            if (!File.Exists(arDetailsFilePath))
            {
                throw new FileNotFoundException("Unable to find the AR Players details file.");
            }

            var arPlayersRawData = File.ReadAllLines(arDetailsFilePath);

            var arWinnersRaw = arPlayersRawData[0];
            var arWinnerNicknames = arWinnersRaw.Split(';').ToList();
            var a = arWinnerNicknames.RemoveAll(x => string.IsNullOrEmpty(x));
            foreach (var winnerNickname in arWinnerNicknames)
            {
                output.Add(new ArPlayerDetail()
                {
                    Nickname = winnerNickname,
                    IsLooser = false
                });
            }

            var arLosersRaw = arPlayersRawData[1];
            var arLoserNicknames = arLosersRaw.Split(';').ToList();
            var b = arLoserNicknames.RemoveAll(x => string.IsNullOrEmpty(x));
            foreach (var loserNickname in arLoserNicknames)
            {
                output.Add(new ArPlayerDetail()
                {
                    Nickname = loserNickname,
                    IsLooser = true
                });
            }

            return output;
        }

        private void SetIsGameHostProperties(List<ApiPlayer> arPlayers, string hostNickname)
        {
            arPlayers.ForEach(x => x.IsGameHost = false);
            if (!string.IsNullOrEmpty(hostNickname))
            {
                var p = arPlayers.Find(x => x.Nickname == hostNickname);
                if(p != null)
                {
                    p.IsGameHost = true;
                }
            }
        }
    }
}
