﻿using NUnit.Framework;
using ReplayParser.Tools.Model;
using ReplayParser.Tools.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ReplayParser.Tools.Tests.Services.GameDataServiceTests
{
    [TestFixture]
    public class SetGameHostPropertiesTests
    {
        [Test]
        public void Sets_GameHostProperty_Correctly()
        {
            //Arrange
            var sut = new GameDataService(string.Empty);

            var players = new List<Player>()
            {
                new Player() { Nickname = "Radek" },
                new Player() { Nickname = "Basia" },
                new Player() { Nickname = "Andrzej" },
                new Player() { Nickname = "Wojtek" }
            };

            //Act
            sut.SetGameHostProperties(players);

            //Assert
            Assert.IsTrue(players.First().IsGameHost);
            Assert.That(players.Where(x => x.Nickname != "Radek").Select(x => x.IsGameHost).ToList().All(x => x == false));
        }

        //TODO: czy ten test w ogole cos sprawdza?
        [Test]
        public void TryingToUse_NullPlayersList_Throws_ArgumentNullException()
        {
            //Arrange
            var sut = new GameDataService(string.Empty);
            List<Player> players = null;

            //Act & Assert
            Assert.Throws<ArgumentNullException>(() => sut.SetGameHostProperties(players));
        }

        //TODO: czy ten test w ogole cos sprawdza?
        [Test]
        public void TryingToUse_EmptyPlayersList_Throws_ArgumentNullException()
        {
            //Arrange
            var sut = new GameDataService(string.Empty);
            var players = new List<Player>();

            //Act & Assert
            Assert.Throws<NullReferenceException>(() => sut.SetGameHostProperties(players));
        }

    }
}
