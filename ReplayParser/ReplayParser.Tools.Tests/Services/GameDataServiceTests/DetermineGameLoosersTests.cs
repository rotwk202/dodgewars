﻿using System;
using ReplayParser.Tools.Services;
using System.Collections.Generic;
using System.Linq;
using ReplayParser.Tools.Model;
using NUnit.Framework;

namespace ReplayParser.Tools.Tests.Servies.GameDataServiceTests
{
    [TestFixture]
    public class DetermineGameLoosersTests
    {
        [Test]
        public void UsesEmptyReporterNicknameString()
        {
            //Arrange
            var service = new GameDataService(string.Empty);
            string reporterNickname = string.Empty;
            var players = new List<Player>
            {
                new Player { Nickname = "Joe", Team = 1, ArmyReplayId = 2 },
                new Player { Nickname = "Jan", Team = 22, ArmyReplayId = 33 },
            };

            //Act & Assert
            Assert.Throws<ArgumentNullException>(() => service.DetermineGameLoosers(reporterNickname, players));
        }

        [Test]
        public void DetectsFFAGame()
        {
            //Arrange
            var service = new GameDataService(string.Empty);
            var players = new List<Player>
            {
                new Player { Nickname = "Joe", Team = -1, ArmyReplayId = 2 },
                new Player { Nickname = "Jan", Team = -1, ArmyReplayId = 33 },
                new Player { Nickname = "Chriss", Team = -1, ArmyReplayId = 33 },
                new Player { Nickname = "Bob", Team = -1, ArmyReplayId = 33 },
            };
            string reporterNickname = "Hombre";

            //Act & Assert
            Assert.Throws<InvalidOperationException>(() => service.DetermineGameLoosers(reporterNickname, players));
        }

        [Test]
        public void DeterminesOneVsOneMatchCase0()
        {
            //Arrange
            var service = new GameDataService(string.Empty);
            var players = new List<Player>
            {
                new Player { Nickname = "Joe", Team = -1, ArmyReplayId = 2 },
                new Player { Nickname = "Jan", Team = -1, ArmyReplayId = 33 },
            };

            //Act
            var reporterNickname = "Joe";
            service.DetermineGameLoosers(reporterNickname, players);

            var loosingPlayer = players.First(x => x.Nickname.Contains(reporterNickname));
            var loosingPlayerActualFlag = loosingPlayer.IsLooser;
            var loosingPlayerExpectedFlag = true;

            //Assert
            Assert.AreEqual(loosingPlayerExpectedFlag, loosingPlayerActualFlag);
            Assert.AreEqual(false, loosingPlayer.IsObserver);
            Assert.AreEqual(-1, loosingPlayer.Team);
        }

        [Test]
        public void DeterminesOneVsOneMatchCase4()
        {
            //Arrange
            var service = new GameDataService(string.Empty);

            var players = new List<Player>
            {
                new Player { Nickname = "Hanna", Team = -1, ArmyReplayId = 2 },
                new Player { Nickname = "anna", Team = -1, ArmyReplayId = 33 },
            };

            //Act
            var reporterNickname = "Hanna";

            service.DetermineGameLoosers(reporterNickname, players);
            var loosingPlayer = players.FirstOrDefault(x => x.Nickname == reporterNickname);

            //Assert
            Assert.IsNotNull(loosingPlayer);
            Assert.AreEqual(true, loosingPlayer.IsLooser);
            Assert.AreEqual(false, loosingPlayer.IsObserver);
        }

        [Test]
        public void DeterminesOneVsOneMatchCase4Reversed()
        {
            //Arrange
            var service = new GameDataService(string.Empty);

            var players = new List<Player>
            {
                new Player { Nickname = "Hanna", Team = -1, ArmyReplayId = 2 },
                new Player { Nickname = "anna", Team = -1, ArmyReplayId = 33 },
            };

            //Act
            var reporterNickname = "anna";

            service.DetermineGameLoosers(reporterNickname, players);
            var loosingPlayer = players.FirstOrDefault(x => x.Nickname == reporterNickname);

            //Assert
            Assert.IsNotNull(loosingPlayer);
            Assert.AreEqual(true, loosingPlayer.IsLooser);
            Assert.AreEqual(false, loosingPlayer.IsObserver);
        }

        //1v1
        [Test]
        public void DeterminesOneVsOneMatchCase1()
        {
            //Arrange
            var service = new GameDataService(string.Empty);
            var players = new List<Player>
            {
                new Player { Nickname = "OhReaLLy", Team = -1, ArmyReplayId = 2 },
                new Player { Nickname = "Otto", Team = -1, ArmyReplayId = 33 },
            };
            //Act
            var reporterNickname = "Otto";
            service.DetermineGameLoosers(reporterNickname, players);
            var loosingPlayer = players.First(x => x.Nickname.Contains(reporterNickname));
            var loosingPlayerActualFlag = loosingPlayer.IsLooser;
            var loosingPlayerExpectedFlag = true;

            //Assert
            Assert.AreEqual(loosingPlayerExpectedFlag, loosingPlayerActualFlag);
            Assert.AreEqual(false, loosingPlayer.IsObserver);
            Assert.AreEqual(-1, loosingPlayer.Team);
        }
    }
}
