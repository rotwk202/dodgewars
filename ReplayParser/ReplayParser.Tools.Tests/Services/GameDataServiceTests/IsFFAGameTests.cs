﻿using NUnit.Framework;
using ReplayParser.Tools.Model;
using ReplayParser.Tools.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReplayParser.Tools.Tests.Services.GameDataServiceTests
{
    [TestFixture]
    public class IsFFAGameTests
    {
        [Test]
        public void Given_FFAGameDetails_Returns_True()
        {
            //Arrange
            var sut = new GameDataService(string.Empty);

            var players = new List<Player>()
            {
                new Player() { Nickname = "Radek", ArmyReplayId = -1, Team = -1 },
                new Player() { Nickname = "Basia",  ArmyReplayId = -1, Team = -1 },
                new Player() { Nickname = "Andrzej", ArmyReplayId = -1, Team = -1 },
                new Player() { Nickname = "Wojtek", ArmyReplayId = -1, Team = -1 },
            };

            //Act
            var result = sut.IsFFAGame(players);

            //Assert
            Assert.IsTrue(result);
        }

        [Test]
        public void Given_TwoVsTwoGameDetails_Returns_False()
        {
            //Arrange
            var sut = new GameDataService(string.Empty);

            var players = new List<Player>()
            {
                new Player() { Nickname = "Radek", ArmyReplayId = -1, Team = 0 },
                new Player() { Nickname = "Basia",  ArmyReplayId = -1, Team = 0 },
                new Player() { Nickname = "Andrzej", ArmyReplayId = -1, Team = 1 },
                new Player() { Nickname = "Wojtek", ArmyReplayId = -1, Team = 1 },
            };

            //Act
            var result = sut.IsFFAGame(players);

            //Assert
            Assert.IsFalse(result);
        }

        [Test]
        public void Given_OneVsOneGameDetails_Returns_False()
        {
            //Arrange
            var sut = new GameDataService(string.Empty);

            var players = new List<Player>()
            {
                new Player() { Nickname = "Radek", ArmyReplayId = -1, Team = -1 },
                new Player() { Nickname = "Basia",  ArmyReplayId = -1, Team = -1 }
            };

            //Act
            var result = sut.IsFFAGame(players);

            //Assert
            Assert.IsFalse(result);
        }

    }

    //1v1 to ffa ale prawidlowe
}
