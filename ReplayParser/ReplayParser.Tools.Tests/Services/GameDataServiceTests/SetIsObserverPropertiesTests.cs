﻿using NUnit.Framework;
using ReplayParser.Tools.Model;
using ReplayParser.Tools.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReplayParser.Tools.Tests.Services.GameDataServiceTests
{
    [TestFixture]
    public class SetIsObserverPropertiesTests
    {
        [Test]
        public void Given_TwoVsTwoGame_Sets_FalseValues()
        {
            //Arrange
            var sut = new GameDataService(string.Empty);

            var players = new List<Player>()
            {
                new Player() { Nickname = "Radek", ArmyReplayId = -1 },
                new Player() { Nickname = "Basia",  ArmyReplayId = -1 },
                new Player() { Nickname = "Andrzej", ArmyReplayId = -1 },
                new Player() { Nickname = "Wojtek", ArmyReplayId = -1 }
            };

            //Act
            sut.SetIsObserverProperties(players);

            //Assert
            Assert.That(players.Select(x => x.IsObserver).ToList().All(x => x == false));
        }

        [Test]
        public void Given_ObserverInGame_CorrectlySets_IsObserverFlags()
        {
            //Arrange
            var sut = new GameDataService(string.Empty);

            var players = new List<Player>()
            {
                new Player() { Nickname = "Radek", ArmyReplayId = -2 },
                new Player() { Nickname = "Basia",  ArmyReplayId = -1 },
                new Player() { Nickname = "Andrzej", ArmyReplayId = -1 }
            };

            //Act
            sut.SetIsObserverProperties(players);

            //Assert
            Assert.IsTrue(players.First().IsObserver);
            Assert.That(players.Where(x => x.Nickname != "Radek").Select(x => x.IsObserver).ToList().All(x => x == false));
            Assert.AreEqual(players.Where(x => x.IsObserver).ToList().Count(), 1);
        }

    }
}
