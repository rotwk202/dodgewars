﻿using NUnit.Framework;
using ReplayParser.Tools.Model;
using ReplayParser.Tools.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReplayParser.Tools.Tests.Services.ReplayNameFormatServiceTests
{
    [TestFixture]
    public class GetReplayNameFormatToSaveTests
    {
        [Test]
        public void Given_InvalidFileName_Creates_CorrectFileName()
        {
            var gameDataService = new GameDataService(null);
            var players = new List<Player>()
            {
                new Player(){ Nickname = "StrayHeart" },
                new Player(){ Nickname = "Eonwe" },
                new Player(){ Nickname = "Bushit" },
                new Player(){ Nickname = "SDK|Rambo" },
            };
            var playersInGameString = gameDataService.ConcatPlayerListToString(players);
            var replaySavedOn = new DateTime(2019, 01, 01, 14, 00, 00);
            var gameId = 1.ToString();

            var newFileName = ReplayNameFormatService.GetReplayNameFormatToSave(playersInGameString, replaySavedOn, gameId);

            Assert.IsNotNull(newFileName);
            Assert.IsNotEmpty(newFileName);
            Assert.AreEqual("DW_1__01.01.2019-14 00 00__StrayHeart-Eonwe-Bushit-SDK]Rambo-.BfME2Replay", newFileName);
        }

        [Test]
        public void Given_ValidFileName_Creates_CorrectFileName()
        {
            var gameDataService = new GameDataService(null);
            var players = new List<Player>()
            {
                new Player(){ Nickname = "StrayHeart" },
                new Player(){ Nickname = "Eonwe" },
                new Player(){ Nickname = "Bushit" },
                new Player(){ Nickname = "SDK]Rambo" },
            };
            var playersInGameString = gameDataService.ConcatPlayerListToString(players);
            var replaySavedOn = new DateTime(2019, 01, 01, 14, 00, 00);
            var gameId = 1.ToString();

            var newFileName = ReplayNameFormatService.GetReplayNameFormatToSave(playersInGameString, replaySavedOn, gameId);

            Assert.IsNotNull(newFileName);
            Assert.IsNotEmpty(newFileName);
            Assert.AreEqual("DW_1__01.01.2019-14 00 00__StrayHeart-Eonwe-Bushit-SDK]Rambo-.BfME2Replay", newFileName);
        }
    }
}
