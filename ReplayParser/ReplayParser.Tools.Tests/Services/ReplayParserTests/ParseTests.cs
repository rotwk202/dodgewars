﻿using NUnit.Framework;
using System;
using System.IO;

namespace ReplayParser.Tools.Tests.Services.ReplayParserTests
{
    [TestFixture]
    public class ParseTests
    {
        [Test]
        public void Given_2v2DisconnectedGame_SetsTimeStampEnd_EqualTo_TimeStampStart()
        {
            var filePath = Path.Combine(AppDomain.CurrentDomain.SetupInformation.ApplicationBase, @"TestResources\2v2-timestamp-end-0-game-probably-disconnected.BfME2Replay");
            var sut = new ReplayParser.Tools.ParserTools.ReplayParser(filePath);

            var replayData = sut.Parse();
            sut.Dispose();

            Assert.AreNotEqual(0, replayData.TimestampEnd);
            Assert.AreEqual(replayData.TimestampStart, replayData.TimestampEnd);
            Assert.AreEqual("s/map mp anorien ii", replayData.HeaderFields.MapName);
            Assert.AreEqual(547145656.ToString(), replayData.HeaderFields.Seed);

            Assert.AreEqual("Muadd", replayData.HeaderFields.Players[0].Nickname);
            Assert.AreEqual(-1, replayData.HeaderFields.Players[0].ArmyReplayId);
            Assert.AreEqual(0, replayData.HeaderFields.Players[0].Team);
            Assert.AreEqual("SDK|Rambo4", replayData.HeaderFields.Players[1].Nickname);
            Assert.AreEqual(-1, replayData.HeaderFields.Players[1].ArmyReplayId);
            Assert.AreEqual(1, replayData.HeaderFields.Players[1].Team);
            Assert.AreEqual("S.S.B", replayData.HeaderFields.Players[2].Nickname);
            Assert.AreEqual(-1, replayData.HeaderFields.Players[2].ArmyReplayId);
            Assert.AreEqual(1, replayData.HeaderFields.Players[2].Team);
            Assert.AreEqual("Orgueil`", replayData.HeaderFields.Players[3].Nickname);
            Assert.AreEqual(-1, replayData.HeaderFields.Players[3].ArmyReplayId);
            Assert.AreEqual(0, replayData.HeaderFields.Players[3].Team);
        }

        [Test]
        public void Given_FinishedGame_1v1NeutralHost_CorrectlyParsesData()
        {
            var filePath = Path.Combine(AppDomain.CurrentDomain.SetupInformation.ApplicationBase, @"TestResources\1v1-neutral-host.bfme2replay");
            var sut = new ReplayParser.Tools.ParserTools.ReplayParser(filePath);

            var replayData = sut.Parse();
            sut.Dispose();

            Assert.AreNotEqual(0, replayData.TimestampEnd);
            Assert.AreNotEqual(replayData.TimestampStart, replayData.TimestampEnd);
            Assert.AreEqual("s/map mp fords of isen", replayData.HeaderFields.MapName);
            Assert.AreEqual(243040687.ToString(), replayData.HeaderFields.Seed);

            Assert.AreEqual("Fairy", replayData.HeaderFields.Players[0].Nickname);
            Assert.AreEqual(-2, replayData.HeaderFields.Players[0].ArmyReplayId);
            Assert.AreEqual(-1, replayData.HeaderFields.Players[0].Team);
            Assert.AreEqual("Talos", replayData.HeaderFields.Players[1].Nickname);
            Assert.AreEqual(-1, replayData.HeaderFields.Players[1].ArmyReplayId);
            Assert.AreEqual(-1, replayData.HeaderFields.Players[1].Team);
            Assert.AreEqual("Asba`", replayData.HeaderFields.Players[2].Nickname);
            Assert.AreEqual(-1, replayData.HeaderFields.Players[2].ArmyReplayId);
            Assert.AreEqual(-1, replayData.HeaderFields.Players[2].Team);
        }

        [Test]
        public void Given_FinishedGame_2v2NeutralHost_CorrectlyParsesData()
        {
            var filePath = Path.Combine(AppDomain.CurrentDomain.SetupInformation.ApplicationBase, @"TestResources\2v2-neutral-host.BfME2Replay");
            var sut = new ReplayParser.Tools.ParserTools.ReplayParser(filePath);

            var replayData = sut.Parse();
            sut.Dispose();

            Assert.AreNotEqual(0, replayData.TimestampEnd);
            Assert.AreNotEqual(replayData.TimestampStart, replayData.TimestampEnd);
            Assert.AreEqual("s/map mp anorien ii", replayData.HeaderFields.MapName);
            Assert.AreEqual(527040046.ToString(), replayData.HeaderFields.Seed);

            Assert.AreEqual("Ugluk", replayData.HeaderFields.Players[0].Nickname);
            Assert.AreEqual(-2, replayData.HeaderFields.Players[0].ArmyReplayId);
            Assert.AreEqual(-1, replayData.HeaderFields.Players[0].Team);
            Assert.AreEqual("Jaberwauke", replayData.HeaderFields.Players[1].Nickname);
            Assert.AreEqual(-1, replayData.HeaderFields.Players[1].ArmyReplayId);
            Assert.AreEqual(0, replayData.HeaderFields.Players[1].Team);
            Assert.AreEqual("TeachMe202", replayData.HeaderFields.Players[2].Nickname);
            Assert.AreEqual(-1, replayData.HeaderFields.Players[2].ArmyReplayId);
            Assert.AreEqual(1, replayData.HeaderFields.Players[2].Team);
            Assert.AreEqual("Orgueil`", replayData.HeaderFields.Players[3].Nickname);
            Assert.AreEqual(-1, replayData.HeaderFields.Players[3].ArmyReplayId);
            Assert.AreEqual(1, replayData.HeaderFields.Players[3].Team);
            Assert.AreEqual("liam666", replayData.HeaderFields.Players[4].Nickname);
            Assert.AreEqual(9, replayData.HeaderFields.Players[4].ArmyReplayId);
            Assert.AreEqual(0, replayData.HeaderFields.Players[4].Team);
        }

    }
}
