﻿using log4net;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReplayReader
{
    class Program
    {
        private static ILog _logger;

        static void Main(string[] args)
        {
            XmlConfigurator.Configure();
            _logger = LogManager.GetLogger("InfoLogger");
            var filePath = @"C:\Users\TOP\AppData\Roaming\My The Lord of the Rings, The Rise of the Witch-king Files\Replays\Last Replay.BfME2Replay";
            //var filePath = @"C:\Users\Dell\AppData\Roaming\My The Lord of the Rings, The Rise of the Witch-king Files\Replays\Last Replay.BfME2Replay";
            var lockingProcesses = FileProcessLockChecker.WhoIsLocking(filePath);

            _logger.Info("**** BEFORE TRY BLOCK ****");
            _logger.Info($"current process id: {Process.GetCurrentProcess().Id}");
            if(lockingProcesses.Count > 0)
            {
                _logger.Info($"Process ids blocking file: {string.Join(",", lockingProcesses.Select(x => x.Id).ToList())}");
            }
            else
            {
                _logger.Info("There is no process locking the file.");
            }

            FileStream fileStream = null;
            BinaryReader reader = null;
            try
            {
                var fileInfo = new FileInfo(filePath);
                _logger.Info($"Try block: fileInfo.Length: {fileInfo.Length}");
                fileStream = fileInfo.Open(FileMode.Open, FileAccess.Read, FileShare.Read);
                fileStream.Position = 0;
                reader = new BinaryReader(fileStream, Encoding.ASCII);
                _logger.Info($"Try block: fileStream.Length: {fileStream.Length}, fileStream.Position: {fileStream.Position}");
            }
            catch (Exception e)
            {
                reader.Dispose();
                fileStream.Dispose();
                _logger.Info($"Catch block: exception: {e}");
            }

            reader.Dispose();
            fileStream.Dispose();

            var lockingProcesses1 = FileProcessLockChecker.WhoIsLocking(filePath);
            _logger.Info("**** AFTER TRY BLOCK ****");
            _logger.Info($"current process id: {Process.GetCurrentProcess().Id}");
            if (lockingProcesses1.Count > 0)
            {
                _logger.Info($"Process ids blocking file: {string.Join(",", lockingProcesses1.Select(x => x.Id).ToList())}");
            }
            else
            {
                _logger.Info("There is no process locking the file.");
            }

        }
    }
}
